package uz.pdp.onlineannouncement.service;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import uz.pdp.onlineannouncement.dao.UserDao;
import uz.pdp.onlineannouncement.entity.User;
import uz.pdp.onlineannouncement.service.impl.UserServiceImpl;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.UUID;
import java.util.stream.Stream;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.junit.jupiter.params.provider.Arguments.of;
import static org.mockito.ArgumentMatchers.*;
import static org.mockito.Mockito.when;

class UserServiceTest {

    @InjectMocks
    UserService userService = UserServiceImpl.getInstance();

    @Mock
    UserDao userDao;

    @BeforeEach
    public void setup()
    {
        MockitoAnnotations.openMocks(this);
    }

    private static Stream<Arguments> userProvider()
    {
        User user1 = new User();
        user1.setId(UUID.randomUUID());

        User user2 = new User();
        user1.setId(UUID.randomUUID());

        User user3 = new User();
        user1.setId(UUID.randomUUID());
        return Stream.of(
                of(user1),
                of(user2),
                of(user3)
        );
    }

    private static Stream<Arguments> userListsProvider()
    {
        User user1 = new User();
        user1.setId(UUID.randomUUID());

        User user2 = new User();
        user1.setId(UUID.randomUUID());

        User user3 = new User();
        user1.setId(UUID.randomUUID());

        List<User> userList1 = new ArrayList<>(Arrays.asList(user1, user2, user3));
        List<User> userList2 = new ArrayList<>(Arrays.asList(user1, user2));
        List<User> userList3 = new ArrayList<>(List.of(user1));
        return Stream.of(
                of(userList1),
                of(userList2),
                of(userList3)
        );
    }

    @ParameterizedTest
    @MethodSource(value = "userProvider")
    public void testInsert_should_return_true(User user)
    {
        when(userDao.insert(any())).thenReturn(true);
        boolean insert = userService.insert(user);
        assertTrue(insert);
    }

    @Test
    public void testAuthenticate_should_return_true()
    {
        when(userDao.authenticate(anyString(), anyString())).thenReturn(true);
        boolean authenticate = userService.authenticate("aaa@gmail.com", "user1");
        assertTrue(authenticate);
    }

    @ParameterizedTest
    @MethodSource(value = "userProvider")
    public void testDelete_should_return_true(User user)
    {
        when(userDao.delete(any())).thenReturn(true);
        boolean delete = userService.delete(user);
        assertTrue(delete);
    }

    @ParameterizedTest
    @MethodSource(value = "userProvider")
    public void testUpdate_should_return_true(User user)
    {
        when(userDao.update(any())).thenReturn(true);
        boolean update = userService.update(user);
        assertTrue(update);
    }

    @Test
    public void testCheckToAdmin_should_return_true()
    {
        when(userDao.checkToAdmin(anyString(), anyString())).thenReturn(true);
        boolean admin = userService.checkToAdmin("aaa@gmail.com", "admin1");
        assertTrue(admin);
    }

    @ParameterizedTest
    @MethodSource(value = "userProvider")
    public void testFindById_should_return_true(User user)
    {
        when(userDao.findById(any())).thenReturn(user);
        User byId = userService.findById(UUID.randomUUID());
        assertEquals(user, byId);
    }

    @ParameterizedTest
    @MethodSource(value = "userProvider")
    public void testFindByUserName_should_return_true(User user)
    {
        when(userDao.findByEmail(anyString())).thenReturn(user);
        User byUserName = userService.findByEmail("email");
        assertEquals(user, byUserName);
    }

    @ParameterizedTest
    @MethodSource(value = "userProvider")
    public void testBlockOrActive_should_return_true(User user)
    {
        when(userDao.blockOrActive(any(), anyBoolean())).thenReturn(true);
        boolean blockOrActive = userService.blockOrActive(user.getId(), true);
        assertTrue( blockOrActive);
    }

    @ParameterizedTest
    @MethodSource(value = "userProvider")
    public void testCheckToActive_should_return_true(User user)
    {
        when(userDao.checkToActive(any())).thenReturn(true);
        boolean checkToActive = userService.checkToActive(user.getId());
        assertTrue(checkToActive);
    }

    @ParameterizedTest
    @MethodSource(value = "userListsProvider")
    public void testFindAll_should_return_true(List<User> users)
    {
        when(userDao.findAll()).thenReturn(users);
        List<User> all = userService.findAll();
        assertEquals(users, all);
    }

    @ParameterizedTest
    @MethodSource(value = "userListsProvider")
    public void testFindAllWithoutUser_should_return_true(List<User> users)
    {
        UUID userId = UUID.randomUUID();
        when(userDao.findAllWithoutUser(userId)).thenReturn(users);
        List<User> allWithoutUser = userService.findAllWithoutUser(userId);
        assertEquals(users, allWithoutUser);
    }
}