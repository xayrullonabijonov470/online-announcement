package uz.pdp.onlineannouncement.service;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import uz.pdp.onlineannouncement.dao.AnnouncementDao;
import uz.pdp.onlineannouncement.entity.Announcement;
import uz.pdp.onlineannouncement.service.impl.AnnouncementServiceImpl;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.UUID;
import java.util.stream.Stream;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.junit.jupiter.params.provider.Arguments.of;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyBoolean;
import static org.mockito.Mockito.when;

class AnnouncementServiceTest {

    @InjectMocks
    AnnouncementService announcementService = AnnouncementServiceImpl.getInstance();

    @Mock
    AnnouncementDao announcementDao;

    @BeforeEach
    public void setup()
    {
        MockitoAnnotations.openMocks(this);
    }

    private static Stream<Arguments> announcementsProvider()
    {
        return Stream.of(
                of(new Announcement(
                        UUID.randomUUID(),
                        null,
                        "Tv",
                        "cheapest",
                        12.0,
                        1)),
                of(new Announcement(
                        UUID.randomUUID(),
                        null,
                        "Phone",
                        "expensive",
                        122.0,
                        41))
        );
    }

    private static Stream<Arguments> announcementListsProvider()
    {
        Announcement announcement1 = new Announcement(
                UUID.randomUUID(),
                null,
                "Tv",
                "the cheapest",
                12.0,
                1);

        Announcement announcement2 = new Announcement(
                UUID.randomUUID(),
                null,
                "Phone",
                "very good",
                122.0,
                41);

        Announcement announcement3 = new Announcement(
                UUID.randomUUID(),
                null,
                "hp",
                "it is new !!!",
                522.0,
                0);
        List<Announcement> announcements1 = new ArrayList<>(Arrays.asList(announcement1, announcement2, announcement3));
        List<Announcement> announcements2 = new ArrayList<>(Arrays.asList(announcement1, announcement2));
        List<Announcement> announcements3 = new ArrayList<>(List.of(announcement1));
        return Stream.of(
                of(announcements1),
                of(announcements2),
                of(announcements3)
        );
    }

    @ParameterizedTest
    @MethodSource(value = "announcementsProvider")
    public void testInsert_should_return_true(Announcement announcement)
    {
        when(announcementDao.insert(any())).thenReturn(true);
        boolean insert = announcementService.insert(announcement);
        assertTrue(insert);
    }

    @ParameterizedTest
    @MethodSource(value = "announcementsProvider")
    public void testDelete_should_return_true(Announcement announcement)
    {
        when(announcementDao.delete(any())).thenReturn(true);
        boolean delete = announcementService.delete(announcement.getId());
        assertTrue(delete);
    }

    @ParameterizedTest
    @MethodSource(value = "announcementsProvider")
    public void testupdate_should_return_true(Announcement announcement)
    {
        when(announcementDao.update(any())).thenReturn(true);
        boolean update = announcementService.update(announcement);
        assertTrue(update);
    }

    @ParameterizedTest
    @MethodSource(value = "announcementListsProvider")
    public void testFindAll_should_return_true(List<Announcement> announcements)
    {
        when(announcementDao.findAll()).thenReturn(announcements);
        List<Announcement> all = announcementService.findAll();
        assertEquals(all.size(), announcements.size());
    }

    @ParameterizedTest
    @MethodSource(value = "announcementsProvider")
    public void testFindById_should_return_true(Announcement announcement)
    {
        when(announcementDao.findById(any())).thenReturn(announcement);
        Announcement announcementActual = announcementService.findById(announcement.getId());
        assertEquals(announcement, announcementActual);
    }

    @ParameterizedTest
    @MethodSource(value = "announcementListsProvider")
    public void testFindAllByApprovedOrBlocked_should_return_true(List<Announcement> announcements)
    {
        when(announcementDao.findAllByApprovedOrBlocked(anyBoolean())).thenReturn(announcements);
        List<Announcement> approvedAnnouncementList = announcementService.findAllByApprovedOrBlocked(true);
        assertEquals(announcements.size(), approvedAnnouncementList.size());
    }

    @ParameterizedTest
    @MethodSource(value = "announcementListsProvider")
    public void testFindAllWithoutOwner_should_return_true(List<Announcement> announcements)
    {
        when(announcementDao.findAllWithoutOwner(any())).thenReturn(announcements);
        List<Announcement> allWithoutOwner = announcementService.findAllWithoutOwner(UUID.randomUUID());
        assertEquals(announcements.size(), allWithoutOwner.size());
    }

    @ParameterizedTest
    @MethodSource(value = "announcementsProvider")
    public void testSetApprovedOrBlocked_should_return_true(Announcement announcement)
    {
        when(announcementDao.setApprovedOrBlocked(any(), anyBoolean())).thenReturn(true);
        boolean approvedOrBlocked = announcementService.setApprovedOrBlocked(announcement.getId(), true);
        assertTrue(approvedOrBlocked);
    }

    @ParameterizedTest
    @MethodSource(value = "announcementListsProvider")
    public void testfindAllByOwner_should_return_true(List<Announcement> announcements)
    {
        when(announcementDao.findAllByOwner(any())).thenReturn(announcements);
        List<Announcement> allByOwner = announcementService.findAllByOwner(UUID.randomUUID());
        assertEquals(announcements.size(), allByOwner.size());
    }
}