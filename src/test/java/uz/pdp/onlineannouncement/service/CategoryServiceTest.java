package uz.pdp.onlineannouncement.service;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import uz.pdp.onlineannouncement.dao.CategoryDao;
import uz.pdp.onlineannouncement.entity.Category;
import uz.pdp.onlineannouncement.service.impl.CategoryServiceImpl;

import java.sql.Timestamp;
import java.util.Arrays;
import java.util.List;
import java.util.UUID;
import java.util.stream.Stream;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.junit.jupiter.params.provider.Arguments.of;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

class CategoryServiceTest {

    @Mock
    CategoryDao categoryDao;

    @InjectMocks
    CategoryService categoryService = CategoryServiceImpl.getInstance();

    @BeforeEach
    public void setup()
    {
        MockitoAnnotations.openMocks(this);
    }

    private static Stream<Arguments> categoriesProvider()
    {
        Category category = new Category();
        category.setId(UUID.randomUUID());
        category.setName("Test1");
        category.setCreatedAt(new Timestamp(System.currentTimeMillis()));

        Category category2 = new Category();
        category2.setId(UUID.randomUUID());
        category2.setName("Test2");
        category2.setCreatedAt(new Timestamp(System.currentTimeMillis()));

        Category category3 = new Category();
        category3.setCreatedAt(new Timestamp(System.currentTimeMillis()));

        return Stream.of(
                of(category),
                of(category2),
                of(category3)
        );
    }

    private static Stream<Arguments> CategoryListProvider()
    {
        Category category = new Category();
        category.setId(UUID.randomUUID());
        category.setName("Test1");
        category.setCreatedAt(new Timestamp(System.currentTimeMillis()));

        Category category2 = new Category();
        category2.setId(UUID.randomUUID());
        category2.setName("Test2");
        category2.setCreatedAt(new Timestamp(System.currentTimeMillis()));

        Category category3 = new Category();
        category.setId(UUID.randomUUID());
        category3.setName("Test3");
        category3.setCreatedAt(new Timestamp(System.currentTimeMillis()));

        List<Category> categoryList = Arrays.asList(category, category2, category3);
        List<Category> categoryList2 = Arrays.asList(category, category2);
        return Stream.of(
                of(categoryList),
                of(categoryList2)
        );
    }

    @ParameterizedTest
    @MethodSource(value = "categoriesProvider")
    public void insertTest(Category category)
    {
        when(categoryDao.insert(any())).thenReturn(true);
        boolean insert = categoryService.insert(category);
        assertTrue(insert);
    }

    @ParameterizedTest
    @MethodSource(value = "categoriesProvider")
    public void testDelete(Category category)
    {
        when(categoryDao.delete(any())).thenReturn(true);
        boolean delete = categoryService.delete(category);
        assertTrue(delete);
    }

    @ParameterizedTest
    @MethodSource(value = "CategoryListProvider")
    public void testFindAll(List<Category> categories)
    {
        when(categoryDao.findAll()).thenReturn(categories);
        List<Category> all = categoryService.findAll();
        assertEquals(all.size(), categories.size());
    }

    @ParameterizedTest
    @MethodSource(value = "categoriesProvider")
    public void testUpdate(Category category)
    {
        when(categoryDao.update(any())).thenReturn(true);
        boolean update = categoryService.update(category);
        assertTrue(update);
    }

    @Test
    public void testFindByIdOrName()
    {
        Category category = new Category();
        category.setId(UUID.randomUUID());

        when(categoryDao.findByIdOrName(any(), any())).thenReturn(category);
        Category byIdOrName = categoryService.findByIdOrName(category.getId(), null);
        assertEquals(category, byIdOrName);
    }

}