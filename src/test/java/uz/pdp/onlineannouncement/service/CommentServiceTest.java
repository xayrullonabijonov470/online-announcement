package uz.pdp.onlineannouncement.service;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import uz.pdp.onlineannouncement.dao.CommentDao;
import uz.pdp.onlineannouncement.entity.Comment;
import uz.pdp.onlineannouncement.service.impl.CommentServiceImpl;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.UUID;
import java.util.stream.Stream;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.junit.jupiter.params.provider.Arguments.of;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

class CommentServiceTest {
    @InjectMocks
    CommentService commentService = CommentServiceImpl.getInstance();

    @Mock
    CommentDao commentDao;

    @BeforeEach
    public void setup()
    {
        MockitoAnnotations.openMocks(this);
    }

    private static Stream<Arguments> commentProvider()
    {
        return Stream.of(
                of(new Comment(
                        UUID.randomUUID(),
                        "first comment",
                        null,
                        "Message text",
                        null,
                        null)),
                of(new Comment(
                        UUID.randomUUID(),
                        "second comment",
                        null,
                        "just message",
                        null,
                        null))
        );
    }

    private static Stream<Arguments> commentListsProvider()
    {
        Comment comment1 = new Comment(
                UUID.randomUUID(),
                "first comment",
                null,
                "Message text",
                null,
                null);

        Comment comment2 = new Comment(
                UUID.randomUUID(),
                "second comment",
                null,
                "just message",
                null,
                null);

        Comment comment3 = new Comment(
                UUID.randomUUID(),
                "third comment",
                null,
                "it is just message too",
                null,
                null);
        List<Comment> commentList1 = new ArrayList<>(Arrays.asList(comment1, comment2, comment3));
        List<Comment> commentList2 = new ArrayList<>(Arrays.asList(comment1, comment2));
        List<Comment> commentList3 = new ArrayList<>(List.of(comment1));
        return Stream.of(
                of(commentList1),
                of(commentList2),
                of(commentList3)
        );
    }

    @ParameterizedTest
    @MethodSource(value = "commentProvider")
    public void testInsert_should_return_true(Comment comment)
    {
        when(commentDao.insert(any())).thenReturn(true);
        boolean insert = commentService.insert(comment);
        assertTrue(insert);
    }

    @ParameterizedTest
    @MethodSource(value = "commentProvider")
    public void testUpdate_should_return_true(Comment comment)
    {
        when(commentDao.update(any())).thenReturn(true);
        boolean update = commentService.update(comment);
        assertTrue(update);
    }

    @ParameterizedTest
    @MethodSource(value = "commentProvider")
    public void testDelete_should_return_true(Comment comment)
    {
        when(commentDao.delete(any())).thenReturn(true);
        boolean delete = commentService.delete(comment.getId());
        assertTrue(delete);
    }

    @ParameterizedTest
    @MethodSource(value = "commentProvider")
    public void testFindById_should_return_true(Comment comment)
    {
        when(commentDao.findById(any())).thenReturn(comment);
        Comment byId = commentService.findById(comment.getId());
        assertEquals(comment, byId);
    }

    @ParameterizedTest
    @MethodSource(value = "commentListsProvider")
    public void testFindAll_should_return_true(List<Comment> comments)
    {
        when(commentDao.findAll()).thenReturn(comments);
        List<Comment> all = commentDao.findAll();
        assertEquals(comments, all);
    }

    @ParameterizedTest
    @MethodSource(value = "commentListsProvider")
    public void testFindAllByAnnouncementId_should_return_true(List<Comment> comments)
    {
        when(commentDao.findAllByAnnouncementId(any())).thenReturn(comments);
        List<Comment> all = commentDao.findAllByAnnouncementId(UUID.randomUUID());
        assertEquals(comments, all);
    }
}