package uz.pdp.onlineannouncement.service;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import uz.pdp.onlineannouncement.dao.RatesDao;
import uz.pdp.onlineannouncement.entity.Rate;
import uz.pdp.onlineannouncement.service.impl.RateServiceImpl;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.UUID;
import java.util.stream.Stream;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.junit.jupiter.params.provider.Arguments.of;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

class RateServiceTest {

    @InjectMocks
    RateService rateService = RateServiceImpl.getInstance();

    @Mock
    RatesDao ratesDao;

    @BeforeEach
    public void setup()
    {
        MockitoAnnotations.openMocks(this);
    }

    private static Stream<Arguments> rateProvider()
    {
        return Stream.of(
                of(new Rate(
                        UUID.randomUUID(),
                        null,
                        null,
                        null,
                        4,
                        new Timestamp(System.currentTimeMillis())
                )),
                of(new Rate(
                        UUID.randomUUID(),
                        null,
                        null,
                        null,
                        2,
                        new Timestamp(System.currentTimeMillis())))
        );
    }

    private static Stream<Arguments> rateListsProvider()
    {
        Rate rate1 = new Rate(
                UUID.randomUUID(),
                null,
                null,
                null,
                4,
                new Timestamp(System.currentTimeMillis())
        );

        Rate rate2 = new Rate(
                UUID.randomUUID(),
                null,
                null,
                null,
                5,
                new Timestamp(System.currentTimeMillis())
        );

        Rate rate3 = new Rate(
                UUID.randomUUID(),
                null,
                null,
                null,
                5,
                new Timestamp(System.currentTimeMillis())
        );
        List<Rate> rateList1 = new ArrayList<>(Arrays.asList(rate1, rate2, rate3));
        List<Rate> rateList2 = new ArrayList<>(Arrays.asList(rate1, rate2));
        List<Rate> rateList3 = new ArrayList<>(List.of(rate1));
        return Stream.of(
                of(rateList1),
                of(rateList2),
                of(rateList3)
        );
    }

    @ParameterizedTest
    @MethodSource(value = "rateProvider")
    public void testInsert_should_return_true(Rate rate)
    {
        when(ratesDao.insert(any())).thenReturn(true);
        boolean insert = rateService.insert(rate);
        assertTrue(insert);
    }

    @ParameterizedTest
    @MethodSource(value = "rateProvider")
    public void testDelete_should_return_true(Rate rate)
    {
        when(ratesDao.delete(any())).thenReturn(true);
        boolean delete = rateService.delete(rate.getId());
        assertTrue(delete);
    }

    @ParameterizedTest
    @MethodSource(value = "rateProvider")
    public void testUpdate_should_return_true(Rate rate)
    {
        when(ratesDao.update(any())).thenReturn(true);
        boolean update = rateService.update(rate);
        assertTrue(update);
    }

    @ParameterizedTest
    @MethodSource(value = "rateProvider")
    public void testFindById_should_return_true(Rate rate)
    {
        when(ratesDao.findById(any())).thenReturn(rate);
        Rate byId = rateService.findById(rate.getId());
        assertEquals(rate, byId);
    }

    @ParameterizedTest
    @MethodSource(value = "rateListsProvider")
    public void testFindAll_should_return_true(List<Rate> rates)
    {
        when(ratesDao.findAll()).thenReturn(rates);
        List<Rate> all = rateService.findAll();
        assertEquals(rates, all);
    }

    @ParameterizedTest
    @MethodSource(value = "rateListsProvider")
    public void testFindAllByToUserId_should_return_true(List<Rate> rates)
    {
        when(ratesDao.findAllByToUserId(any())).thenReturn(rates);
        List<Rate> all = rateService.findAllByToUserId(UUID.randomUUID());
        assertEquals(rates, all);
    }

}