package uz.pdp.onlineannouncement.service;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import uz.pdp.onlineannouncement.dao.ChatDao;
import uz.pdp.onlineannouncement.entity.Chat;
import uz.pdp.onlineannouncement.service.impl.ChatServiceImpl;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.UUID;
import java.util.stream.Stream;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.junit.jupiter.params.provider.Arguments.of;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

class ChatServiceTest {

    @InjectMocks
    ChatService chatService = ChatServiceImpl.getInstance();

    @Mock
    ChatDao chatDao;

    @BeforeEach
    public void setup()
    {
        MockitoAnnotations.openMocks(this);
    }

    private static Stream<Arguments> chatProvider()
    {
        return Stream.of(
                of(new Chat(UUID.randomUUID(),
                        null,
                        null,
                        null,
                        null,
                        null,
                        "hello",
                        true,
                        true)),
                of(new Chat(UUID.randomUUID(),
                        "just name",
                        null,
                        null,
                        null,
                        null,
                        "how are you",
                        true,
                        true))
        );
    }

    private static Stream<Arguments> chatListsProvider()
    {
        Chat chat1 =new Chat(
                UUID.randomUUID(),
                null,
                null,
                null,
                null,
                null,
                "Hi",
                true,
                true);

        Chat chat2 = new Chat(UUID.randomUUID(),
                "name",
                null,
                null,
                null,
                null,
                "hello",
                true,
                true);

        Chat chat3 = new Chat(UUID.randomUUID(),
                "chatname",
                null,
                null,
                null,
                null,
                "hello mello",
                true,
                true);
        List<Chat> chatList1 = new ArrayList<>(Arrays.asList(chat1, chat2, chat3));
        List<Chat> chatList2 = new ArrayList<>(Arrays.asList(chat1, chat2));
        List<Chat> chatList3 = new ArrayList<>(List.of(chat1));
        return Stream.of(
                of(chatList1),
                of(chatList2),
                of(chatList3)
        );
    }

    @ParameterizedTest
    @MethodSource(value = "chatProvider")
    public void testInsert_should_return_true(Chat chat)
    {
        when(chatDao.insert(any())).thenReturn(true);
        boolean insert = chatService.insert(chat);
        assertTrue(insert);
    }

    @ParameterizedTest
    @MethodSource(value = "chatProvider")
    public void testDelete_should_return_true(Chat chat)
    {
        when(chatDao.delete(any())).thenReturn(true);
        boolean delete = chatService.delete(chat.getId());
        assertTrue(delete);
    }

    @ParameterizedTest
    @MethodSource(value = "chatProvider")
    public void testUpdate_should_return_true(Chat chat)
    {
        when(chatDao.update(any())).thenReturn(true);
        boolean update = chatService.update(chat);
        assertTrue(update);
    }

    @ParameterizedTest
    @MethodSource(value = "chatProvider")
    public void testFindById_should_return_true(Chat chat)
    {
        when(chatDao.findById(any())).thenReturn(chat);
        Chat chatById = chatService.findById(chat.getId());
        assertEquals(chat, chatById);
    }

    @ParameterizedTest
    @MethodSource(value = "chatListsProvider")
    public void testFindAll_should_return_true(List<Chat> chats)
    {
        when(chatDao.findAll()).thenReturn(chats);
        List<Chat> all = chatService.findAll();
        assertEquals(chats, all);
    }

    @ParameterizedTest
    @MethodSource(value = "chatListsProvider")
    public void testFindAllByBothUserAndAnnouncement_should_return_true(List<Chat> chats)
    {
        when(chatDao.findAllByBothUserAndAnnouncement(any(), any(), any())).thenReturn(chats);
        List<Chat> all = chatService.findAllByBothUserAndAnnouncement(UUID.randomUUID(), UUID.randomUUID(), UUID.randomUUID());
        assertEquals(chats, all);
    }

    @ParameterizedTest
    @MethodSource(value = "chatListsProvider")
    public void testFindAllUserChattersByUser_should_return_true(List<Chat> chats)
    {
        when(chatDao.findAllUserChattersByUser(any())).thenReturn(chats);
        List<Chat> all = chatService.findAllUserChattersByUser(UUID.randomUUID());
        assertEquals(chats, all);
    }
}