package uz.pdp.onlineannouncement.dao;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import uz.pdp.onlineannouncement.dao.impl.CommentDaoImpl;
import uz.pdp.onlineannouncement.entity.Announcement;
import uz.pdp.onlineannouncement.entity.Comment;
import uz.pdp.onlineannouncement.entity.User;
import uz.pdp.onlineannouncement.pool.ConnectionPool;

import java.sql.*;
import java.util.List;
import java.util.UUID;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.when;

class CommentDaoTest {

    @Mock
    Connection connection;

    @Mock
    ResultSet resultSet;

    @Mock
    ConnectionPool connectionPool;

    @Mock
    PreparedStatement preparedStatement;

    @Mock
    Statement statement;

    @InjectMocks
    CommentDaoImpl commentDao;

    @BeforeEach
    public void setup()
    {
        MockitoAnnotations.openMocks(this);
    }

    @Test
    void insert() throws SQLException
    {
        when(connectionPool.getConnection()).thenReturn(connection);
        when(connection.prepareStatement(anyString())).thenReturn(preparedStatement);

        Announcement announcement = new Announcement();
        announcement.setId(UUID.randomUUID());
        User user = new User();
        user.setId(UUID.randomUUID());

        Comment comment = new Comment();
        comment.setAnnouncement(announcement);
        comment.setUser(user);

        boolean insert = commentDao.insert(comment);
        assertTrue(insert);
    }

    @Test
    void delete() throws SQLException
    {
        when(connectionPool.getConnection()).thenReturn(connection);
        when(connection.prepareStatement(anyString())).thenReturn(preparedStatement);

        boolean delete = commentDao.delete(UUID.randomUUID());
        assertTrue(delete);
    }

    @Test
    void findAll() throws SQLException {
        when(connectionPool.getConnection()).thenReturn(connection);
        when(connection.createStatement()).thenReturn(statement);
        when(statement.executeQuery(any())).thenReturn(resultSet);

        List<Comment> all = commentDao.findAll();
        assertTrue(all.isEmpty());
    }

    @Test
    void findAllByAnnouncementId() throws SQLException
    {
        when(connectionPool.getConnection()).thenReturn(connection);
        when(connection.prepareStatement(anyString())).thenReturn(preparedStatement);
        when(preparedStatement.executeQuery()).thenReturn(resultSet);

        List<Comment> allByAnnouncementId = commentDao.findAllByAnnouncementId(UUID.randomUUID());
        assertTrue(allByAnnouncementId.isEmpty());
    }

    @Test
    void update()
    {
        assertFalse(commentDao.update(new Comment()));
    }

    @Test
    void findById() throws SQLException
    {
        when(connectionPool.getConnection()).thenReturn(connection);
        when(connection.prepareStatement(anyString())).thenReturn(preparedStatement);
        when(preparedStatement.executeQuery()).thenReturn(resultSet);

        Comment byId = commentDao.findById(UUID.randomUUID());
        assertNull(byId);
    }
}