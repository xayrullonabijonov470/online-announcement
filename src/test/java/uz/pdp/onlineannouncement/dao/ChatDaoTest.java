package uz.pdp.onlineannouncement.dao;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import uz.pdp.onlineannouncement.dao.impl.ChatDaoImpl;
import uz.pdp.onlineannouncement.entity.Announcement;
import uz.pdp.onlineannouncement.entity.Chat;
import uz.pdp.onlineannouncement.entity.User;
import uz.pdp.onlineannouncement.pool.ConnectionPool;

import java.sql.*;
import java.util.List;
import java.util.UUID;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.*;
import static org.mockito.Mockito.when;

class ChatDaoTest
{
    @Mock
    Connection connection;

    @Mock
    ResultSet resultSet;

    @Mock
    ConnectionPool connectionPool;

    @Mock
    PreparedStatement preparedStatement;

    @Mock
    Statement statement;

    @InjectMocks
    ChatDaoImpl chatDao;

    @BeforeEach
    public void setup()
    {
        MockitoAnnotations.openMocks(this);
    }

    @Test
    void insert() throws SQLException
    {
        when(connectionPool.getConnection()).thenReturn(connection);
        when(connection.prepareStatement(anyString())).thenReturn(preparedStatement);

        Announcement announcement = new Announcement();
        announcement.setId(UUID.randomUUID());
        User sender = new User();
        sender.setId(UUID.randomUUID());
        User receiver = new User();
        receiver.setId(UUID.randomUUID());

        Chat chat = new Chat();
        chat.setAnnouncement(announcement);
        chat.setSender(sender);
        chat.setReceiver(receiver);

        boolean insert = chatDao.insert(chat);
        assertTrue(insert);
    }

    @Test
    void delete()
    {
        boolean delete = chatDao.delete(UUID.randomUUID());
        assertFalse(delete);
    }

    @Test
    void findAll() throws SQLException {
        when(connectionPool.getConnection()).thenReturn(connection);
        when(connection.createStatement()).thenReturn(statement);
        when(statement.executeQuery(any())).thenReturn(resultSet);

        List<Chat> all = chatDao.findAll();
        assertTrue(all.isEmpty());
    }

    @Test
    void findAllByBothUserAndAnnouncement() throws SQLException
    {
        when(connectionPool.getConnection()).thenReturn(connection);
        when(connection.prepareStatement(any())).thenReturn(preparedStatement);
        when(preparedStatement.executeQuery()).thenReturn(resultSet);

        List<Chat> all = chatDao.findAllByBothUserAndAnnouncement(
                UUID.randomUUID(),
                UUID.randomUUID(),
                UUID.randomUUID());
        assertTrue(all.isEmpty());
    }

    @Test
    void update()
    {
        assertFalse(chatDao.update(null));
    }

    @Test
    void findById() throws SQLException
    {
        when(connectionPool.getConnection()).thenReturn(connection);
        when(connection.prepareStatement(any())).thenReturn(preparedStatement);
        when(preparedStatement.executeQuery()).thenReturn(resultSet);

        Chat chat = chatDao.findById(UUID.randomUUID());
        assertNull(chat);
    }

    @Test
    void findAllUserChattersByUser() throws SQLException
    {
        when(connectionPool.getConnection()).thenReturn(connection);
        when(connection.prepareStatement(any())).thenReturn(preparedStatement);
        when(preparedStatement.executeQuery()).thenReturn(resultSet);

        List<Chat> allUserChattersByUser = chatDao.findAllUserChattersByUser(UUID.randomUUID());
        assertTrue(allUserChattersByUser.isEmpty());
    }
}