package uz.pdp.onlineannouncement.dao;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import uz.pdp.onlineannouncement.dao.impl.RatesDaoImpl;
import uz.pdp.onlineannouncement.entity.Rate;
import uz.pdp.onlineannouncement.entity.User;
import uz.pdp.onlineannouncement.pool.ConnectionPool;

import java.sql.*;
import java.util.List;
import java.util.UUID;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.when;

class RatesDaoTest {

    @Mock
    Connection connection;

    @Mock
    ResultSet resultSet;

    @Mock
    ConnectionPool connectionPool;

    @Mock
    PreparedStatement preparedStatement;

    @Mock
    Statement statement;

    @InjectMocks
    RatesDaoImpl ratesDao;

    @BeforeEach
    public void setup()
    {
        MockitoAnnotations.openMocks(this);
    }

    @Test
    void insert() throws SQLException
    {
        when(connectionPool.getConnection()).thenReturn(connection);
        when(connection.prepareStatement(anyString())).thenReturn(preparedStatement);
        when(preparedStatement.executeQuery()).thenReturn(resultSet);

        User toUser = new User();
        toUser.setId(UUID.randomUUID());

        User fromUser = new User();
        fromUser.setId(UUID.randomUUID());

        Rate rate = new Rate();
        rate.setId(UUID.randomUUID());
        rate.setToUser(toUser);
        rate.setFromUser(fromUser);

        boolean insert = ratesDao.insert(rate);
        assertTrue(insert);
    }

    @Test
    void delete() throws SQLException
    {
        when(connectionPool.getConnection()).thenReturn(connection);
        when(connection.prepareStatement(anyString())).thenReturn(preparedStatement);

        boolean delete = ratesDao.delete(UUID.randomUUID());
        assertTrue(delete);
    }

    @Test
    void findAll() throws SQLException
    {
        when(connectionPool.getConnection()).thenReturn(connection);
        when(connection.createStatement()).thenReturn(statement);
        when(statement.executeQuery(any())).thenReturn(resultSet);

        List<Rate> all = ratesDao.findAll();
        assertTrue(all.isEmpty());
    }

    @Test
    void findAllByToUserId() throws SQLException
    {
        when(connectionPool.getConnection()).thenReturn(connection);
        when(connection.prepareStatement(any())).thenReturn(preparedStatement);
        when(preparedStatement.executeQuery()).thenReturn(resultSet);

        List<Rate> rateList = ratesDao.findAllByToUserId(UUID.randomUUID());
        assertTrue(rateList.isEmpty());
    }

    @Test
    void update()
    {
        boolean update = ratesDao.update(new Rate());
        assertFalse(update);
    }

    @Test
    void findById() throws SQLException
    {
        when(connectionPool.getConnection()).thenReturn(connection);
        when(connection.prepareStatement(any())).thenReturn(preparedStatement);
        when(preparedStatement.executeQuery()).thenReturn(resultSet);

        Rate byId = ratesDao.findById(UUID.randomUUID());
        assertNull(byId);
    }
}