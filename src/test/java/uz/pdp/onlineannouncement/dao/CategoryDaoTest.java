package uz.pdp.onlineannouncement.dao;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import uz.pdp.onlineannouncement.dao.impl.CategoryDaoImpl;
import uz.pdp.onlineannouncement.entity.Category;
import uz.pdp.onlineannouncement.pool.ConnectionPool;

import java.sql.*;
import java.util.List;
import java.util.UUID;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.when;

public class CategoryDaoTest {
    @Mock
    Connection connection;

    @Mock
    ResultSet resultSet;

    @Mock
    ConnectionPool connectionPool;

    @Mock
    PreparedStatement preparedStatement;

    @Mock
    Statement statement;

    @InjectMocks
    CategoryDaoImpl categoryDao;

    @BeforeEach
    public void setup()
    {
        MockitoAnnotations.openMocks(this);
    }

    @Test
    public void test_insert() throws SQLException
    {
        when(connectionPool.getConnection()).thenReturn(connection);
        when(connection.prepareStatement(anyString())).thenReturn(preparedStatement);
        when(preparedStatement.executeQuery()).thenReturn(resultSet);

        Category category = new Category();
        category.setId(UUID.randomUUID());
        category.setName("test");
        category.setCreatedAt(new Timestamp(System.currentTimeMillis()));

        boolean insert = categoryDao.insert(category);
        assertTrue(insert);
    }

    @Test
    public void test_delete() throws SQLException
    {
        when(connectionPool.getConnection()).thenReturn(connection);
        when(connection.prepareStatement(anyString())).thenReturn(preparedStatement);

        Category category = new Category();
        category.setId(UUID.randomUUID());

        boolean delete = categoryDao.delete(category);
        assertTrue(delete);
    }

    @Test
    public void test_findAll() throws SQLException {
        when(connectionPool.getConnection()).thenReturn(connection);
        when(connection.createStatement()).thenReturn(statement);
        when(statement.executeQuery(any())).thenReturn(resultSet);

        List<Category> all = categoryDao.findAll();
        assertEquals(0, all.size());
    }

    @Test
    public void test_update() throws SQLException {
        when(connectionPool.getConnection()).thenReturn(connection);
        when(connection.prepareStatement(anyString())).thenReturn(preparedStatement);

        boolean update = categoryDao.update(new Category());
        assertTrue(update);
    }

    @Test
    public void test_findByIdOrName() throws SQLException {
        when(connectionPool.getConnection()).thenReturn(connection);
        when(connection.prepareStatement(anyString())).thenReturn(preparedStatement);
        when(preparedStatement.executeQuery()).thenReturn(resultSet);

        UUID categoryId = UUID.randomUUID();
        Category category = categoryDao.findByIdOrName(categoryId, null);
        assertNotNull(category);
    }
}
