package uz.pdp.onlineannouncement.dao;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import uz.pdp.onlineannouncement.dao.impl.UserDaoImpl;
import uz.pdp.onlineannouncement.entity.User;
import uz.pdp.onlineannouncement.pool.ConnectionPool;

import java.sql.*;
import java.util.List;
import java.util.UUID;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

class UserDaoTest {

    @Mock
    Connection connection;

    @Mock
    ResultSet resultSet;

    @Mock
    ConnectionPool connectionPool;

    @Mock
    PreparedStatement preparedStatement;

    @Mock
    Statement statement;

    @InjectMocks
    UserDaoImpl userDao;

    @BeforeEach
    public void setup()
    {
        MockitoAnnotations.openMocks(this);
    }


    @Test
    void authenticate() throws SQLException
    {
        when(connectionPool.getConnection()).thenReturn(connection);
        when(connection.prepareStatement(any())).thenReturn(preparedStatement);
        when(preparedStatement.executeQuery()).thenReturn(resultSet);

        boolean authenticate = userDao.authenticate("admin", "admin");
        assertFalse(authenticate);
    }

    @Test
    void insert() throws SQLException
    {
        when(connectionPool.getConnection()).thenReturn(connection);
        when(connection.prepareStatement(any())).thenReturn(preparedStatement);

        boolean insert = userDao.insert(new User());
        assertTrue(insert);
    }

    @Test
    void delete()
    {
        boolean delete = userDao.delete(new User());
        assertFalse(delete);
    }

    @Test
    void findAll() throws SQLException {
        when(connectionPool.getConnection()).thenReturn(connection);
        when(connection.createStatement()).thenReturn(statement);
        when(statement.executeQuery(any())).thenReturn(resultSet);

        List<User> all = userDao.findAll();
        assertTrue(all.isEmpty());
    }

    @Test
    void update() throws SQLException
    {
        when(connectionPool.getConnection()).thenReturn(connection);
        when(connection.prepareStatement(any())).thenReturn(preparedStatement);

        boolean update = userDao.update(new User());
        assertTrue(update);
    }

    @Test
    void checkToAdmin() throws SQLException
    {
        when(connectionPool.getConnection()).thenReturn(connection);
        when(connection.prepareStatement(any())).thenReturn(preparedStatement);
        when(preparedStatement.executeQuery()).thenReturn(resultSet);

        boolean admin = userDao.checkToAdmin("user", "user");
        assertFalse(admin);
    }

    @Test
    void checkToActive() throws SQLException
    {
        when(connectionPool.getConnection()).thenReturn(connection);
        when(connection.prepareStatement(any())).thenReturn(preparedStatement);
        when(preparedStatement.executeQuery()).thenReturn(resultSet);

        boolean active = userDao.checkToActive(UUID.randomUUID());
        assertFalse(active);
    }

    @Test
    void findById() throws SQLException
    {
        when(connectionPool.getConnection()).thenReturn(connection);
        when(connection.prepareStatement(any())).thenReturn(preparedStatement);
        when(preparedStatement.executeQuery()).thenReturn(resultSet);

        User byId = userDao.findById(UUID.randomUUID());
        assertNull(byId);
    }

    @Test
    void findByUserName() throws SQLException {
        when(connectionPool.getConnection()).thenReturn(connection);
        when(connection.prepareStatement(any())).thenReturn(preparedStatement);
        when(preparedStatement.executeQuery()).thenReturn(resultSet);

        User userByUserName = userDao.findByEmail("admin");
        assertNull(userByUserName);
    }

    @Test
    void blockOrActive() throws SQLException
    {
        when(connectionPool.getConnection()).thenReturn(connection);
        when(connection.prepareStatement(any())).thenReturn(preparedStatement);

        boolean blockOrActive = userDao.blockOrActive(UUID.randomUUID(), true);
        assertTrue(blockOrActive);
    }

    @Test
    void findAllWithoutUser() throws SQLException {
        when(connectionPool.getConnection()).thenReturn(connection);
        when(connection.prepareStatement(any())).thenReturn(preparedStatement);
        when(preparedStatement.executeQuery()).thenReturn(resultSet);

        List<User> allWithoutUser = userDao.findAllWithoutUser(UUID.randomUUID());
        assertTrue(allWithoutUser.isEmpty());
    }
}