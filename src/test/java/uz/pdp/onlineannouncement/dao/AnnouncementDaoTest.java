package uz.pdp.onlineannouncement.dao;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import uz.pdp.onlineannouncement.dao.impl.AnnouncementDaoImpl;
import uz.pdp.onlineannouncement.entity.Announcement;
import uz.pdp.onlineannouncement.entity.Category;
import uz.pdp.onlineannouncement.entity.User;
import uz.pdp.onlineannouncement.pool.ConnectionPool;

import java.sql.*;
import java.util.List;
import java.util.UUID;

import static org.junit.jupiter.api.Assertions.assertNull;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.when;

class AnnouncementDaoTest {

    @Mock
    Connection connection;

    @Mock
    ResultSet resultSet;

    @Mock
    ConnectionPool connectionPool;

    @Mock
    PreparedStatement preparedStatement;

    @Mock
    Statement statement;

    @InjectMocks
    AnnouncementDaoImpl announcementDao;

    @BeforeEach
    public void setup()
    {
        MockitoAnnotations.openMocks(this);
    }


    @Test
    void test_insert() throws SQLException
    {
        when(connectionPool.getConnection()).thenReturn(connection);
        when(connection.prepareStatement(anyString())).thenReturn(preparedStatement);

        Category category = new Category();
        category.setId(UUID.randomUUID());
        User user = new User();
        user.setId(UUID.randomUUID());
        Announcement announcement = new Announcement();
        announcement.setCategory(category);
        announcement.setUser(user);

        boolean insert = announcementDao.insert(announcement);
        assertTrue(insert);

    }

    @Test
    void test_delete() throws SQLException
    {
        when(connectionPool.getConnection()).thenReturn(connection);
        when(connection.prepareStatement(anyString())).thenReturn(preparedStatement);

        UUID annsId = UUID.randomUUID();
        announcementDao.delete(annsId);
    }

    @Test
    void test_findAll() throws SQLException
    {
        when(connectionPool.getConnection()).thenReturn(connection);
        when(connection.createStatement()).thenReturn(statement);
        when(statement.executeQuery(anyString())).thenReturn(resultSet);

        List<Announcement> all = announcementDao.findAll();
        assertTrue(all.isEmpty());
    }

    @Test
    void test_update() throws SQLException
    {
        when(connectionPool.getConnection()).thenReturn(connection);
        when(connection.prepareStatement(anyString())).thenReturn(preparedStatement);

        Category category = new Category();
        category.setId(UUID.randomUUID());
        User user = new User();
        user.setId(UUID.randomUUID());
        Announcement announcement = new Announcement();
        announcement.setCategory(category);
        announcement.setUser(user);

        boolean update = announcementDao.update(announcement);
        assertTrue(update);
    }

    @Test
    void test_findById() throws SQLException
    {
        when(connectionPool.getConnection()).thenReturn(connection);
        when(connection.prepareStatement(anyString())).thenReturn(preparedStatement);
        when(preparedStatement.executeQuery()).thenReturn(resultSet);

        UUID announcementId = UUID.randomUUID();
        Announcement announcement = announcementDao.findById(announcementId);
        assertNull(announcement);
    }

    @Test
    void test_findAllByApprovedOrBlocked() throws SQLException
    {
        when(connectionPool.getConnection()).thenReturn(connection);
        when(connection.prepareStatement(anyString())).thenReturn(preparedStatement);
        when(preparedStatement.executeQuery()).thenReturn(resultSet);

        List<Announcement> all = announcementDao.findAllByApprovedOrBlocked(true);
        assertTrue(all.isEmpty());
    }

    @Test
    void test_findAllWithoutOwner() throws SQLException
    {
        when(connectionPool.getConnection()).thenReturn(connection);
        when(connection.prepareStatement(anyString())).thenReturn(preparedStatement);
        when(preparedStatement.executeQuery()).thenReturn(resultSet);

        List<Announcement> all = announcementDao.findAllWithoutOwner(UUID.randomUUID());
        assertTrue(all.isEmpty());
    }

    @Test
    void test_findAllByOwner() throws SQLException
    {
        when(connectionPool.getConnection()).thenReturn(connection);
        when(connection.prepareStatement(anyString())).thenReturn(preparedStatement);
        when(preparedStatement.executeQuery()).thenReturn(resultSet);

        List<Announcement> all = announcementDao.findAllByOwner(UUID.randomUUID());
        assertTrue(all.isEmpty());
    }

    @Test
    void test_findAllByCategory() throws SQLException
    {
        when(connectionPool.getConnection()).thenReturn(connection);
        when(connection.prepareStatement(anyString())).thenReturn(preparedStatement);
        when(preparedStatement.executeQuery()).thenReturn(resultSet);

        List<Announcement> all = announcementDao.findAllByCategory(UUID.randomUUID(), UUID.randomUUID());
        assertTrue(all.isEmpty());
    }

    @Test
    void test_setApprovedOrBlocked() throws SQLException
    {
        when(connectionPool.getConnection()).thenReturn(connection);
        when(connection.prepareStatement(anyString())).thenReturn(preparedStatement);

        boolean approvedOrBlocked = announcementDao.setApprovedOrBlocked(UUID.randomUUID(), true);
        assertTrue(approvedOrBlocked);
    }
}