package uz.pdp.onlineannouncement.pool;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.sql.Connection;

import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class ConnectionPoolTest {

    ConnectionPool connectionPool;

    @BeforeEach
    public void setup()
    {
        connectionPool = ConnectionPool.getInstance();
    }

    @Test
    public void test_getConnection()
    {
        Connection connection = connectionPool.getConnection();
        assertNotNull(connection);
        connectionPool.releaseConnection(connection);
    }

    @Test
    public void test_releaseConnection()
    {
        Connection connection1 = connectionPool.getConnection();

        int freeConnectionNumber = connectionPool.getFreeConnectionNumber();
        int usedConnectionNumber = connectionPool.getUsedConnectionNumber();
        connectionPool.releaseConnection(connection1);
        int freeConnectionNumber2 = connectionPool.getFreeConnectionNumber();
        int usedConnectionNumber2 = connectionPool.getUsedConnectionNumber();
        assertTrue(freeConnectionNumber < freeConnectionNumber2 && usedConnectionNumber > usedConnectionNumber2);
    }
}