--------------------------------- User ------------------------------

create table users
(
    id           uuid             not null
        constraint users_pk
            primary key,
    role_name    varchar          not null,
    fio          varchar          not null,
    user_name    varchar          not null,
    email    varchar          not null,
    password     varchar          not null,
    active       boolean,
    phone_number varchar          not null,
    avg_rate     double precision not null,
    created_at   timestamp
    CONSTRAINT "unique-email" UNIQUE (email)
);

create unique index users_user_name_uindex
    on users (user_name);

--------------------------------- Category ---------------------------

create table category
(
    id         uuid      not null
        constraint category_pk
            primary key,
    name       varchar   not null,
    created_at timestamp not null
);

create unique index category_name_uindex
    on category (name);


--------------------------------- Announcement ----------------------

create table announcement
(
    id          uuid             not null
        constraint announcement_pk
            primary key,
    category_id uuid             not null
        constraint announcement_category_id_fk
            references category,
    user_id     uuid             not null
        constraint announcement_users_id_fk
            references users,
    header      varchar          not null,
    definition  varchar          not null,
    price       double precision not null,
    discount    double precision,
    approved    boolean          not null,
    created_at  timestamp        not null
);


--------------------------------- Chat ------------------------------

create table chat
(
    id              uuid      not null
        constraint chat_pk
            primary key,
    announcement_id uuid      not null
        constraint chat_announcement_id_fk
            references announcement,
    sender_id       uuid      not null
        constraint chat_users_id_fk
            references users,
    receiver_id     uuid      not null
        constraint chat_users_id_fk_2
            references users,
    message         varchar   not null,
    date            timestamp not null,
    is_read         boolean   not null,
    is_deleted      boolean   not null
);


--------------------------------- Rates ------------------------------

create table rates
(
    id         uuid             not null
        constraint rates_pk
            primary key,
    to_user    uuid             not null
        constraint rates_users_id_fk
            references users,
    from_user  uuid             not null
        constraint rates_users_id_fk_2
            references users,
    rate       double precision not null,
    created_at timestamp,
    UNIQUE (to_user, from_user)
);


--------------------------------- Comment ------------------------------


create table comment
(
    id                uuid    not null
        constraint comment_pk
            primary key,
    announcement_id   uuid    not null
        constraint comment_announcement_id_fk
            references announcement,
    text              varchar not null,
    parent_comment_id uuid
        constraint comment_comment_id_fk
            references comment,
    user_id           uuid    not null
        constraint comment_users_id_fk
            references users,
    created_at        timestamp
);


--------------------------------- Trigger function ------------------

create function setavgrate() returns trigger
    language plpgsql
as
$$
BEGIN
update users set avg_rate = (select avg(rate) from rates
                             where to_user = NEW.to_user) where id = NEW.to_user;
RETURN NEW;
END;
$$;

------------------------------- Trigger -------------------------------

create trigger setavgratetouser
    after insert
    on rates
    for each row
    execute procedure setavgrate();
