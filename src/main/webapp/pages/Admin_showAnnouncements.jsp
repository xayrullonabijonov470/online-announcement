<%@ page import="uz.pdp.onlineannouncement.entity.Announcement" %>
<%@ page import="uz.pdp.onlineannouncement.command.navigation.AttributeParameterHolder" %>
<%@ page import="java.util.List" %>
<%@ page
        import="static uz.pdp.onlineannouncement.command.navigation.AttributeParameterHolder.PARAMETER_COMMAND_CONTROLLER" %>
<%@ page import="static uz.pdp.onlineannouncement.command.navigation.AttributeParameterHolder.PARAMETER_COMMAND" %>
<%@ page import="static uz.pdp.onlineannouncement.command.navigation.AttributeParameterHolder.*" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>show anss</title>
    <style>
        table {
            border-collapse: collapse;
            width: 100%;
            border: 0 solid #ddd;
        }

        th, td {
            text-align: center;
            padding: 16px;
        }

        .addBtn {
            display: flex;
            justify-content: center;
            align-items: center;
            border: none;
            text-decoration: none;
            transition: 0.3s ease;
            border-radius: 15px;
            background-color: #008CBA;
            color: white;
            width: 150px;
            height: 50px;
            margin-left: 20px;
            font-size: 16px;
            cursor: pointer;
            text-align: center;
        }

        .addBtn:hover {
            transform: scale(0.95);
            color: white;
            text-decoration: none;
        }

        .btn-activate {
            display: flex;
            justify-content: center;
            align-items: center;
            border: none;
            text-decoration: none;
            transition: 0.3s ease;
            border-radius: 15px;
            background-color: #008CBA;
            color: white;
            width: 150px;
            height: 50px;
            margin-left: 20px;
            font-size: 16px;
            cursor: pointer;
            text-align: center;
        }

        .btn-activate:hover {
            transform: scale(0.95);
            color: white;
            text-decoration: none;
        }

        .btn-block {
            display: flex;
            justify-content: center;
            align-items: center;
            border: none;
            text-decoration: none;
            transition: 0.3s ease;
            border-radius: 15px;
            background-color: red;
            color: white;
            width: 150px;
            height: 50px;
            margin-left: 20px;
            font-size: 16px;
            cursor: pointer;
            text-align: center;
        }

        .btn-block:hover {
            transform: scale(0.95);
            color: white;
            text-decoration: none;
        }
    </style>
</head>
<body>

<% List<Announcement> annList = (List<Announcement>) session.getAttribute(AttributeParameterHolder.SESSION_ATTRIBUTE_ANNOUNCEMENT_LIST);
    if (!annList.isEmpty()) { %>

<TABLE cellpadding="5" border="0" style="background-color: #ffffcc; font-size-adjust: initial">
    <tr>
        <th>Category</th>
        <th>Owner</th>
        <th>Header</th>
        <th>Definition</th>
        <th>Price</th>
        <th>Discount</th>
        <th>Approved</th>
        <th>Date</th>
        <th style="resize: block">Action</th>
    </tr>
    <%
        for (Announcement ann : annList) {
            StringBuilder hrefValue = new StringBuilder(PARAMETER_COMMAND_CONTROLLER + "?" + PARAMETER_COMMAND + "=");
            if (ann.isApproved()){
                hrefValue.append(AttributeParameterHolder.PARAMETER_COMMAND_ADMIN_BLOCK_ANNOUNCEMENT);
            }else {
                hrefValue.append(PARAMETER_COMMAND_ADMIN_APPROVE_ANNOUNCEMENT);
            }
    %>
    <TR>
        <TD><%= ann.getCategory().getName()%>
        </TD>
        <TD><%= ann.getUser().getName()%>
        </TD>
        <TD><%= ann.getHeader()%>
        </TD>
        <TD><%= ann.getDefinition()%>
        </TD>
        <TD><%= ann.getPrice()%>
        </TD>
        <TD><%= ann.getDiscount()%>
        </TD>
        <TD><%= ann.isApproved() ? "Approved" : "Waiting for approve ..."%>
        </TD>
        <TD><%= ann.getCreatedAt()%>
        </TD>
        <td>
            <a class="<%= ann.isApproved() ? "btn-block" : "btn-activate"%>" href="<%= hrefValue%>&${AttributeParameterHolder.PARAMETER_ANNOUNCEMENT_ID}=<%=ann.getId()%>">
                <%= ann.isApproved() ? "Block" : "Approve"%>
            </a>
        </td>
    </TR>
    <% } %>
</TABLE>
<% } else { %>
<h1>Don't have any announcement </h1>
<% } %>
</body>
</html>
