<%@ page import="uz.pdp.onlineannouncement.entity.Announcement" %>
<%@ page import="uz.pdp.onlineannouncement.command.navigation.AttributeParameterHolder" %>
<%@ page import="java.util.List" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Title</title>
    <style>
        #div1111 {
            height: calc(100vh - 110px);
            overflow-y: scroll;
        }

        table {
            border-collapse: collapse;
            width: 95%;
            border: 0 solid #ddd;
        }

        th, td {
            text-align: center;
            padding: 16px;
        }

        .addBtn {
            display: flex;
            justify-content: center;
            align-items: center;
            border: none;
            text-decoration: none;
            transition: 0.3s ease;
            border-radius: 15px;
            background-color: #008CBA;
            color: white;
            width: 150px;
            height: 50px;
            margin-left: 20px;
            font-size: 16px;
            cursor: pointer;
            text-align: center;
        }

        .addBtn:hover {
            transform: scale(0.95);
            color: white;
            text-decoration: none;
        }

        .editBtn {
            display: flex;
            justify-content: center;
            align-items: center;
            border: none;
            text-decoration: none;
            transition: 0.3s ease;
            border-radius: 15px;
            background-color: chocolate;
            color: white;
            width: 150px;
            height: 50px;
            margin-left: 20px;
            font-size: 16px;
            cursor: pointer;
            text-align: center;
        }

        .editBtn:hover {
            transform: scale(0.95);
            color: white;
            text-decoration: none;
        }

        .deleteBtn {
            display: flex;
            justify-content: center;
            align-items: center;
            border: none;
            text-decoration: none;
            transition: 0.3s ease;
            border-radius: 15px;
            background-color: red;
            color: white;
            width: 150px;
            height: 50px;
            margin-left: 20px;
            font-size: 16px;
            cursor: pointer;
            text-align: center;
        }

        .deleteBtn:hover {
            transform: scale(0.95);
            color: white;
            text-decoration: none;
        }
    </style>
</head>
<body>

<%
    List<Announcement> addList = (List<Announcement>) session.getAttribute(AttributeParameterHolder.SESSION_ATTRIBUTE_ANNOUNCEMENT_LIST);
    if (!addList.isEmpty()) {
%>
<div id="div1111">
    <TABLE cellpadding="8" border="0"
           style="font-size-adjust: initial; width: 100%">
        <tr>
            <th>Category</th>
            <th>Header</th>
            <th>Definition</th>
            <th>Price</th>
            <th>Discount</th>
            <th>Date</th>
            <th>Status</th>
            <th style="resize: block">Action</th>
        </tr>
        <%
            for (Announcement announcement : addList) {
        %>
        <%--        style="background: <%= user.isActive() ? "rgba(34,62,194,0.8)" : "rgba(194,10,17,0.8)"%>; color: white"--%>
        <TR>
            <TD><%= announcement.getCategory().getName() %>
            </TD>
            <TD><%= announcement.getHeader() %>
            </TD>
            <TD><%= announcement.getDefinition() %>
            </TD>
            <TD><%= announcement.getPrice() %>
            </TD>
            <TD><%= announcement.getDiscount() %>
            </TD>
            <TD><%= announcement.getCreatedAt() %>
            </TD>
            <TD><%= announcement.isApproved() ? "Active" : "Waiting approve..." %>
            </TD>
            <td>
                <a class="editBtn"
                   href="${AttributeParameterHolder.PARAMETER_COMMAND_CONTROLLER}?${AttributeParameterHolder.PARAMETER_COMMAND}=${AttributeParameterHolder.PARAMETER_COMMAND_USER_START_EDIT_ANNOUNCEMENT}&${AttributeParameterHolder.PARAMETER_ANNOUNCEMENT_ID}=<%=announcement.getId()%>">
                     Edit ✏️️
                </a>
                <a class="deleteBtn"
                   href="${AttributeParameterHolder.PARAMETER_COMMAND_CONTROLLER}?${AttributeParameterHolder.PARAMETER_COMMAND}=${AttributeParameterHolder.PARAMETER_COMMAND_USER_BLOCK_ANNOUNCEMENT}&${AttributeParameterHolder.PARAMETER_ANNOUNCEMENT_ID}=<%=announcement.getId()%>">
                     InActivate 🚫
                </a>
            </td>
            <% } %>
        </TR>
    </Table>
    <% } else { %>

    <div >
        <h2>Not have any announcement</h2>
    </div>

    <% } %>
</div>
</body>
</html>
