<%@ page import="uz.pdp.onlineannouncement.command.navigation.AttributeParameterHolder" %>
<%@ page import="uz.pdp.onlineannouncement.entity.Comment" %>
<%@ page import="uz.pdp.onlineannouncement.entity.Announcement" %>
<%@ page import="static uz.pdp.onlineannouncement.command.navigation.AttributeParameterHolder.SESSION_ATTRIBUTE_ANNOUNCEMENT_OWNER" %>
<%@ page import="uz.pdp.onlineannouncement.entity.User" %>
<%@ page import="static uz.pdp.onlineannouncement.command.navigation.AttributeParameterHolder.SESSION_ATTRIBUTE_COMMENT_LIST" %>
<%@ page import="java.util.List" %>
<%@ page import="static uz.pdp.onlineannouncement.command.navigation.AttributeParameterHolder.*" %>
<%@ page import="uz.pdp.onlineannouncement.entity.Rate" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<meta name="viewport" content="width=device-width, initial-scale=1">

<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css"
      integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">

<html>
<head>
    <title>View</title>
    <style>
        #div111 {
            height: calc(100vh);
            /*overflow-y: scroll;*/
        }
    </style>
</head>
<body>

<% Announcement announcement = (Announcement) session.getAttribute(AttributeParameterHolder.SESSION_ATTRIBUTE_ANNOUNCEMENT);
    User owner = (User) session.getAttribute(SESSION_ATTRIBUTE_ANNOUNCEMENT_OWNER); %>
<div id="div111" style="display: flex; flex-direction: row">

    <% if (announcement != null && owner != null) { %>
    <div class="col-sm-6 col-form-label">
        <jsp:include page="User_showAnnouncementInfoAndComments.jsp"/>
    </div>

    <div class="col-sm-6 col-form-label">
        <jsp:include page="User_showUserInfoAndRates.jsp"/>
    </div>
    <% } %>

</div>

</body>
</html>
