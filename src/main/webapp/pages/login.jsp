
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ page import="uz.pdp.onlineannouncement.command.navigation.AttributeParameterHolder" %>

<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css"
      integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
<html>
<head>
    <title>Login Page</title>
    <!-- Add Bootstrap CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
    <!-- Custom CSS for login page -->
    <style>
        body {
            background-color: #f8f9fa;
        }
        .addBtn {
            display: flex;
            justify-content: center;
            align-items: center;
            border: none;
            text-decoration: none;
            transition: 0.3s ease;
            border-radius: 15px;
            background-color: blue;
            color: white;
            width: 150px;
            height: 50px;
            margin-left: 20px;
            font-size: 16px;
            cursor: pointer;
            text-align: center;
        }
        .addBtn:hover {
            transform: scale(0.95);
            color: white;
            text-decoration: none;
        }

        .login-container {
            max-width: 400px;
            margin: 100px auto;
            padding: 20px;
            border-radius: 10px;
            box-shadow: 0 4px 8px rgba(0, 0, 0, 0.1);
            background-color: #ffffff;
        }

        .login-container h2 {
            text-align: center;
            margin-bottom: 30px;
        }

        .login-container .form-group {
            margin-bottom: 20px;
        }

        .login-container .btn-login {
            width: 100%;
        }
    </style>
</head>
<body>
<div class="container">
    <div class="row">
        <div class="col-md-6 offset-md-3">
            <div class="login-container">
                <h2>Login</h2>
                <form action="${pageContext.request.contextPath}/controller" method="post">

                    <input type="hidden" name="${AttributeParameterHolder.PARAMETER_COMMAND}"
                           value="${AttributeParameterHolder.PARAMETER_COMMAND_LOGIN}"/>

                    <div class="form-group">
                        <label for="${AttributeParameterHolder.PARAMETER_USER_EMAIL}">Email:</label>
                        <input
                                type="email"
                                class="form-control"
                                required name="${AttributeParameterHolder.PARAMETER_USER_EMAIL}"
                                placeholder="Enter email:">
                    </div>
                    <div class="form-group">
                        <label for="${AttributeParameterHolder.PARAMETER_USER_PASSWORD}">Password:</label>
                        <input
                                class="form-control"
                                type="password"
                                required name="${AttributeParameterHolder.PARAMETER_USER_PASSWORD}"
                                placeholder="Enter password:">
                    </div>
                    <div style="display: flex; justify-content: right">
                        <a class="addBtn"
                           href="${AttributeParameterHolder.PARAMETER_COMMAND_CONTROLLER}?${AttributeParameterHolder.PARAMETER_COMMAND}=${AttributeParameterHolder.PARAMETER_COMMAND_PRE_REGISTER}">
                            Free register
                        </a>

                        <input class="addBtn" type="submit" value="Enter">
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

<!-- Add Bootstrap JS and jQuery -->
<script src="https://code.jquery.com/jquery-3.5.1.slim.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.9.1/dist/umd/popper.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>
</body>
</html>
