
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ page import="uz.pdp.onlineannouncement.command.navigation.AttributeParameterHolder" %>
<%@ page import="java.util.List" %>
<%@ page import="uz.pdp.onlineannouncement.entity.Category" %>
<%@ page
        import="static uz.pdp.onlineannouncement.command.navigation.AttributeParameterHolder.SESSION_ATTRIBUTE_ANNOUNCEMENT" %>
<%@ page import="uz.pdp.onlineannouncement.entity.Announcement" %>

<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css"
      integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">

<html>
<head>
    <title>Title</title>
    <style>
        .addBtn {
            display: flex;
            justify-content: center;
            align-items: center;
            border: none;
            text-decoration: none;
            transition: 0.3s ease;
            border-radius: 15px;
            background-color: #008CBA;
            color: white;
            width: 150px;
            height: 50px;
            margin-left: 20px;
            font-size: 16px;
            cursor: pointer;
            text-align: center;
        }

        .addBtn:hover {
            transform: scale(0.95);
            color: white;
            text-decoration: none;
        }

        .logout {
            display: flex;
            justify-content: center;
            align-items: center;
            border: none;
            text-decoration: none;
            transition: 0.3s ease;
            border-radius: 15px;
            background-color: chocolate;
            color: white;
            width: 150px;
            height: 50px;
            margin-left: 20px;
            font-size: 16px;
            cursor: pointer;
            text-align: center;
        }

        .logout:hover {
            transform: scale(0.95);
            color: white;
            text-decoration: none;
        }
    </style>
</head>
<body>

<%  List<Category> categoryList = (List<Category>) session.getAttribute(AttributeParameterHolder.SESSION_ATTRIBUTE_CATEGORY_LIST);
    Announcement announcement = (Announcement) session.getAttribute(SESSION_ATTRIBUTE_ANNOUNCEMENT);
    if (announcement != null) { %>

<center>
    <div class="container">
        <h1> Create New Announcement </h1>
        <div class="card">
            <div class="card-body">
                <form action="/controller">

                    <input type="hidden" name="${AttributeParameterHolder.PARAMETER_COMMAND}"
                           value="${AttributeParameterHolder.PARAMETER_COMMAND_USER_FINISH_EDIT_ANNOUNCEMENT}"/>
                    <div class="form-group row form-floating">
                        <label for="${AttributeParameterHolder.PARAMETER_ANNOUNCEMENT_CATEGORY_ID}"
                               class="col-sm-3 col-form-label">Select Category</label>
                        <div class="col-sm-9 form-floating">
                                <select required name="${AttributeParameterHolder.PARAMETER_ANNOUNCEMENT_CATEGORY_ID}"
                                class="form-select w-100 h-75" aria-label="Default select example" id="floatingSelect">
                                    <option value="<%= announcement.getCategory().getId()%>"> <%= announcement.getCategory().getName()%></option>
                                    <% if (!categoryList.isEmpty()){
                                        for (Category category : categoryList) {
                                           if (!category.getName().equals(announcement.getCategory().getName()))%>
                                    <option value="<%= category.getId() %>"> <%= category.getName() %></option>
                                    <% }  } else { %>
                                    <option value=" Is Empty"></option>
                                    <% } %>
                                    <label for="floatingSelect">Select Category</label>
                                </select>
                        </div>
                    </div>

                    <div class="form-group row">
                        <label for="${AttributeParameterHolder.PARAMETER_ANNOUNCEMENT_HEADER}"
                               class="col-sm-3 col-form-label">Header of Announcement</label>
                        <div class="col-sm-9 form-floating">
                            <input
                                    class="form-control"
                                    type="text"
                                    required name="${AttributeParameterHolder.PARAMETER_ANNOUNCEMENT_HEADER}"
                                    value="<%= announcement.getHeader()%>"
                                    placeholder="Enter header:"/>
                        </div>
                    </div>

                    <div class="form-group row">
                        <label for="${AttributeParameterHolder.PARAMETER_ANNOUNCEMENT_DEFINITION}"
                               class="col-sm-3 col-form-label">Definition</label>
                        <div class="form-floating col-sm-9">
                             <textarea
                                    class="form-control"
                                    type="text"
                                    required name="${AttributeParameterHolder.PARAMETER_ANNOUNCEMENT_DEFINITION}"
                                    style="height: 100px"
                                    placeholder="Enter definition:"><%= announcement.getDefinition()%></textarea>
                        </div>
                    </div>

                    <div class="form-group row">
                        <label for="${AttributeParameterHolder.PARAMETER_ANNOUNCEMENT_PRICE}"
                               class="col-sm-3 col-form-label">Price</label>
                        <div class="input-group col-sm-9">
                            <input  class="form-control"
                                    type="number"
                                    value="<%= announcement.getPrice()%>"
                                    min="0"
                                    required name="${AttributeParameterHolder.PARAMETER_ANNOUNCEMENT_PRICE}"
                                    placeholder="Enter price:"/>
                            <span class="input-group-text">So'm</span>
                            <%--                            <span class="input-group-text">0.00</span>--%>
                        </div>
                    </div>

                    <div class="form-group row">
                        <label for="${AttributeParameterHolder.PARAMETER_ANNOUNCEMENT_DISCOUNT}"
                               class="col-sm-3 col-form-label">Discount</label>
                        <div class="input-group col-sm-9">
                            <input  class="form-control"
                                    type="number"
                                    value="<%= announcement.getDiscount()%>"
                                    min="0"
                                    required name="${AttributeParameterHolder.PARAMETER_ANNOUNCEMENT_DISCOUNT}"
                                    placeholder="Enter price:">
                            <span class="input-group-text">So'm</span>
                            <%--                            <span class="input-group-text">0.00</span>--%>
                        </div>
                    </div>

                    <div style="display: flex; justify-content: right">
                        <a class="logout"
                           href="/controller?${AttributeParameterHolder.PARAMETER_COMMAND}=${AttributeParameterHolder.PARAMETER_COMMAND_LOGIN}">
                            Cancel
                        </a>
                        <input class="addBtn" type="submit" value="Save">
                    </div>

                </form>
            </div>
        </div>
    </div>
</center>
<% } %>


</body>
</html>
