
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ page import="uz.pdp.onlineannouncement.command.navigation.AttributeParameterHolder" %>
<%@ page import="uz.pdp.onlineannouncement.entity.Category" %>
<%@ page import="static uz.pdp.onlineannouncement.command.navigation.AttributeParameterHolder.SESSION_ATTRIBUTE_CATEGORY_LIST" %>
<%@ page import="java.util.List" %>
<%@ page import="static uz.pdp.onlineannouncement.command.navigation.AttributeParameterHolder.SESSION_ATTRIBUTE_CATEGORY" %>

<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css"
      integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">

<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>

<html>
<head>
    <link rel="stylesheet" type="text/css" href="css/main.css"/>
    <title>Create category</title>
    <style>
        #div1 {
            height: calc(100vh - 80px);
            overflow-y: scroll;
        }


        table {
            border-collapse: collapse;
            width: 100%;
            border: 0 solid #ddd;
        }

        th, td {
            text-align: center;
            justify-content: center;
            padding: 16px;
        }

        .addBtn {
            display: flex;
            justify-content: center;
            align-items: center;
            border: none;
            text-decoration: none;
            transition: 0.3s ease;
            border-radius: 15px;
            background-color: #008CBA;
            color: white;
            width: 150px;
            height: 50px;
            margin-left: 20px;
            font-size: 16px;
            cursor: pointer;
            text-align: center;
        }

        .addBtn:hover {
            transform: scale(0.95);
            color: white;
            text-decoration: none;
        }

        .btn-edit {
            display: flex;
            justify-content: center;
            align-items: center;
            border: none;
            text-decoration: none;
            transition: 0.3s ease;
            border-radius: 15px;
            background-color: chocolate;
            color: white;
            width: 150px;
            height: 50px;
            margin-left: 20px;
            font-size: 16px;
            cursor: pointer;
            text-align: center;
        }

        .btn-edit:hover {
            transform: scale(0.95);
            color: white;
            text-decoration: none;
        }
    </style>

</head>
<body>

<% List<Category> categoryList = (List<Category>) session.getAttribute(SESSION_ATTRIBUTE_CATEGORY_LIST);
    Category current_category = (Category) session.getAttribute(SESSION_ATTRIBUTE_CATEGORY);
    if (!categoryList.isEmpty()) { %>

<div style="display: flex; position: relative">

    <div class="col-sm-8 col-form-label" id="div1">
        <center>
            <TABLE cellpadding="5" border="0" style="background-color: #ffffcc; font-size-adjust: initial">
                <tr>
                    <th>T/R</th>
                    <th>Name</th>
                    <th>Created At</th>
                    <th style="resize: block">Action</th>
                </tr>
                <% int tr = 0;
                    for (Category category : categoryList) {
                        tr++;
                %>
                <TR>
                    <TD><%= tr %>
                    </TD>
                    <TD><%= category.getName()%>
                    <TD><%= category.getCreatedAt()%>
                    </TD>
                    <td style="justify-items: center">
                        <a class="btn-edit"
                           href="${AttributeParameterHolder.PARAMETER_COMMAND_CONTROLLER}?${AttributeParameterHolder.PARAMETER_COMMAND}=${AttributeParameterHolder.PARAMETER_COMMAND_START_EDIT_CATEGORY}&${AttributeParameterHolder.PARAMETER_CATEGORY_ID}=<%= category.getId() %>">
                            Edit
                        </a>
                    </td>
                </TR>
                <% } %>
            </TABLE>
        </center>
    </div>
    <% } else {%>
    <h1>Don't have any Category yet </h1>
    <% } %>
    <div class="col-sm-4 col-form-label"
         style="position: fixed; top: 100px; right: 0; display: flex; flex-direction: column">
        <h3><%= current_category != null ? "Edit Category " : "Create new Category"%>
        </h3>
        <form class="form-inline" action="${AttributeParameterHolder.PARAMETER_COMMAND_CONTROLLER}">
            <% if (current_category != null) {%>
            <div class="form-group" style="display: flex; flex-direction: row">
                <input type="hidden" name="${AttributeParameterHolder.PARAMETER_COMMAND}"
                       value="${AttributeParameterHolder.PARAMETER_COMMAND_FINISH_EDIT_CATEGORY}">
                <input type="hidden" name="${AttributeParameterHolder.PARAMETER_CATEGORY_ID}"
                       value="<%= current_category.getId()%>">
                <label class="sr-only" for="cat_name"> Category Name :</label>
                <input type="text" class="form-control" id="cat_name"
                       placeholder="Category Name :"
                       value="<%= current_category.getName()%>"
                       required name="${AttributeParameterHolder.PARAMETER_CATEGORY_NAME}">
                <button class="addBtn" style="width: 150px; margin-left: 20px">Edit
                </button>
            </div>
            <% } else { %>
            <div class="form-group" style="display: flex; flex-direction: row">
                <input type="hidden" name="${AttributeParameterHolder.PARAMETER_COMMAND}"
                       value="${AttributeParameterHolder.PARAMETER_COMMAND_ADD_CATEGORY}">
                <label class="sr-only" for="cat_name1"> Category Name :</label>
                <input type="text" class="form-control" id="cat_name1"
                       placeholder="Category Name :"
                       required name="${AttributeParameterHolder.PARAMETER_CATEGORY_NAME}">
                <button class="addBtn" style="width: 150px; margin-left: 20px">Create
                </button>
            </div>
            <% } %>
        </form>
    </div>

</div>

</body>
</html>
