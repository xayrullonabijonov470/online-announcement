<%@ page import="uz.pdp.onlineannouncement.command.navigation.AttributeParameterHolder.*" %>
<%@ page import="static uz.pdp.onlineannouncement.command.navigation.AttributeParameterHolder.PARAMETER_RADIO_KEY" %>
<%@ page import="static uz.pdp.onlineannouncement.command.navigation.AttributeParameterHolder.PARAMETER_RADIO_USERS" %>
<%@ page import="static uz.pdp.onlineannouncement.command.navigation.AttributeParameterHolder.*" %>
<%@ page import="uz.pdp.onlineannouncement.command.navigation.AttributeParameterHolder" %>
<%@ page import="uz.pdp.onlineannouncement.entity.Category" %>
<%@ page import="java.util.List" %>
<%@ page import="java.util.UUID" %>

<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css"
      integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
<link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
<html xmlns:th="http://www.thymeleaf.org" lang="en">

<head>

    <title>Main</title>
    <style>
        #div1 {
            height: calc(100vh - 140px);
            /*overflow-y: scroll;*/
        }

        .href1 {

        }

        table {
            border-collapse: collapse;
            width: 90%;
            border: 1px solid #ddd;
        }

        th, td {
            text-align: center;
            padding: 16px;
        }

        .addBtn {
            display: flex;
            justify-content: center;
            align-items: center;
            border: none;
            text-decoration: none;
            transition: 0.3s ease;
            border-radius: 15px;
            background-color: #4CAF50;
            color: white;
            width: 150px;
            height: 50px;
            margin-left: 20px;
            font-size: 16px;
            cursor: pointer;
            text-align: center;
        }

        .addBtn:hover {
            transform: scale(0.95);
            color: white;
            text-decoration: none;
        }
    </style>

</head>
<body>

<% String valueOfRadioKey = (String) session.getAttribute(PARAMETER_RADIO_KEY);
    String radio_key = AttributeParameterHolder.PARAMETER_RADIO_KEY;
    Category selectedCategory = (Category) session.getAttribute(SESSION_ATTRIBUTE_CATEGORY);
    List<Category> categoryList = (List<Category>) session.getAttribute(AttributeParameterHolder.SESSION_ATTRIBUTE_CATEGORY_LIST);
    String commad = AttributeParameterHolder.PARAMETER_COMMAND;%>

<div style="display: flex; flex-direction: column; width: 100%; height: 100%">
    <div style="height: 70px; width: 100% ; ">

        <div class="w3-bar w3-border"
             style="display: flex; justify-content: space-between; border-radius: 15px; height: 55px;">
            <a href="${pageContext.request.contextPath}/controller?<%= commad%>=${AttributeParameterHolder.PARAMETER_COMMAND_RADIO_COMMAND}&<%= radio_key%>=${AttributeParameterHolder.PARAMETER_RADIO_USERS}"
               class="w3-bar-item w3-button <%= valueOfRadioKey.equals(PARAMETER_RADIO_USERS) ? "w3-green" : "w3-light-grey" %>"
               style="border-radius: 12px; font-size: 20px;text-decoration: none;">All Users</a>
            <a href="${pageContext.request.contextPath}/controller?<%= commad%>=${AttributeParameterHolder.PARAMETER_COMMAND_RADIO_COMMAND}&<%= radio_key%>=${AttributeParameterHolder.PARAMETER_RADIO_NEW_ANNOUNCEMENTS}"
               class="w3-bar-item w3-button <%= valueOfRadioKey.equals(PARAMETER_RADIO_NEW_ANNOUNCEMENTS) ? "w3-green" : "w3-light-grey" %>"
               style="border-radius: 15px; font-size: 20px; text-decoration: none;">New Anns</a>
            <a href="${pageContext.request.contextPath}/controller?<%= commad%>=${AttributeParameterHolder.PARAMETER_COMMAND_RADIO_COMMAND}&<%= radio_key%>=${AttributeParameterHolder.PARAMETER_RADIO_ALL_ANNOUNCEMENTS}"
               class="w3-bar-item w3-button <%= valueOfRadioKey.equals(PARAMETER_RADIO_ALL_ANNOUNCEMENTS) ? "w3-green" : "w3-light-grey" %>"
               style="border-radius: 15px; font-size: 20px; text-decoration: none;">All anns</a>
            <a href="${pageContext.request.contextPath}/controller?<%= commad%>=${AttributeParameterHolder.PARAMETER_COMMAND_RADIO_COMMAND}&<%= radio_key%>=${AttributeParameterHolder.PARAMETER_RADIO_ADMIN_ALL_CATEGORY}"
               class="w3-bar-item w3-button <%= valueOfRadioKey.equals(PARAMETER_RADIO_ADMIN_ALL_CATEGORY) ? "w3-green" : "w3-light-grey" %>"
               style="border-radius: 15px; font-size: 20px; text-decoration: none;">Categories</a>
            <a href="${pageContext.request.contextPath}/controller?<%= commad%>=${AttributeParameterHolder.PARAMETER_COMMAND_RADIO_COMMAND}&<%= radio_key%>=${AttributeParameterHolder.PARAMETER_RADIO_ADMIN_INFO}"
               class="w3-bar-item w3-button <%= valueOfRadioKey.equals(PARAMETER_RADIO_ADMIN_INFO) ? "w3-green" : "w3-light-grey" %>"
               style="border-radius: 15px; font-size: 20px; text-decoration: none;">My info</a>
        </div>
    </div>


    <div id="div1" style="margin-bottom: 20px">
        <div style="text-align: center;">
            <% switch (valueOfRadioKey) {
                case PARAMETER_RADIO_USERS: %>

            <jsp:include page="Admin_showUsers.jsp"/>

            <% break;
                case PARAMETER_RADIO_NEW_ANNOUNCEMENTS:
                case PARAMETER_RADIO_ALL_ANNOUNCEMENTS: %>

            <jsp:include page="Admin_showAnnouncements.jsp"/>

            <% break;
                case PARAMETER_RADIO_ADMIN_ALL_CATEGORY: %>

            <jsp:include page="admin_showOrCreateCategory.jsp"/>

            <% break;
                case PARAMETER_RADIO_ADMIN_INFO:%>

            <jsp:include page="User_showInfo.jsp"/>

            <% break;
            }%>
        </div>
    </div>

    <div style="width: 100%; height: 70px">
        <div style="display: flex; flex-direction: row; width: 70%;margin: auto; justify-content: space-between; ">

            <a class="addBtn"
               href="${AttributeParameterHolder.PARAMETER_COMMAND_CONTROLLER}?${AttributeParameterHolder.PARAMETER_COMMAND}=${AttributeParameterHolder.PARAMETER_COMMAND_PRE_REGISTER}">
                Add New Admin
            </a>

            <a class="addBtn"
               href="${AttributeParameterHolder.PARAMETER_COMMAND_CONTROLLER}?${AttributeParameterHolder.PARAMETER_COMMAND}=${AttributeParameterHolder.PARAMETER_COMMAND_DEFAULT}">
                Default
            </a>

            <a class="addBtn"
               href="${AttributeParameterHolder.PARAMETER_COMMAND_CONTROLLER}?${AttributeParameterHolder.PARAMETER_COMMAND}=${AttributeParameterHolder.PARAMETER_COMMAND_LOGOUT}">
                LogOut
            </a>
        </div>
    </div>
</div>
</body>
</html>