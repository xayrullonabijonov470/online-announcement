<%@ page
        import="static uz.pdp.onlineannouncement.command.navigation.AttributeParameterHolder.SESSION_ATTRIBUTE_CHAT_LIST" %>
<%@ page import="java.util.List" %>
<%@ page import="uz.pdp.onlineannouncement.entity.Chat" %>
<%@ page import="uz.pdp.onlineannouncement.entity.User" %>
<%@ page import="static uz.pdp.onlineannouncement.command.navigation.AttributeParameterHolder.SESSION_ATTRIBUTE_USER" %>
<%@ page import="static uz.pdp.onlineannouncement.command.navigation.AttributeParameterHolder.*" %>
<%@ page import="java.util.HashSet" %>
<%@ page import="java.util.UUID" %>
<%@ page import="uz.pdp.onlineannouncement.command.navigation.AttributeParameterHolder" %>
<%@ page import="uz.pdp.onlineannouncement.entity.Announcement" %>
<%@ page import="java.util.ArrayList" %><%--
  Created by IntelliJ IDEA.
  User: hp
  Date: 11/25/2022
  Time: 9:10 AM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css"
      integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">

<html>
<head>
    <title>Chatting</title>
    <style>
        #div1 {
            height: calc(100vh - 80px);
            overflow-y: scroll;
        }

        #div2 {
            height: calc(100vh - 80px);
            /*overflow-y: scroll;*/
        }

        .addBtn {
            display: flex;
            justify-content: center;
            align-items: center;
            border: none;
            text-decoration: none;
            transition: 0.3s ease;
            border-radius: 15px;
            background-color: #008CBA;
            color: white;
            padding: 15px;
            height: 50px;
            margin-left: 20px;
            font-size: 16px;
            cursor: pointer;
            text-align: center;
        }

        .addBtn:hover {
            transform: scale(0.95);
            color: white;
            text-decoration: none;
        }

        .block {
            display: block;
            width: 100%;
            border: none;
            padding: 25px;
            background-color: #ddd;
            border-radius: 25px;
            color: black;
            /*padding: 14px 28px;*/
            font-size: 16px;
            cursor: pointer;
            text-align: center;
        }

        .block:hover {
            background-color: #04AA6D;
            color: white;
        }

        .block2 {
            display: block;
            width: 100%;
            border: none;
            padding: 25px;
            background-color: #008CBA;
            border-radius: 25px;
            color: white;
            /*padding: 14px 28px;*/
            font-size: 16px;
            cursor: pointer;
            text-align: center;
        }

        .logout {
            display: flex;
            justify-content: center;
            align-items: center;
            border: none;
            text-decoration: none;
            transition: 0.3s ease;
            border-radius: 15px;
            background-color: chocolate;
            color: white;
            width: 150px;
            height: 50px;
            margin-left: 20px;
            font-size: 16px;
            cursor: pointer;
            text-align: center;
        }

        .logout:hover {
            transform: scale(0.95);
            color: white;
            text-decoration: none;
        }

        .btn {
            background-color: DodgerBlue;
            border: none;
            color: white;
            margin: 15px;
            font-size: 30px;
            cursor: pointer;
        }

        .btn:hover {
            background-color: RoyalBlue;
            color: white;
            text-decoration: none;
        }

        .container {
            border: 2px solid #dedede;
            background-color: #f1f1f1;
            border-radius: 8px;
            padding: 15px;
            margin: 10px 0;
        }

        .darker {
            border-color: #ccc;
            margin-right: 10px;
            background-color: #ddd;
        }

        .container::after {
            content: "";
            clear: both;
            display: table;
        }

        .container img {
            float: left;
            max-width: 60px;
            width: 100%;
            margin-right: 20px;
            border-radius: 50%;
        }

        .container img.right {
            float: right;
            margin-left: 20px;
            margin-right: 0;
        }

        .time-right {
            float: right;
            color: #aaa;
        }

        .time-left {
            float: left;
            color: #999;
        }
    </style>
</head>
<body>

<% List<Chat> chatList = (List<Chat>) session.getAttribute(SESSION_ATTRIBUTE_CHAT_USER_CHATTERS);
    Object partnerObj = session.getAttribute(SESSION_ATTRIBUTE_CHAT_PARTNER_ID);
    User currentUser = (User) session.getAttribute(SESSION_ATTRIBUTE_USER); %>

<div style="width: 100% ; height: 80px; display: flex; flex-direction: row; place-content: space-between; background-color: #82d8dd">
    <h1 style="color: #d98a1b; margin-left: 20px; margin-top: 10px">
        Hi, ${sessionScope.get(AttributeParameterHolder.SESSION_ATTRIBUTE_USER_NAME)}</h1>
    <p style="color: red">${sessionScope.get(AttributeParameterHolder.SESSION_ATTRIBUTE_BLOCKED_MSG)}</p>
    <div style="justify-content: space-between; display: flex; flex-direction: row; margin-right: 50px; margin-top: 11px">
        <a class="addBtn"
           style="margin-right: 20px; margin-top: 10px"
           href="${pageContext.request.contextPath}/controller?command=${AttributeParameterHolder.PARAMETER_COMMAND_USER_START_CREATE_ANNOUNCEMENT}">
            Create Announcement
        </a>
        <a style="margin-top: 10px; margin-right: 20px" class="logout"
           href="${AttributeParameterHolder.PARAMETER_COMMAND_CONTROLLER}?${AttributeParameterHolder.PARAMETER_COMMAND}=${AttributeParameterHolder.PARAMETER_COMMAND_LOGOUT}">
            <% session.setAttribute(AttributeParameterHolder.SESSION_ATTRIBUTE_BLOCKED_MSG, null); %>
            LogOut
        </a>
    </div>
</div>
<div id="div2" style="display: flex; flex-direction: row">
    <div id="div1" class="col-sm-3 col-form-label" style="background-color: #ffffcc;">
        <% if (chatList != null && !chatList.isEmpty()) {
            List<UUID> partnerIdList = new ArrayList<>(); %>
        <% for (Chat chat : chatList) {
            boolean currentuserIsReceiver = chat.getReceiver().getId().equals(currentUser.getId());
        %>

        <form action="/controller" method="post" style="width: 100%">
            <input type="hidden" name="${AttributeParameterHolder.PARAMETER_COMMAND}"
                   value="${AttributeParameterHolder.PARAMETER_COMMAND_USER_START_CHATTING}">
            <input type="hidden" name="${AttributeParameterHolder.PARAMETER_CHAT_PARTNER_ID}"
                   value="<%= currentuserIsReceiver ? chat.getSender().getId() : chat.getReceiver().getId()%>">
            <input type="hidden" name="${AttributeParameterHolder.PARAMETER_CHAT_ANNS_ID}"
                   value="<%= chat.getAnnouncement().getId()%>">
            <button class="block">
                <b>
                    <%= currentuserIsReceiver ? chat.getSender().getName() : chat.getReceiver().getName()%>
                </b>
                <br/>
                <b>
                    add owner : <%= chat.getAnnouncement().getUser().getName()%>
                </b>
                <h6>Announcement : <%= chat.getAnnouncement().getHeader()%>
            </button>
        </form>
        <% } %>
        <% } else { %>
        <h2>Not have any Chatters of you </h2>
        <% } %>
    </div>

    <div class="col-md-9 col-form-label">
        <% if (partnerObj != null) { %>

        <jsp:include page="User_showMessages.jsp"/>

        <% } else { %>
        <h2>Please Choose Partner ! </h2>
        <% } %>
    </div>
</div>

</body>
</html>
