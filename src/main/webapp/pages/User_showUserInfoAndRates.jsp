<%@ page import="uz.pdp.onlineannouncement.entity.Announcement" %>
<%@ page import="uz.pdp.onlineannouncement.command.navigation.AttributeParameterHolder" %>
<%@ page import="uz.pdp.onlineannouncement.entity.User" %>
<%@ page import="uz.pdp.onlineannouncement.entity.Rate" %>
<%@ page import="static uz.pdp.onlineannouncement.command.navigation.AttributeParameterHolder.SESSION_ATTRIBUTE_RATES_TO_USER" %>
<%@ page import="static uz.pdp.onlineannouncement.command.navigation.AttributeParameterHolder.*" %>
<%@ page import="java.util.List" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css"
      integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">

<html>
<head>
    <title>user info</title>
    <style>
        #div11 {
            overflow-y: scroll;
            height: calc(100vh - 55%);
        }

        table, th, td {
            border: 0 solid black;
            border-collapse: collapse;
        }

        th, td {
            padding: 5px;
            text-align: left;
        }
        .addBtn {
            display: flex;
            justify-content: center;
            align-items: center;
            border: none;
            text-decoration: none;
            transition: 0.3s ease;
            border-radius: 15px;
            background-color: #008CBA;
            color: white;
            padding: 15px;
            height: 50px;
            margin-left: 20px;
            font-size: 16px;
            cursor: pointer;
            text-align: center;
        }

        .addBtn:hover {
            transform: scale(0.95);
            color: white;
            text-decoration: none;
        }
    </style>
</head>
<body>
<div>
<% Announcement announcement = (Announcement) session.getAttribute(AttributeParameterHolder.SESSION_ATTRIBUTE_ANNOUNCEMENT);
    User owner = (User) session.getAttribute(SESSION_ATTRIBUTE_ANNOUNCEMENT_OWNER);
    List<Rate> ratesToUser = (List<Rate>) session.getAttribute(SESSION_ATTRIBUTE_RATES_TO_USER);
    if (announcement != null && owner != null) { %>

    <div>
        <h4 style="color: chocolate">About Announcement Owner</h4>
        <table style="width:100%">
            <tr>
                <th>FIO:</th>
                <td><%= owner.getName()%>
                </td>
            </tr>
            <tr>
                <th>UserName:</th>
                <td><%= owner.getUsername()%>
                </td>
            </tr>
            <tr>
                <th>Phone Number:</th>
                <td><%= owner.getPhoneNumber()%>
                </td>
            </tr>
            <tr>
                <th>AVG Rates:</th>
                <td><%= owner.getAvgRate()%>
                </td>
            </tr>
            <tr>
                <th>Member since:</th>
                <td><%= owner.getCreatedAt()%>
                </td>
            </tr>
            <tr>
                <th></th>
                <td>
                    <form action="/controller" method="post">
                        <input type="hidden" name="${AttributeParameterHolder.PARAMETER_COMMAND}"
                               value="${AttributeParameterHolder.PARAMETER_COMMAND_USER_START_CHATTING}">
                        <input type="hidden" name="${AttributeParameterHolder.PARAMETER_CHAT_PARTNER_ID}"
                               value="<%= owner.getId()%>">
                        <input type="hidden" name="${AttributeParameterHolder.PARAMETER_CHAT_ANNS_ID}"
                               value="<%= announcement.getId()%>">
                        <label>
                            <button class="addBtn">
                               Send Message
                            </button>
                        </label>
                    </form>
                </td>
            </tr>
        </table>
    </div>
    <h3 style="color: chocolate">All rate about this user</h3>
    <div id="div11">
        <% if (!ratesToUser.isEmpty()) { %>

        <TABLE cellpadding="8" border="0"
               style="font-size-adjust: initial; width: 100%;">
            <tr>
                <th></th>
                <th></th>
                <th></th>
            </tr>
            <%
                for (Rate rate : ratesToUser) {
            %>
            <TR>
                <TD>rate : <%= rate.getRate() %>
                </TD>
                <TD>from : <%= rate.getFromUser().getName() %>
                </TD>
                <TD>date : <%= rate.getCreatedAt() %>
                </TD>
                <% } %>
            </TR>
        </Table>
        <% } else { %>
        <h3>Not Have Any rates</h3>
        <% } %>
    </div>
    <form action="/controller" style="margin-bottom: 0;margin-top : 10px; width: 100%; display: flex; flex-direction: row; justify-content: right" method="post">
        <input type="hidden" name="${AttributeParameterHolder.PARAMETER_COMMAND}"
               value="${AttributeParameterHolder.PARAMETER_COMMAND_USER_LEAVE_RATE}">
        <label style="width: 100%">
            <select required name="${AttributeParameterHolder.PARAMETER_RATE_NUMBER}"
                    class="form-select w-100 h-75" id="rate" >
                <option></option>
                <% session.setAttribute(SESSION_ATTRIBUTE_RATE_TO_USER, announcement.getUser());
                    for (int i = 1; i <= 5; i++) { %>
                <option value="<%= i%>"><%= i%>
                </option>
                <% } %>
                <label for="rate" >Select Rate</label>
            </select>
            Leave rate
        </label>
        <input class="addBtn" type="submit" value="Send"/>
    </form>

<% } %>
</div>
</body>
</html>
