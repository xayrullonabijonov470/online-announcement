<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ page import="uz.pdp.onlineannouncement.command.navigation.AttributeParameterHolder.*" %>
<%@ page import="uz.pdp.onlineannouncement.entity.User" %>
<%@ page import="uz.pdp.onlineannouncement.command.navigation.AttributeParameterHolder" %>
<%@ page import="java.util.List" %>
<%@ page import="static uz.pdp.onlineannouncement.command.navigation.AttributeParameterHolder.PARAMETER_RADIO_USERS" %>
<%@ page import="static uz.pdp.onlineannouncement.command.navigation.AttributeParameterHolder.PARAMETER_RADIO_KEY" %>
<%@ page import="uz.pdp.onlineannouncement.entity.Announcement" %>
<%@ page import="static uz.pdp.onlineannouncement.command.navigation.AttributeParameterHolder.*" %>
<%@ page import="uz.pdp.onlineannouncement.command.navigation.PageNavigation" %>
<%@ page import="uz.pdp.onlineannouncement.entity.Category" %>

<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css"
      integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
<html>
<head>
    <title>UserCabinet</title>
    <style>
        #div1 {
            height: calc(100vh - 95px);
        }

        .btn {
            background-color: DodgerBlue;
            border: none;
            color: white;
            margin: 15px;
            font-size: 30px;
            cursor: pointer;
        }

        .btn:hover {
            background-color: RoyalBlue;
            color: white;
            text-decoration: none;
        }

        .addBtn2 {
            display: flex;
            justify-content: center;
            align-items: center;
            border: none;
            text-decoration: none;
            transition: 0.3s ease;
            border-radius: 15px;
            background-color: #008CBA;
            color: white;
            padding: 15px;
            height: 50px;
            margin-left: 20px;
            font-size: 16px;
            cursor: pointer;
            text-align: center;
        }

        .addBtn2:hover {
            transform: scale(0.95);
            color: white;
            text-decoration: none;
        }

        .logout {
            display: flex;
            justify-content: center;
            align-items: center;
            border: none;
            text-decoration: none;
            transition: 0.3s ease;
            border-radius: 15px;
            background-color: chocolate;
            color: white;
            width: 150px;
            height: 50px;
            margin-left: 20px;
            font-size: 16px;
            cursor: pointer;
            text-align: center;
        }

        .logout:hover {
            transform: scale(0.95);
            color: white;
            text-decoration: none;
        }

        .vertical-menu {
            width: 100%;
        }

        .vertical-menu a {
            background-color: #eee;
            color: black;
            display: block;
            padding: 12px;
            text-decoration: none;
        }

        .vertical-menu a:hover {
            background-color: #ccc;
        }

        .vertical-menu a.active {
            background-color: #04AA6D;
            color: white;
        }
    </style>
</head>
<body>

<% List<Category> categoryList = (List<Category>) session.getAttribute(AttributeParameterHolder.SESSION_ATTRIBUTE_CATEGORY_LIST);
    Category selectedCategory = (Category) session.getAttribute(SESSION_ATTRIBUTE_CATEGORY); %>
<div style="width: 100% ;display: flex; flex-direction: row; place-content: space-between">
    <a href="${pageContext.request.contextPath}/controller?command=${AttributeParameterHolder.PARAMETER_COMMAND_HOME_PAGE}"
       class="btn"><i class="fa fa-home"></i> SMART MARKET</a>
    <h1 style="color: chocolate; margin-left: 20px; margin-top: 10px">
        Hi, ${sessionScope.get(AttributeParameterHolder.SESSION_ATTRIBUTE_USER_NAME)}</h1>
    <p style="color: red">${sessionScope.get(AttributeParameterHolder.SESSION_ATTRIBUTE_BLOCKED_MSG)}</p>
    <div style="justify-content: space-between; display: flex; flex-direction: row; margin-right: 50px; margin-top: 11px">
        <a class="addBtn2"
           style="margin-right: 20px; margin-top: 10px"
           href="${pageContext.request.contextPath}/controller?command=${AttributeParameterHolder.PARAMETER_COMMAND_USER_START_CREATE_ANNOUNCEMENT}">
            Create Announcement
        </a>
        <a style="margin-top: 10px; margin-right: 20px" class="logout"
           href="${AttributeParameterHolder.PARAMETER_COMMAND_CONTROLLER}?${AttributeParameterHolder.PARAMETER_COMMAND}=${AttributeParameterHolder.PARAMETER_COMMAND_LOGOUT}">
            <% session.setAttribute(AttributeParameterHolder.SESSION_ATTRIBUTE_BLOCKED_MSG, null); %>
            LogOut
        </a>
    </div>
</div>
<div id="div1" style="display: flex; flex-direction: row">
    <div class="col-sm-3 col-form-label" style="background-color: #ffffcc">

        <div class="vertical-menu w-100">
            <a href="${pageContext.request.contextPath}/controller?${AttributeParameterHolder.PARAMETER_COMMAND}=${AttributeParameterHolder.PARAMETER_COMMAND_RADIO_COMMAND_USER}&${AttributeParameterHolder.PARAMETER_RADIO_KEY}=${AttributeParameterHolder.PARAMETER_USER_RADIO_ALL_ANNOUNCEMENT}"
                    <%= session.getAttribute(PARAMETER_RADIO_KEY).equals(PARAMETER_USER_RADIO_ALL_ANNOUNCEMENT) ? "class=\"active\"" : "" %> >
                Announcements</a>
            <a href="${pageContext.request.contextPath}/controller?${AttributeParameterHolder.PARAMETER_COMMAND}=${AttributeParameterHolder.PARAMETER_COMMAND_RADIO_COMMAND_USER}&${AttributeParameterHolder.PARAMETER_RADIO_KEY}=${AttributeParameterHolder.PARAMETER_USER_RADIO_MESSAGING}"
                    <%= session.getAttribute(PARAMETER_RADIO_KEY).equals(PARAMETER_USER_RADIO_MESSAGING) ? "class=\"active\"" : "" %> >
                Messaging 💬</a>
            <a href="${pageContext.request.contextPath}/controller?${AttributeParameterHolder.PARAMETER_COMMAND}=${AttributeParameterHolder.PARAMETER_COMMAND_RADIO_COMMAND_USER}&${AttributeParameterHolder.PARAMETER_RADIO_KEY}=${AttributeParameterHolder.PARAMETER_USER_RADIO_OWN_ANNOUNCEMENT}"
                    <%= session.getAttribute(PARAMETER_RADIO_KEY).equals(PARAMETER_USER_RADIO_OWN_ANNOUNCEMENT) ? "class=\"active\"" : "" %>>
                My announcements</a>
            <a href="${pageContext.request.contextPath}/controller?${AttributeParameterHolder.PARAMETER_COMMAND}=${AttributeParameterHolder.PARAMETER_COMMAND_RADIO_COMMAND_USER}&${AttributeParameterHolder.PARAMETER_RADIO_KEY}=${AttributeParameterHolder.PARAMETER_USER_RADIO_INFO}"
                    <%= session.getAttribute(PARAMETER_RADIO_KEY).equals(PARAMETER_USER_RADIO_INFO) ? "class=\"active\"" : "" %>>
                Personal info</a>
        </div>
    </div>

    <div class="col-sm-9 col-form-label">
        <%
            String key = (String) session.getAttribute(AttributeParameterHolder.PARAMETER_RADIO_KEY);
            switch (key) {
                case PARAMETER_USER_RADIO_ALL_ANNOUNCEMENT: %>

        <jsp:include page="user_showAnnouncements.jsp"/>

        <% break;
            case PARAMETER_USER_RADIO_OWN_ANNOUNCEMENT: %>

        <jsp:include page="User_showOwnAnnouncements.jsp"/>

        <% break;
            case PARAMETER_USER_RADIO_INFO: %>

        <jsp:include page="User_showInfo.jsp"/>

        <% break;
        } %>
    </div>

</div>
</body>
</html>
