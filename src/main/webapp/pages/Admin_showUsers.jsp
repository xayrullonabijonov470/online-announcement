<%@ page import="uz.pdp.onlineannouncement.entity.User" %>
<%@ page import="uz.pdp.onlineannouncement.command.navigation.AttributeParameterHolder" %>
<%@ page import="java.util.List" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>showUsers</title>
    <style>

        #div1 {
            height: calc(100vh);
            overflow-y: scroll;
        }

        table {
            border-collapse: collapse;
            width: 100%;
            border: 1px solid #ddd;
        }

        th, td {
            text-align: center;
            justify-content: center;
            padding: 16px;
        }

        .addBtn {
            display: flex;
            justify-content: center;
            align-items: center;
            border: none;
            text-decoration: none;
            transition: 0.3s ease;
            border-radius: 15px;
            background-color: #008CBA;
            color: white;
            width: 150px;
            height: 50px;
            margin-left: 20px;
            font-size: 16px;
            cursor: pointer;
            text-align: center;
        }

        .addBtn:hover {
            transform: scale(0.95);
            color: white;
            text-decoration: none;
        }

        .btn-activate {
            display: flex;
            justify-content: center;
            align-items: center;
            border: none;
            text-decoration: none;
            transition: 0.3s ease;
            border-radius: 15px;
            background-color: #008CBA;
            color: white;
            width: 150px;
            height: 50px;
            margin-left: 20px;
            font-size: 16px;
            cursor: pointer;
            text-align: center;
        }

        .btn-activate:hover {
            transform: scale(0.95);
            color: white;
            text-decoration: none;
        }

        .btn-block {
            display: flex;
            justify-content: center;
            align-items: center;
            border: none;
            text-decoration: none;
            transition: 0.3s ease;
            border-radius: 15px;
            background-color: red;
            color: white;
            width: 150px;
            height: 50px;
            margin-left: 20px;
            font-size: 16px;
            cursor: pointer;
            text-align: center;
        }

        .btn-block:hover {
            transform: scale(0.95);
            color: white;
            text-decoration: none;
        }
    </style>
</head>
<body>


<div>
    <%
        List<User> userList = (List<User>) session.getAttribute(AttributeParameterHolder.SESSION_ATTRIBUTE_USER_LIST);
        if (userList != null) {
    %>
    <TABLE cellpadding="5" border="0.5" style="background-color: #ffffcc; font-size-adjust: initial">
        <tr>
            <th>T/R</th>
            <th>Name</th>
            <th>userName</th>
            <th>Email</th>
            <th>Role</th>
            <th>Status</th>
            <th>Avg Rate</th>
            <th>PhoneNumber</th>
            <th>Created At</th>
            <th style="resize: block">Action</th>
        </tr>
        <%
            int i = 0;
            for (User user : userList) {
                i++ ;
        %>
        <TR>
            <TD><%=i%>
            </TD>
            <TD><%=user.getName()%>
            </TD>
            <TD><%=user.getUsername()%>
            </TD>
            <TD><%=user.getEmail()%>
            </TD>
            <TD><%=user.getRoleName()%>
            </TD>
            <TD><%= user.isActive() ? "ActIve" : "Blocked"%>
            </TD>
            <TD><%=user.getAvgRate()%>
            </TD>
            <TD><%=user.getPhoneNumber()%>
            </TD>
            <TD><%=user.getCreatedAt()%>
            </TD>
            <td style="display: flex; flex-direction: row; border-width: 0">
                <a class="<%= user.isActive() ? "btn-block" : "btn-activate"%>"
                   href="${AttributeParameterHolder.PARAMETER_COMMAND_CONTROLLER}?${AttributeParameterHolder.PARAMETER_COMMAND}=${AttributeParameterHolder.PARAMETER_COMMAND_ADMIN_BLOCK_OR_ACTIVATE_USER}&${AttributeParameterHolder.PARAMETER_USER_ID}=<%=user.getId()%>">
                    <%= user.isActive() ? "Block" : "Activate"%>
                </a>
            </td>
        </TR>
        <% } %>
    </TABLE>
    <% } else { %>
    <h1> Not Have Any User</h1>
    <% } %>
</div>
</body>
</html>
