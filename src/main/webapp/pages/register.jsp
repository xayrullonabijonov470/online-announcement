<%@ page import="uz.pdp.onlineannouncement.entity.enums.RoleName" %>
<%@ page import="uz.pdp.onlineannouncement.command.navigation.AttributeParameterHolder" %>
<%@ page import="static uz.pdp.onlineannouncement.command.navigation.AttributeParameterHolder.*" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css"
      integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">

<%
    String role = (String) request.getSession().getAttribute("role");
    String action = PARAMETER_COMMAND_REGISTER;
    if (role != null) {
        action = (role.equals(RoleName.ADMIN.name())) ? PARAMETER_COMMAND_ADD_ADMIN : PARAMETER_COMMAND_REGISTER;
    }
    request.getSession().setAttribute("action", action);
%>
<html>
<head>
    <style>
        .addBtn:hover {
            width: 100%;
        }

        .addBtn {
            display: block;
            width: 80%;
            height: 80%;
            border: none;
            border-radius: 15px;
            background-color: #008CBA;
            color: white;
            padding: 14px 28px;
            font-size: 16px;
            cursor: pointer;
            text-align: center;
        }

        .addBtn:hover {
            width: 90%;
        }
    </style>
    <title>Register Page</title>
</head>
<body>

<center>
    <div class="container">
        <h1>REGISTRATION FORM</h1>
        <div class="card">
            <div class="card-body">
                <form action="/controller">

                    <input type="hidden" name="${AttributeParameterHolder.PARAMETER_COMMAND}" value="${action}"/>

                    <div class="form-group row">
                        <label for="${AttributeParameterHolder.PARAMETER_USER_FIO}" class="col-sm-2 col-form-label">First
                            Name</label>
                        <div class="col-sm-7">
                            <input
                                    class="form-control"
                                    type="text"
                                    required name="${AttributeParameterHolder.PARAMETER_USER_FIO}"
                                    placeholder="Enter FIO:">
                        </div>
                    </div>

                    <div class="form-group row">
                        <label for="${AttributeParameterHolder.PARAMETER_USER_NAME}" class="col-sm-2 col-form-label">Last
                            Name</label>
                        <div class="col-sm-7">
                            <input
                                    class="form-control"
                                    type="text"
                                    value=""
                                    required name="${AttributeParameterHolder.PARAMETER_USER_NAME}"
                                    placeholder="Enter userName:">
                        </div>
                    </div>

                    <div class="form-group row">
                        <label for="${AttributeParameterHolder.PARAMETER_USER_EMAIL}" class="col-sm-2 col-form-label">Email</label>
                        <div class="col-sm-7">
                            <input
                                    class="form-control"
                                    type="text"
                                    value=""
                                    required name="${AttributeParameterHolder.PARAMETER_USER_EMAIL}"
                                    placeholder="Enter email:">
                        </div>
                    </div>

                    <div class="form-group row">
                        <label for="${AttributeParameterHolder.PARAMETER_USER_PASSWORD}"
                               class="col-sm-2 col-form-label">Password</label>
                        <div class="col-sm-7">
                            <input
                                    class="form-control"
                                    type="password"
                                    value=""
                                    required name="${AttributeParameterHolder.PARAMETER_USER_PASSWORD}"
                                    placeholder="Enter password:"/>
                        </div>
                    </div>

                    <div class="form-group row">
                        <label for="${AttributeParameterHolder.PARAMETER_USER_PHONE_NUMBER}"
                               class="col-sm-2 col-form-label">PhoneNumber</label>
                        <div class="col-sm-7">
                            <input
                                    class="form-control"
                                    type="text"
                                    required name="${AttributeParameterHolder.PARAMETER_USER_PHONE_NUMBER}"
                                    placeholder="Enter Phone Number:">
                        </div>
                    </div>
                    <div style="alignment: right">
                        <table style="width: 50%">
                            <tr>
                                <th>
                                    <a href="${AttributeParameterHolder.PARAMETER_COMMAND_CONTROLLER}?${AttributeParameterHolder.PARAMETER_COMMAND}=${AttributeParameterHolder.PARAMETER_COMMAND_DEFAULT}">
                                        <button class="addBtn" type="button">Already have an account?</button>
                                    </a>
                                </th>
                                <th style="alignment: right">
                                    <input class="addBtn" type="submit" value="Submit">
                                </th>
                            </tr>
                        </table>
                    </div>
                </form>
            </div>
        </div>
    </div>
</center>
</body>
</html>
