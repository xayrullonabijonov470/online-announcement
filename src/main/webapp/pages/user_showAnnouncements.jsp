<%@ page import="uz.pdp.onlineannouncement.entity.Announcement" %>
<%@ page import="uz.pdp.onlineannouncement.command.navigation.AttributeParameterHolder" %>
<%@ page import="java.util.List" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Announcements</title>
    <style>
        #div111 {
            height: calc(100vh - 110px);
            overflow-y: scroll;
        }

        table {
            border-collapse: collapse;
            width: 95%;
            border: 0 solid #ddd;
        }

        th, td {
            text-align: center;
            padding: 16px;
        }

        .addBtn {
            display: flex;
            justify-content: center;
            align-items: center;
            border: none;
            text-decoration: none;
            transition: 0.3s ease;
            border-radius: 15px;
            background-color: #008CBA;
            color: white;
            width: 150px;
            height: 50px;
            margin-left: 20px;
            font-size: 16px;
            cursor: pointer;
            text-align: center;
        }

        .addBtn:hover {
            transform: scale(0.95);
            color: white;
            text-decoration: none;
        }
    </style>
</head>
<body>
<%
    List<Announcement> addList = (List<Announcement>) session.getAttribute(AttributeParameterHolder.SESSION_ATTRIBUTE_ANNOUNCEMENT_LIST);
    if (!addList.isEmpty()) {
%>
<div id="div111">
    <br/>
    <TABLE cellpadding="8" border="0"
           style="font-size-adjust: initial; width: 100%">
        <tr>
            <th>Category</th>
            <th>Header</th>
            <th>Definition</th>
            <th>Price</th>
            <th>Discount</th>
            <th>Date</th>
            <th>User</th>
            <th style="resize: block">Action</th>
        </tr>
        <%
            for (Announcement announcement : addList) {
        %>
        <TR>
            <TD><%= announcement.getCategory().getName() %>
            </TD>
            <TD><%= announcement.getHeader() %>
            </TD>
            <TD><%= announcement.getDefinition() %>
            </TD>
            <TD><%= announcement.getPrice() %>
            </TD>
            <TD><%= announcement.getDiscount() %>
            </TD>
            <TD><%= announcement.getCreatedAt() %>
            </TD>
            <TD><%= announcement.getUser().getName() %>
            </TD>
            <td>
                <form action="/controller" method="post">
                    <a class="addBtn"
                       href="${AttributeParameterHolder.PARAMETER_COMMAND_CONTROLLER}?${AttributeParameterHolder.PARAMETER_COMMAND}=${AttributeParameterHolder.PARAMETER_COMMAND_USER_START_VIEW_ANNOUNCEMENT}&${AttributeParameterHolder.PARAMETER_ANNOUNCEMENT_ID}=<%=announcement.getId()%>">
                        view ↗️
                    </a>
                </form>
            </td>
            <% } %>
        </TR>
    </Table>
    <% } else { %>

    <jsp:text>Not have any Announcement</jsp:text>

    <% } %>
</div>
</body>
</html>
