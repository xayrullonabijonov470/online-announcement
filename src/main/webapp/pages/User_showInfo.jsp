<%@ page import="uz.pdp.onlineannouncement.command.navigation.AttributeParameterHolder" %>
<%@ page import="uz.pdp.onlineannouncement.entity.User" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css"
      integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.3/dist/css/bootstrap.min.css" rel="stylesheet"
      integrity="sha384-rbsA2VBKQhggwzxH7pPCaAqO46MgnOM80zW1RWuH61DGLwZJEdK2Kadq2F9CUG65" crossorigin="anonymous">

<html>
<head>
    <title>Info</title>
    <style>
        .addBtn {
            display: flex;
            justify-content: center;
            align-items: center;
            border: none;
            text-decoration: none;
            transition: 0.3s ease;
            border-radius: 15px;
            background-color: #008CBA;
            color: white;
            padding: 15px;
            width: 150px;
            height: 50px;
            margin-left: 20px;
            font-size: 16px;
            cursor: pointer;
            text-align: center;
        }

        .addBtn:hover {
            transform: scale(0.95);
            color: white;
            text-decoration: none;
        }

    </style>
</head>
<body>

<!-- JavaScript Bundle with Popper -->
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.2.3/dist/js/bootstrap.bundle.min.js"
        integrity="sha384-kenU1KFdBIe4zVF0s0G1M5b4hcpxyD9F7jL+jjXkk+Q2h455rYXK/7HAuoJl+0I4"
        crossorigin="anonymous"></script>
<script src="https://code.jquery.com/jquery-3.6.1.min.js"
        integrity="sha256-o88AwQnZB+VDvE9tvIXrMQaPlFFSUTR+nldQm1LuPXQ=" crossorigin="anonymous"></script>
<%--// kk li--%>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@4.4.1/dist/js/bootstrap.min.js"
        integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6"
        crossorigin="anonymous"></script>

<% User currentUser = (User) session.getAttribute(AttributeParameterHolder.SESSION_ATTRIBUTE_USER); %>
<center>
    <div class="container">
        <h1>Your info</h1>
        <div class="card">
            <div class="card-body">
                <form action="/controller">

                    <input type="hidden" name="${AttributeParameterHolder.PARAMETER_COMMAND}" value="${action}"/>

                    <div class="form-group row">
                        <label for="${AttributeParameterHolder.PARAMETER_USER_FIO}" class="col-sm-3 col-form-label">
                            FIO</label>
                        <div class="col-sm-9">
                            <input
                                    class="form-control"
                                    type="text"
                                    disabled
                                    required name="${AttributeParameterHolder.PARAMETER_USER_FIO}"
                                    value="<%= currentUser.getName()%>"
                                    placeholder="Enter FIO:">
                        </div>
                    </div>

                    <div class="form-group row">
                        <label for="${AttributeParameterHolder.PARAMETER_USER_NAME}" class="col-sm-3 col-form-label">UserName</label>
                        <div class="col-sm-9">
                            <input
                                    class="form-control"
                                    type="text"
                                    disabled
                                    value="<%= currentUser.getUsername()%>"
                                    required name="${AttributeParameterHolder.PARAMETER_USER_NAME}"
                                    placeholder="Enter userName:">
                        </div>
                    </div>

                    <div class="form-group row">
                        <label for="${AttributeParameterHolder.PARAMETER_USER_EMAIL}" class="col-sm-3 col-form-label">Email</label>
                        <div class="col-sm-9">
                            <input
                                    class="form-control"
                                    type="text"
                                    disabled
                                    value="<%= currentUser.getEmail()%>"
                                    required name="${AttributeParameterHolder.PARAMETER_USER_EMAIL}"
                                    placeholder="Enter email:">
                        </div>
                    </div>

                    <div class="form-group row">
                        <label for="${AttributeParameterHolder.PARAMETER_USER_PASSWORD}"
                               class="col-sm-3 col-form-label">Password</label>
                        <div class="col-sm-9">
                            <input
                                    class="form-control"
                                    type="password"
                                    disabled
                                    value="<%= currentUser.getPassword()%>"
                                    required name="${AttributeParameterHolder.PARAMETER_USER_PASSWORD}"
                                    placeholder="Enter password:"/>
                        </div>
                    </div>

                    <div class="form-group row">
                        <label for="${AttributeParameterHolder.PARAMETER_USER_PHONE_NUMBER}"
                               class="col-sm-3 col-form-label">Phone Number</label>
                        <div class="col-sm-9">
                            <input
                                    class="form-control"
                                    disabled
                                    type="text"
                                    value="<%=currentUser.getPhoneNumber()%>"
                                    required name="${AttributeParameterHolder.PARAMETER_USER_PHONE_NUMBER}"
                                    placeholder="Enter Phone Number:">
                        </div>
                    </div>
                    <div style="justify-content: end; display: flex">
                        <%--                        <a href="${AttributeParameterHolder.PARAMETER_COMMAND_CONTROLLER}?${AttributeParameterHolder.PARAMETER_COMMAND}=${AttributeParameterHolder.PARAMETER_COMMAND_DEFAULT}">--%>
                        <%--                            <button class="addBtn" type="button">Edit </button>--%>
                        <%--                        </a>--%>
                        <button type="button" class="btn btn-primary" data-bs-toggle="modal"
                                data-bs-target="#exampleModal" data-bs-whatever="@mdo">Edit
                        </button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</center>

<div class="modal fade" data-bs-backdrop="static" data-bs-keyboard="false" id="exampleModal" tabindex="-1"
     aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h1 class="modal-title fs-5" id="exampleModalLabel">Edit info</h1>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <div class="modal-body">
                <form action="/controller" method="post">

                    <input type="hidden" name="${AttributeParameterHolder.PARAMETER_COMMAND}" value="${AttributeParameterHolder.PARAMETER_COMMAND_EDIT_USER_INFO}"/>

                    <div class="form-group row">
                        <label for="${AttributeParameterHolder.PARAMETER_USER_FIO}" class="col-sm-3 col-form-label">
                            FIO</label>
                        <div class="col-sm-9">
                            <input
                                    class="form-control"
                                    type="text"
                                    required name="${AttributeParameterHolder.PARAMETER_USER_FIO}"
                                    value="<%= currentUser.getName()%>"
                                    placeholder="Enter FIO:">
                        </div>
                    </div>

                    <div class="form-group row">
                        <label for="${AttributeParameterHolder.PARAMETER_USER_NAME}" class="col-sm-3 col-form-label">UserName</label>
                        <div class="col-sm-9">
                            <input
                                    class="form-control"
                                    type="text"
                                    value="<%= currentUser.getUsername()%>"
                                    required name="${AttributeParameterHolder.PARAMETER_USER_NAME}"
                                    placeholder="Enter userName:">
                        </div>
                    </div>

                    <div class="form-group row">
                        <label for="${AttributeParameterHolder.PARAMETER_USER_EMAIL}" class="col-sm-3 col-form-label">Email</label>
                        <div class="col-sm-9">
                            <input
                                    class="form-control"
                                    type="text"
                                    value="<%= currentUser.getEmail()%>"
                                    required name="${AttributeParameterHolder.PARAMETER_USER_EMAIL}"
                                    placeholder="Enter email:">
                        </div>
                    </div>

                    <div class="form-group row">
                        <label for="${AttributeParameterHolder.PARAMETER_USER_PASSWORD}"
                               class="col-sm-3 col-form-label">Password</label>
                        <div class="col-sm-9">
                            <input
                                    class="form-control"
                                    type="password"
                                    value="<%= currentUser.getPassword()%>"
                                    required name="${AttributeParameterHolder.PARAMETER_USER_PASSWORD}"
                                    placeholder="Enter password:"/>
                        </div>
                    </div>

                    <div class="form-group row">
                        <label for="${AttributeParameterHolder.PARAMETER_USER_PHONE_NUMBER}"
                               class="col-sm-3 col-form-label">Phone Number</label>
                        <div class="col-sm-9">
                            <input
                                    class="form-control"
                                    type="text"
                                    value="<%=currentUser.getPhoneNumber()%>"
                                    required name="${AttributeParameterHolder.PARAMETER_USER_PHONE_NUMBER}"
                                    placeholder="Enter Phone Number:">
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-primary">Save Changes</button>
                    </div>
                </form>

            </div>
        </div>
    </div>
</div>

</body>
</html>
