<%@ page import="static uz.pdp.onlineannouncement.command.navigation.AttributeParameterHolder.SESSION_ATTRIBUTE_CHAT_LIST" %>
<%@ page import="uz.pdp.onlineannouncement.entity.Chat" %>
<%@ page import="java.util.List" %>
<%@ page import="static uz.pdp.onlineannouncement.command.navigation.AttributeParameterHolder.*" %>
<%@ page import="uz.pdp.onlineannouncement.entity.User" %>
<%@ page import="uz.pdp.onlineannouncement.command.navigation.AttributeParameterHolder" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css"
      integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
<html>
<head>
    <title>Messages</title>
    <style>

        #div1 {
            overflow-y: scroll;
        }

        #div2 {
            height: calc(100vh - 90px);
            background-color: #eeecec;
        }

        .container {
            border: 2px solid white;
            background-color: white;
            border-radius: 8px;
            width: 60%;
            padding: 15px;
            margin: 10px 0;
        }

        .darker {
            border-color: #ccc;
            margin-right: 10px;
            width: 60%;
            background-color: #d1f4f1;
        }

        .container::after {
            content: "";
            clear: both;
            display: table;
        }

        .container img {
            float: left;
            max-width: 60px;
            width: 100%;
            margin-right: 20px;
            border-radius: 50%;
        }

        .container img.right {
            float: right;
            margin-left: 20px;
            margin-right: 0;
        }

        .time-right {
            float: right;
            color: #aaa;
        }

        .time-left {
            float: left;
            color: #999;
        }
    </style>
</head>
<body>

<div id="div2" style="display: flex; flex-direction: column; width: 100%">
    <div id="div1" class="col-form-label d-flex flex-column" >
        <% List<Chat> chatList = (List<Chat>) session.getAttribute(SESSION_ATTRIBUTE_CHAT_LIST);
            User currentUser = (User) session.getAttribute(SESSION_ATTRIBUTE_USER);
            if (chatList != null && !chatList.isEmpty()) {
                for (Chat chat : chatList) {
                    if (chat.getReceiver().getId().equals(currentUser.getId())) { %>
        <div class="container align-self-start w-55 ">
            <img src="https://freepngimg.com/thumb/chat/158649-chat-icon-download-free-image.png" alt="Avatar"
                 style="width:100%;">
            <p><%= chat.getMessage()%>
            </p>
            <span class="time-right"><%= chat.getCreatedAt()%></span>
            <span class="time-left"><%= chat.getSender().getName()%></span>
        </div>
        <% } else { %>
        <div class="container darker align-self-end w-55" style="margin-right: 10px">
            <img src="https://cryptologos.cc/logos/chatcoin-chat-logo.png?v=023" alt="Avatar" class="right"
                 style="width:100%;">
            <p><%= chat.getMessage()%>
            </p>
            <span class="time-left"><%= chat.getCreatedAt()%></span>
            <span class="time-right">you</span>
        </div>
        <% }
        }
        } else { %>
            <h2>Hey <%= currentUser.getName() %>, You don't have messages with this partner</h2>
        <% } %>
    </div>
    <div class="col-form-label d-flex flex-row w-100" style="width: 100%; margin-bottom: 0">
        <form action="/controller" method="post" style="width: 90%">
            <input type="hidden" name="${AttributeParameterHolder.PARAMETER_COMMAND}"
                   value="${AttributeParameterHolder.PARAMETER_COMMAND_USER_SEND_MESSAGE}">
            <div class="input-group mb-3">
                <textarea class="form-control w-75"
                          required name="${AttributeParameterHolder.PARAMETER_CHAT_MESSAGE}"
                          placeholder="Enter text here!"
                          aria-describedby="button-addon2"
                          style="height: 60px"></textarea>
                <button class="btn btn-outline-secondary bg-success text-light" type="submit" id="button-addon2">Send</button>
            </div>
        </form>
    </div>
</div>
</body>
</html>
