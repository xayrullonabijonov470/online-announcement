<%@ page import="uz.pdp.onlineannouncement.entity.Announcement" %>
<%@ page import="uz.pdp.onlineannouncement.command.navigation.AttributeParameterHolder" %>
<%@ page import="uz.pdp.onlineannouncement.entity.User" %>
<%@ page import="uz.pdp.onlineannouncement.entity.Rate" %>
<%@ page import="static uz.pdp.onlineannouncement.command.navigation.AttributeParameterHolder.SESSION_ATTRIBUTE_RATES_TO_USER" %>
<%@ page import="static uz.pdp.onlineannouncement.command.navigation.AttributeParameterHolder.SESSION_ATTRIBUTE_COMMENT_LIST" %>
<%@ page import="uz.pdp.onlineannouncement.entity.Comment" %>
<%@ page import="static uz.pdp.onlineannouncement.command.navigation.AttributeParameterHolder.*" %>
<%@ page import="java.util.List" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css"
      integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">

<html>
<head>
    <title>Info</title>
    <style>
        #div1 {
            overflow-y: scroll;
            height: calc(100vh - 53%);
        }

        table, th, td {
            border: 0 solid black;
            border-collapse: collapse;
        }

        th, td {
            padding: 5px;
            text-align: left;
        }

        addBtn {
            display: flex;
            justify-content: center;
            align-items: center;
            border: none;
            text-decoration: none;
            transition: 0.3s ease;
            border-radius: 15px;
            background-color: #008CBA;
            color: white;
            width: 150px;
            height: 50px;
            margin-left: 20px;
            font-size: 16px;
            cursor: pointer;
            text-align: center;
        }

        .addBtn:hover {
            transform: scale(0.95);
            color: white;
            text-decoration: none;
        }
    </style>
</head>
<body>
<% Announcement announcement = (Announcement) session.getAttribute(AttributeParameterHolder.SESSION_ATTRIBUTE_ANNOUNCEMENT);
    User owner = (User) session.getAttribute(SESSION_ATTRIBUTE_ANNOUNCEMENT_OWNER);
    List<Comment> commentList = (List<Comment>) session.getAttribute(SESSION_ATTRIBUTE_COMMENT_LIST);
    if (announcement != null && owner != null) { %>
<div>
    <div>
        <h4 style="justify-content: center; color: chocolate">Announcement info</h4>
        <table style="width:100% ">
            <tr>
                <th>Category of Announcement:</th>
                <td><%= announcement.getCategory().getName()%>
                </td>
            </tr>
            <tr>
                <th>Owner:</th>
                <td><%= announcement.getUser().getName()%>
                </td>
            </tr>
            <tr>
                <th>Header:</th>
                <td><%= announcement.getHeader()%>
                </td>
            </tr>
            <tr>
                <th>Definition:</th>
                <td><%= announcement.getDefinition()%>
                </td>
            </tr>
            <tr>
                <th>Price:</th>
                <td><%= announcement.getPrice()%>
                </td>
            </tr>
            <tr>
                <th>Discount:</th>
                <td><%= announcement.getDiscount()%>
                </td>
            </tr>
            <tr>
                <th>Created At:</th>
                <td><%= announcement.getCreatedAt()%>
                </td>
            </tr>
        </table>
    </div>
    <h3 style="color: chocolate">All comments about this announcement</h3>
    <div id="div1">
        <% if (!commentList.isEmpty()) { %>

        <TABLE cellpadding="8" border="0"
               style="font-size-adjust: initial; width: 100%; display: flex">
            <tr>
                <th></th>
                <th></th>
                <th></th>
            </tr>
            <%
                for (Comment comment : commentList) {
            %>
            <TR>
                <TD>message : <%= comment.getText() %>
                </TD>
                <TD>from : <%= comment.getUser().getName() %>
                </TD>
                <TD>date : <%= comment.getCreatedAt() %>
                </TD>
                <% } %>
            </TR>
        </Table>
        <% } else { %>
        <h3>Not Have Any Comment</h3>
        <% } %>
    </div>
    <form action="/controller"style="margin-bottom: 0;margin-top : 10px; width: 100%; display: flex; flex-direction: row; justify-content: end" method="post">
        <input type="hidden" name="${AttributeParameterHolder.PARAMETER_COMMAND}"
               value="${AttributeParameterHolder.PARAMETER_COMMAND_USER_LEAVE_COMMENT}">
        <label style="width: 100%; display: flex; justify-content: end">
            <input style="place-content: space-between; width: 750%" type="text" required
                   name="${AttributeParameterHolder.PARAMETER_COMMENT_TEXT}" placeholder="Leave comment">
            <% session.setAttribute(AttributeParameterHolder.SESSION_ATTRIBUTE_COMMENT_FROM_USER, session.getAttribute(AttributeParameterHolder.SESSION_ATTRIBUTE_USER));
                session.setAttribute(AttributeParameterHolder.SESSION_ATTRIBUTE_COMMENT_TO_ANNOUNCEMENT, announcement); %>
        </label>
        <input class="addBtn" type="submit" value="Send"/>
    </form>
<% } %>
</div>
</body>
</html>
