<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ page import="uz.pdp.onlineannouncement.command.navigation.AttributeParameterHolder" %>
<%@ page import="static uz.pdp.onlineannouncement.command.navigation.AttributeParameterHolder.PARAMETER_USER_NAME" %>
<%@ page import="uz.pdp.onlineannouncement.entity.Announcement" %>
<%@ page import="java.util.List" %>
<%@ page import="uz.pdp.onlineannouncement.service.impl.UserServiceImpl" %>
<%@ page import="uz.pdp.onlineannouncement.service.AnnouncementService" %>
<%@ page import="uz.pdp.onlineannouncement.service.impl.AnnouncementServiceImpl" %>

<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css"
      integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">

<!DOCTYPE html>

<html>
<head>
    <title>Login</title>
    <style>

        #div1 {
            height: calc(100vh - 80px);
            overflow-y: scroll;
        }

        .button2 {
            background-color: white;
            color: black;
            border: 2px solid #008CBA;
            display: flex;
            justify-content: center;
            align-items: center;
            text-decoration: none;
            transition: 0.3s ease;
            border-radius: 15px;
            width: 150px;
            height: 50px;
            margin-left: 20px;
            font-size: 16px;
            cursor: pointer;
            text-align: center;
        }

        .button2:hover {
            background-color: #4CAF50;
            color: white;
            transform: scale(0.95);
            text-decoration: none;
        }

        .addBtn {
            display: flex;
            justify-content: center;
            align-items: center;
            border: none;
            text-decoration: none;
            transition: 0.3s ease;
            border-radius: 15px;
            background-color: #4CAF50;
            color: white;
            width: 150px;
            height: 50px;
            margin-left: 20px;
            font-size: 16px;
            cursor: pointer;
            text-align: center;
        }

        .addBtn:hover {
            transform: scale(0.95);
            color: white;
            text-decoration: none;
        }
    </style>
</head>
<body>

<div>
    <div style="background-color: #31c0c7; width: 100% ;height: 80px; display: flex; flex-direction: row; place-content: space-between">
        <h1 style="color: chocolate; margin-left: 20px; margin-top: 10px">SMART MARKET</h1>
        <div style="justify-content: space-between; display: flex; flex-direction: row; margin-right: 50px; margin-top: 11px">
            <a class="button2"
               style="margin-right: 20px; margin-top: 10px"
               href="${AttributeParameterHolder.PARAMETER_COMMAND_CONTROLLER}?${AttributeParameterHolder.PARAMETER_COMMAND}=${AttributeParameterHolder.PARAMETER_COMMAND_PRE_REGISTER}">
                Free register
            </a>
            <a style="margin-top: 10px; margin-right: 20px" class="button2"
               href="${AttributeParameterHolder.PARAMETER_COMMAND_CONTROLLER}?${AttributeParameterHolder.PARAMETER_COMMAND}=${AttributeParameterHolder.PARAMETER_COMMAND_PRE_LOGIN}">
                LogIn
            </a>
        </div>
    </div>
    <%
        List<Announcement> addList = AnnouncementServiceImpl.getInstance().findAll();
        if (!addList.isEmpty()) {
    %>
    <div id="div1" style="padding-top: 20px">
        <center>
        <TABLE cellpadding="8" border="0"
               style="font-size-adjust: initial; width: 95%">
            <tr>
                <th>T\R</th>
                <th>Category</th>
                <th>Header</th>
                <th>Definition</th>
                <th>Price</th>
                <th>Discount</th>
                <th>Date</th>
                <th>User</th>
                <th style="resize: block">Action</th>
            </tr>
            <%
                int i = 0;
                for (Announcement announcement : addList) {
                    i++;
            %>
            <%--        style="background: <%= user.isActive() ? "rgba(34,62,194,0.8)" : "rgba(194,10,17,0.8)"%>; color: white"--%>
            <TR>
                <TD><%= i%></TD>
                <TD><%= announcement.getCategory().getName() %>
                </TD>
                <TD><%= announcement.getHeader() %>
                </TD>
                <TD><%= announcement.getDefinition() %>
                </TD>
                <TD><%= announcement.getPrice() %>
                </TD>
                <TD><%= announcement.getDiscount() %>
                </TD>
                <TD><%= announcement.getCreatedAt() %>
                </TD>
                <TD><%= announcement.getUser().getName() %>
                </TD>
                <td>
                    <form action="/controller" method="post">
                        <input type="hidden" name="${AttributeParameterHolder.PARAMETER_ANNOUNCEMENT_ID}" value="<%= announcement.getId()%>">
                        <input type="hidden" name="${AttributeParameterHolder.PARAMETER_COMMAND}" value="${AttributeParameterHolder.PARAMETER_COMMAND_LOGIN}">
                        <button class="addBtn" >view ↗</button>
                        </form>
                </td>
                <% } %>
            </TR>
        </Table>

        <% } else { %>

        <jsp:text>Not have any Announcement</jsp:text>

        <% } %>
        </center>
    </div>
</div>
</body>
</html>