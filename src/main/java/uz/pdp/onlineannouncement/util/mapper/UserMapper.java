package uz.pdp.onlineannouncement.util.mapper;

import uz.pdp.onlineannouncement.entity.User;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

public class UserMapper {
    private static final UserMapper userMapper = new UserMapper();

    private UserMapper() {
    }

    public static UserMapper getInstance() {
        return userMapper;
    }

    public static List<User> convertResultSetToUserList(ResultSet resultSet) throws SQLException {
        List<User> users = new ArrayList<>();
        while (resultSet.next()) {
            User user = new User.UserBuilder()
                    .setId((UUID) resultSet.getObject(1))
                    .setRoleName(resultSet.getString(2))
                    .setName(resultSet.getString(3))
                    .setUsername(resultSet.getString(4))
                    .setEmail(resultSet.getString(5))
                    .setPassword(resultSet.getString(6))
                    .setActive(resultSet.getBoolean(7))
                    .setPhoneNumber(resultSet.getString(8))
                    .setAvgRate(Math.round(resultSet.getDouble(9)))
                    .setCreatedAt(resultSet.getTimestamp(10))
                    .build();
            users.add(user);
        }
        return users;
    }
}
