package uz.pdp.onlineannouncement.util.mapper;

import uz.pdp.onlineannouncement.entity.Comment;
import uz.pdp.onlineannouncement.service.AnnouncementService;
import uz.pdp.onlineannouncement.service.UserService;
import uz.pdp.onlineannouncement.service.impl.AnnouncementServiceImpl;
import uz.pdp.onlineannouncement.service.impl.UserServiceImpl;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

public class CommentMapper {
    public static List<Comment> convertResultSetToCommentList(ResultSet resultSet) throws SQLException {
        List<Comment> commentList = new ArrayList<>();
        AnnouncementService announcementService = AnnouncementServiceImpl.getInstance();
        UserService userService = UserServiceImpl.getInstance();
        while (resultSet.next()) {
            Comment comment = new Comment.CommentBuilder()
                    .setId((UUID) resultSet.getObject(1))
                    .setAnnouncement(announcementService.findById((UUID) resultSet.getObject(2)))
                    .setText(resultSet.getString(3))
                    // 4 ParentComment ignored
                    .setUser(userService.findById((UUID) resultSet.getObject(5)))
                    .setCreatedAt(resultSet.getTimestamp(6))
                    .build();
            commentList.add(comment);
        }
        return commentList;
    }
}
