package uz.pdp.onlineannouncement.util.mapper;

import uz.pdp.onlineannouncement.entity.Rate;
import uz.pdp.onlineannouncement.service.UserService;
import uz.pdp.onlineannouncement.service.impl.UserServiceImpl;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

public class RateMapper {

    public static final RateMapper instance = new RateMapper();

    public static RateMapper getInstance() {
        return instance;
    }

    private RateMapper() {
    }

    public static List<Rate> convertResultSetToRateList(ResultSet resultSet) throws SQLException {
        List<Rate> rateList = new ArrayList<>();
        UserService userService = UserServiceImpl.getInstance();
        while (resultSet.next()) {
            Rate rate = new Rate.RateBuilder()
                    .setId((UUID) resultSet.getObject(1))
                    .setToUser(userService.findById((UUID) resultSet.getObject(2)))
                    .setFromUser(userService.findById((UUID) resultSet.getObject(3)))
                    .setRate(resultSet.getDouble(4))
                    .setCreatedAt(resultSet.getTimestamp(5))
                    .build();
            rateList.add(rate);

        }
        return rateList;
    }
}
