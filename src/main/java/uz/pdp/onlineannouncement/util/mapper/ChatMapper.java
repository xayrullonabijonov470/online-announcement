package uz.pdp.onlineannouncement.util.mapper;

import uz.pdp.onlineannouncement.entity.Chat;
import uz.pdp.onlineannouncement.service.AnnouncementService;
import uz.pdp.onlineannouncement.service.UserService;
import uz.pdp.onlineannouncement.service.impl.AnnouncementServiceImpl;
import uz.pdp.onlineannouncement.service.impl.UserServiceImpl;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

public class ChatMapper {

    public static List<Chat> convertResultSetToChatList(ResultSet resultSet) throws SQLException {
        List<Chat> chatList = new ArrayList<>();
        AnnouncementService announcementService = AnnouncementServiceImpl.getInstance();
        UserService userService = UserServiceImpl.getInstance();
        while (resultSet.next()) {
            Chat chat = new Chat.ChatBuilder()
                    .setId((UUID) resultSet.getObject(1))
                    .setAnnouncement(announcementService.findById((UUID) resultSet.getObject(2)))
                    .setSender(userService.findById((UUID) resultSet.getObject(3)))
                    .setReceiver(userService.findById((UUID) resultSet.getObject(4)))
                    .setMessage(resultSet.getString(5))
                    .setCreatedAt(resultSet.getTimestamp(6))
                    .setRead(resultSet.getBoolean(7))
                    .setDeleted(resultSet.getBoolean(8))
                    .build();
            chatList.add(chat);
        }
        return chatList;
    }
}
