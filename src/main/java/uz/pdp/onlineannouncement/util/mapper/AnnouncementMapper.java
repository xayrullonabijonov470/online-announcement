package uz.pdp.onlineannouncement.util.mapper;

import uz.pdp.onlineannouncement.entity.Announcement;
import uz.pdp.onlineannouncement.entity.Category;
import uz.pdp.onlineannouncement.entity.User;
import uz.pdp.onlineannouncement.service.CategoryService;
import uz.pdp.onlineannouncement.service.UserService;
import uz.pdp.onlineannouncement.service.impl.CategoryServiceImpl;
import uz.pdp.onlineannouncement.service.impl.UserServiceImpl;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

public class AnnouncementMapper
{

    public static List<Announcement> convertResultSetToAnnouncement(ResultSet resultSet) throws SQLException
    {
        List<Announcement> list = new ArrayList<>();
        while (resultSet.next())
        {
            Announcement announcement = new Announcement();
            announcement.setId((UUID) resultSet.getObject(1));       // set id
            UUID categoryId = (UUID) resultSet.getObject(2);

            CategoryService categoryService = CategoryServiceImpl.getInstance();
            Category category = categoryService.findByIdOrName(categoryId, null);

            announcement.setCategory(category);                                // set category

            UUID userId = (UUID) resultSet.getObject(3);
            UserService userService = UserServiceImpl.getInstance();
            User user = userService.findById(userId);

            announcement.setUser(user);                                        // set user
            announcement.setHeader(resultSet.getString(4));          // set header
            announcement.setDefinition(resultSet.getString(5));      // set definition
            announcement.setPrice(resultSet.getDouble(6));           // set price
            announcement.setDiscount(resultSet.getDouble(7));        // set discount
            announcement.setApproved(resultSet.getBoolean(8));       // set approved
            announcement.setCreatedAt(resultSet.getTimestamp(9));              // set date

            list.add(announcement);                                            // add to list
        }
        return list;
    }
}
