package uz.pdp.onlineannouncement.entity;

import java.sql.Timestamp;
import java.util.UUID;

public class User extends AbstractEntity {
    private String roleName;
    private String username;
    private String email;
    private String password;
    private String phoneNumber;
    private boolean active;
    private double avgRate;

    public User() {

    }

    public User(UUID id, String name, String roleName, String username, String email, String password, String phoneNumber, boolean active, double avgRate) {
        super(id, name);
        this.roleName = roleName;
        this.username = username;
        this.email = email;
        this.password = password;
        this.phoneNumber = phoneNumber;
        this.active = active;
        this.avgRate = avgRate;
    }

    public User(UUID id, String name, Timestamp createdAt, String roleName, String username, String email, String password, String phoneNumber, boolean active, double avgRate) {
        super(id, name, createdAt);
        this.roleName = roleName;
        this.username = username;
        this.email = email;
        this.password = password;
        this.phoneNumber = phoneNumber;
        this.active = active;
        this.avgRate = avgRate;
    }

    public User(String roleName, String username, String email, String password, String phoneNumber, boolean active, double avgRate) {
        this.roleName = roleName;
        this.username = username;
        this.email = email;
        this.password = password;
        this.phoneNumber = phoneNumber;
        this.active = active;
        this.avgRate = avgRate;
    }

    public User(UUID id, String name, String email, String roleName, String username, String password, String phoneNumber, boolean active, double avgRate, Timestamp createdAt) {
        super(id, name, createdAt);
        this.roleName = roleName;
        this.username = username;
        this.email=email;
        this.password = password;
        this.phoneNumber = phoneNumber;
        this.active = active;
        this.avgRate = avgRate;
    }

    public String getRoleName() {
        return roleName;
    }

    public void setRoleName(String roleName) {
        this.roleName = roleName;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public boolean isActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }

    public double getAvgRate() {
        return avgRate;
    }

    public void setAvgRate(double avgRate) {
        this.avgRate = avgRate;
    }

    public static class UserBuilder {
        private final User user;

        public UserBuilder() {
            user = new User();
        }

        public UserBuilder setId(UUID id) {
            user.setId(id);
            return this;
        }

        public UserBuilder setName(String name) {
            user.setName(name);
            return this;
        }

        public UserBuilder setCreatedAt(Timestamp createdAt) {
            user.setCreatedAt(createdAt);
            return this;
        }

        public UserBuilder setRoleName(String roleName) {
            user.setRoleName(roleName);
            return this;
        }

        public UserBuilder setUsername(String username) {
            user.setUsername(username);
            return this;
        }

        public UserBuilder setEmail(String email){
            user.setEmail(email);
            return this;
        }

        public UserBuilder setPassword(String password) {
            user.setPassword(password);
            return this;
        }

        public UserBuilder setPhoneNumber(String phoneNumber) {
            user.setPhoneNumber(phoneNumber);
            return this;
        }

        public UserBuilder setActive(boolean active) {
            user.setActive(active);
            return this;
        }

        public UserBuilder setAvgRate(double avgRate) {
            user.setAvgRate(avgRate);
            return this;
        }

        public User build() {
            return user;
        }
    }

}
