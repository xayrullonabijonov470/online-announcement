package uz.pdp.onlineannouncement.entity;

import java.sql.Timestamp;
import java.util.UUID;

public class Rate extends AbstractEntity {
    private User toUser;
    private User fromUser;
    private double rate;

    public Rate(UUID id, String name, User toUser, User fromUser, double rate, Timestamp timestamp) {
        super(id, name, timestamp);
        this.toUser = toUser;
        this.fromUser = fromUser;
        this.rate = rate;
    }

    public Rate() {

    }

    public User getToUser() {
        return toUser;
    }

    public void setToUser(User toUser) {
        this.toUser = toUser;
    }

    public User getFromUser() {
        return fromUser;
    }

    public void setFromUser(User fromUser) {
        this.fromUser = fromUser;
    }

    public double getRate() {
        return rate;
    }

    public void setRate(double rate) {
        this.rate = rate;
    }

    public static class RateBuilder {
        private final Rate rate;

        public RateBuilder() {
            rate = new Rate();
        }

        public RateBuilder setId(UUID id) {
            rate.setId(id);
            return this;
        }

        public RateBuilder setName(String name) {
            rate.setName(name);
            return this;
        }

        public RateBuilder setCreatedAt(Timestamp createdAt) {
            rate.setCreatedAt(createdAt);
            return this;
        }

        public RateBuilder setToUser(User toUser) {
            rate.setToUser(toUser);
            return this;
        }

        public RateBuilder setFromUser(User fromUser) {
            rate.setFromUser(fromUser);
            return this;
        }

        public RateBuilder setRate(double rate) {
            this.rate.setRate(rate);
            return this;
        }

        public Rate build() {
            return rate;
        }
    }

}
