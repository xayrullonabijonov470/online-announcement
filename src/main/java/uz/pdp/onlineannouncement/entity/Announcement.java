package uz.pdp.onlineannouncement.entity;


import java.sql.Timestamp;
import java.util.UUID;

public class Announcement extends AbstractEntity {
    private Category category;
    private User user;
    private String header;
    private String definition;
    private double price;
    private double discount;
    private boolean approved;


    public Announcement() {

    }

    public Announcement(UUID id, Category category, User user, String header, String definition, double price, double discount, boolean approved) {
        super(id, null);
        this.category = category;
        this.user = user;
        this.header = header;
        this.definition = definition;
        this.price = price;
        this.discount = discount;
        this.approved = approved;
    }

    public Announcement(Category category, User user, String header, String definition, double price, double discount, boolean approved) {
        this.category = category;
        this.user = user;
        this.header = header;
        this.definition = definition;
        this.price = price;
        this.discount = discount;
        this.approved = approved;

    }

    public Announcement(UUID id, Category category, String header, String definition, double price, double discount) {
        super(id, null);
        this.category = category;
        this.header = header;
        this.definition = definition;
        this.price = price;
        this.discount = discount;
    }

    public Announcement(UUID id, String name, Timestamp createdAt, Category category, User user, String header, String definition, double price, double discount, boolean approved) {
        super(id, name, createdAt);
        this.category = category;
        this.user = user;
        this.header = header;
        this.definition = definition;
        this.price = price;
        this.discount = discount;
        this.approved = approved;
    }

    public Category getCategory() {
        return category;
    }

    public void setCategory(Category category) {
        this.category = category;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public String getHeader() {
        return header;
    }

    public void setHeader(String header) {
        this.header = header;
    }

    public String getDefinition() {
        return definition;
    }

    public void setDefinition(String definition) {
        this.definition = definition;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public double getDiscount() {
        return discount;
    }

    public void setDiscount(double discount) {
        this.discount = discount;
    }

    public boolean isApproved() {
        return approved;
    }

    public void setApproved(boolean approved) {
        this.approved = approved;
    }

    public static class AnnouncementBuilder {
        private final Announcement announcement;

        public AnnouncementBuilder() {
            announcement = new Announcement();
        }

        public AnnouncementBuilder setId(UUID id) {
            announcement.setId(id);
            return this;
        }

        public AnnouncementBuilder setName(String name) {
            announcement.setName(name);
            return this;
        }

        public AnnouncementBuilder setCreatedAt(Timestamp createdAt) {
            announcement.setCreatedAt(createdAt);
            return this;
        }

        public AnnouncementBuilder setCategory(Category category) {
            announcement.setCategory(category);
            return this;
        }

        public AnnouncementBuilder setUser(User user) {
            announcement.setUser(user);
            return this;
        }

        public AnnouncementBuilder setHeader(String header) {
            announcement.setHeader(header);
            return this;
        }

        public AnnouncementBuilder setDefinition(String definition) {
            announcement.setDefinition(definition);
            return this;
        }

        public AnnouncementBuilder setPrice(Double price) {
            announcement.setPrice(price);
            return this;
        }

        public AnnouncementBuilder setDiscount(Double discount) {
            announcement.setDiscount(discount);
            return this;
        }

        public AnnouncementBuilder setApproved(boolean approved) {
            announcement.setApproved(approved);
            return this;
        }

        public Announcement build() {
            return announcement;
        }
    }
}
