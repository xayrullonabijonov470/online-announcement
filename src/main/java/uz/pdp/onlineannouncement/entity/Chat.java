package uz.pdp.onlineannouncement.entity;

import java.sql.Timestamp;
import java.util.UUID;

public class Chat extends AbstractEntity {
    private Announcement announcement;
    private User sender;
    private User receiver;
    private String message;
    private boolean read;
    private boolean deleted;

    public Chat(UUID id, String name, Announcement announcement, User sender, User receiver, String message, boolean read, boolean deleted) {
        super(id, name);
        this.announcement = announcement;
        this.sender = sender;
        this.receiver = receiver;
        this.message = message;
        this.read = read;
        this.deleted = deleted;
    }

    public Chat(UUID id, String name, Timestamp createdAt, Announcement announcement, User sender, User receiver, String message, boolean read, boolean deleted) {
        super(id, name, createdAt);
        this.announcement = announcement;
        this.sender = sender;
        this.receiver = receiver;
        this.message = message;
        this.read = read;
        this.deleted = deleted;
    }

    public Chat() {

    }

    public Announcement getAnnouncement() {
        return announcement;
    }

    public void setAnnouncement(Announcement announcement) {
        this.announcement = announcement;
    }

    public User getSender() {
        return sender;
    }

    public void setSender(User sender) {
        this.sender = sender;
    }

    public User getReceiver() {
        return receiver;
    }

    public void setReceiver(User receiver) {
        this.receiver = receiver;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public boolean isRead() {
        return read;
    }

    public void setRead(boolean read) {
        this.read = read;
    }

    public boolean isDeleted() {
        return deleted;
    }

    public void setDeleted(boolean deleted) {
        this.deleted = deleted;
    }

    public static class ChatBuilder {
        private final Chat chat;

        public ChatBuilder() {
            chat = new Chat();
        }

        public ChatBuilder setId(UUID id) {
            chat.setId(id);
            return this;
        }

        public ChatBuilder setName(String name) {
            chat.setName(name);
            return this;
        }

        public ChatBuilder setCreatedAt(Timestamp createdAt) {
            chat.setCreatedAt(createdAt);
            return this;
        }

        public ChatBuilder setAnnouncement(Announcement announcement) {
            chat.setAnnouncement(announcement);
            return this;
        }

        public ChatBuilder setSender(User sender) {
            chat.setSender(sender);
            return this;
        }

        public ChatBuilder setReceiver(User receiver) {
            chat.setReceiver(receiver);
            return this;
        }

        public ChatBuilder setMessage(String message) {
            chat.setMessage(message);
            return this;
        }

        public ChatBuilder setRead(boolean read) {
            chat.setRead(read);
            return this;
        }

        public ChatBuilder setDeleted(boolean deleted) {
            chat.setDeleted(deleted);
            return this;
        }

        public Chat build() {
            return chat;
        }
    }

}
