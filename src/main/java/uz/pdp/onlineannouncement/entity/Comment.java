package uz.pdp.onlineannouncement.entity;

import java.sql.Timestamp;
import java.util.UUID;

public class Comment extends AbstractEntity {
    private Announcement announcement;
    private String text;
    private Comment comment;
    private User user;

    public Comment(UUID id, String name, Announcement announcement, String text, Comment comment, User user) {
        super(id, name);
        this.announcement = announcement;
        this.text = text;
        this.comment = comment;
        this.user = user;
    }

    public Comment(UUID id, String name, Timestamp createdAt, Announcement announcement, String text, Comment comment, User user) {
        super(id, name, createdAt);
        this.announcement = announcement;
        this.text = text;
        this.comment = comment;
        this.user = user;
    }

    public Comment() {

    }

    public Announcement getAnnouncement() {
        return announcement;
    }

    public void setAnnouncement(Announcement announcement) {
        this.announcement = announcement;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public Comment getComment() {
        return comment;
    }

    public void setComment(Comment comment) {
        this.comment = comment;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public static class CommentBuilder {
        private final Comment comment;

        public CommentBuilder() {
            comment = new Comment();
        }

        public CommentBuilder setId(UUID id) {
            comment.setId(id);
            return this;
        }

        public CommentBuilder setName(String name) {
            comment.setName(name);
            return this;
        }

        public CommentBuilder setCreatedAt(Timestamp createdAt) {
            comment.setCreatedAt(createdAt);
            return this;
        }

        public CommentBuilder setAnnouncement(Announcement announcement) {
            comment.setAnnouncement(announcement);
            return this;
        }

        public CommentBuilder setText(String text) {
            comment.setText(text);
            return this;
        }

        public CommentBuilder setComment(Comment comment) {
            comment.setComment(comment);
            return this;
        }

        public CommentBuilder setUser(User user) {
            comment.setUser(user);
            return this;
        }

        public Comment build() {
            return comment;
        }
    }

}
