package uz.pdp.onlineannouncement.entity;


import java.sql.Timestamp;
import java.util.UUID;

public class Category extends AbstractEntity{

    public static class CategoryBuilder {
        private final Category category;

        public CategoryBuilder() {
            category = new Category();
        }

        public CategoryBuilder setId(UUID id) {
            category.setId(id);
            return this;
        }

        public CategoryBuilder setName(String name) {
            category.setName(name);
            return this;
        }

        public CategoryBuilder setCreatedAt(Timestamp createdAt) {
            category.setCreatedAt(createdAt);
            return this;
        }

        public Category build() {
            return category;
        }
    }
}
