package uz.pdp.onlineannouncement.command;

import uz.pdp.onlineannouncement.command.navigation.PageNavigation;

public enum PreCommandType {
    PRE_CREATE_ANNOUNCEMENT(PageNavigation.PAGE_USER_CREATE_ANNOUNCEMENT),
    PRE_REGISTER(PageNavigation.PAGE_REGISTER),
    PRE_LOGIN(PageNavigation.PAGE_LOGIN);

    public final String page;

    PreCommandType(String page) {
        this.page = page;
    }
}
