package uz.pdp.onlineannouncement.command.impl.userCommand;

import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpSession;
import uz.pdp.onlineannouncement.command.Command;
import uz.pdp.onlineannouncement.command.navigation.Router;
import uz.pdp.onlineannouncement.entity.Announcement;
import uz.pdp.onlineannouncement.entity.Comment;
import uz.pdp.onlineannouncement.entity.User;
import uz.pdp.onlineannouncement.service.CommentService;
import uz.pdp.onlineannouncement.service.impl.CommentServiceImpl;

import java.sql.Timestamp;
import java.util.UUID;

import static uz.pdp.onlineannouncement.command.navigation.AttributeParameterHolder.*;
import static uz.pdp.onlineannouncement.command.navigation.PageNavigation.PAGE_USER_VIEW_ANNOUNCEMENT;
import static uz.pdp.onlineannouncement.command.navigation.Router.PageChangeType.REDIRECT;

public class User_LeaveComment implements Command {
    @Override
    public Router execute(HttpServletRequest request) {
        HttpSession session = request.getSession();
        String text = request.getParameter(PARAMETER_COMMENT_TEXT);
        Announcement announcement = (Announcement) session.getAttribute(SESSION_ATTRIBUTE_COMMENT_TO_ANNOUNCEMENT);
        User fromUser = (User) session.getAttribute(SESSION_ATTRIBUTE_COMMENT_FROM_USER);

        Comment comment = new Comment.CommentBuilder()
                .setId(UUID.randomUUID())
                .setName(text)
                .setCreatedAt(new Timestamp(System.currentTimeMillis()))
                .setAnnouncement(announcement)
                .setText(text)
                .setComment(null)
                .setUser(fromUser)
                .build();

        CommentService commentService = CommentServiceImpl.getInstance();

        commentService.insert(comment);

        User_StartViewAnnouncements user_startViewAnnouncements = new User_StartViewAnnouncements();
        user_startViewAnnouncements.refresh(request);

        return new Router(PAGE_USER_VIEW_ANNOUNCEMENT, REDIRECT);
    }
}
