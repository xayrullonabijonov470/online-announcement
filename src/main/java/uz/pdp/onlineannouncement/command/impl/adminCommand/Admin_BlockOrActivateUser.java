package uz.pdp.onlineannouncement.command.impl.adminCommand;

import jakarta.servlet.http.HttpServletRequest;
import uz.pdp.onlineannouncement.command.Command;
import uz.pdp.onlineannouncement.command.navigation.Router;
import uz.pdp.onlineannouncement.entity.User;
import uz.pdp.onlineannouncement.service.UserService;
import uz.pdp.onlineannouncement.service.impl.UserServiceImpl;

import java.util.UUID;

import static uz.pdp.onlineannouncement.command.navigation.AttributeParameterHolder.PARAMETER_USER_ID;
import static uz.pdp.onlineannouncement.command.navigation.PageNavigation.PAGE_ADMIN_HOME_PAGE;
import static uz.pdp.onlineannouncement.command.navigation.Router.PageChangeType.REDIRECT;

public class Admin_BlockOrActivateUser implements Command
{
    @Override
    public Router execute(HttpServletRequest request) {
        UUID userId = UUID.fromString(request.getParameter(PARAMETER_USER_ID));

        UserService userService = UserServiceImpl.getInstance();
        User userById = userService.findById(userId);

        userService.blockOrActive(userById.getId(), !userById.isActive());

        AdminRadioCommands adminRadioCommands = new AdminRadioCommands();
        adminRadioCommands.refreshAllUsers(request);

        return new Router(PAGE_ADMIN_HOME_PAGE, REDIRECT);
    }
}
