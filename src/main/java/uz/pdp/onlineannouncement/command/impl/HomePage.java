package uz.pdp.onlineannouncement.command.impl;

import jakarta.servlet.http.HttpServletRequest;
import uz.pdp.onlineannouncement.command.Command;
import uz.pdp.onlineannouncement.command.navigation.Router;

import static uz.pdp.onlineannouncement.command.navigation.PageNavigation.PAGE_DEFAULT;
import static uz.pdp.onlineannouncement.command.navigation.Router.PageChangeType.FORWARD;

public class HomePage implements Command {
    @Override
    public Router execute(HttpServletRequest request) {
        return new Router(PAGE_DEFAULT, FORWARD);
    }
}
