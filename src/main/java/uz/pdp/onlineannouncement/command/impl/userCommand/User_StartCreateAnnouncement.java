package uz.pdp.onlineannouncement.command.impl.userCommand;

import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpSession;
import uz.pdp.onlineannouncement.command.Command;
import uz.pdp.onlineannouncement.command.navigation.AttributeParameterHolder;
import uz.pdp.onlineannouncement.command.navigation.Router;
import uz.pdp.onlineannouncement.entity.Category;
import uz.pdp.onlineannouncement.entity.User;
import uz.pdp.onlineannouncement.service.CategoryService;
import uz.pdp.onlineannouncement.service.UserService;
import uz.pdp.onlineannouncement.service.impl.CategoryServiceImpl;
import uz.pdp.onlineannouncement.service.impl.UserServiceImpl;

import java.util.List;

import static uz.pdp.onlineannouncement.command.navigation.AttributeParameterHolder.SESSION_ATTRIBUTE_BLOCKED_MSG;
import static uz.pdp.onlineannouncement.command.navigation.AttributeParameterHolder.SESSION_ATTRIBUTE_CATEGORY_LIST;
import static uz.pdp.onlineannouncement.command.navigation.PageNavigation.PAGE_USER_CABINET;
import static uz.pdp.onlineannouncement.command.navigation.PageNavigation.PAGE_USER_CREATE_ANNOUNCEMENT;
import static uz.pdp.onlineannouncement.command.navigation.Router.PageChangeType.REDIRECT;

public class User_StartCreateAnnouncement implements Command {
    @Override
    public Router execute(HttpServletRequest request) {

        HttpSession session = request.getSession();
        User currentUser = (User) session.getAttribute(AttributeParameterHolder.SESSION_ATTRIBUTE_USER);
        UserService userService = UserServiceImpl.getInstance();
        if (userService.checkToActive(currentUser.getId()))
        {
            CategoryService categoryService = CategoryServiceImpl.getInstance();
            List<Category> all = categoryService.findAll();

            session.setAttribute(SESSION_ATTRIBUTE_CATEGORY_LIST, all);

            return new Router(PAGE_USER_CREATE_ANNOUNCEMENT, REDIRECT);
        }
        session.setAttribute(SESSION_ATTRIBUTE_BLOCKED_MSG, "You are blocked. Please call our admin!");
        return new Router(PAGE_USER_CABINET, REDIRECT);
    }
}
