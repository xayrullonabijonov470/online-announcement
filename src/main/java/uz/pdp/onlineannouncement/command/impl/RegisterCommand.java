package uz.pdp.onlineannouncement.command.impl;

import jakarta.servlet.http.HttpServletRequest;
import uz.pdp.onlineannouncement.command.Command;
import uz.pdp.onlineannouncement.command.navigation.Router;
import uz.pdp.onlineannouncement.command.navigation.Router.PageChangeType;
import uz.pdp.onlineannouncement.entity.User;
import uz.pdp.onlineannouncement.entity.enums.RoleName;
import uz.pdp.onlineannouncement.service.UserService;
import uz.pdp.onlineannouncement.service.impl.UserServiceImpl;

import java.sql.Timestamp;
import java.util.UUID;

import static uz.pdp.onlineannouncement.command.navigation.AttributeParameterHolder.*;
import static uz.pdp.onlineannouncement.command.navigation.PageNavigation.PAGE_DEFAULT;

public class RegisterCommand implements Command {
    @Override
    public Router execute(HttpServletRequest request)
    {
        addUserOrAdmin(request, false);
        return new Router(PAGE_DEFAULT, PageChangeType.REDIRECT);
    }

    public static void addUserOrAdmin(HttpServletRequest request, boolean isAdmin)
    {
        String fio = request.getParameter(PARAMETER_USER_FIO);
        String user_name = request.getParameter(PARAMETER_USER_NAME);
        String email = request.getParameter(PARAMETER_USER_EMAIL);
        String password = request.getParameter(PARAMETER_USER_PASSWORD);
        String phone_number = request.getParameter(PARAMETER_USER_PHONE_NUMBER);
        String roleName;
        if (isAdmin){
            roleName = RoleName.ADMIN.name();
        }else {
            roleName = RoleName.USER.name();
        }

        User user = new User.UserBuilder()
                .setId(UUID.randomUUID())
                .setName(fio)
                .setRoleName(roleName)
                .setUsername(user_name)
                .setEmail(email)
                .setPassword(password)
                .setPhoneNumber(phone_number)
                .setActive(true)
                .setAvgRate(0)
                .setCreatedAt(new Timestamp(System.currentTimeMillis()))
                .build();

        UserService userService = UserServiceImpl.getInstance();
        userService.insert(user);
    }
}
