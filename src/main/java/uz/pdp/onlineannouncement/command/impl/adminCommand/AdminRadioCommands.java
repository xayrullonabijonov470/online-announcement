package uz.pdp.onlineannouncement.command.impl.adminCommand;

import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpSession;
import uz.pdp.onlineannouncement.command.Command;
import uz.pdp.onlineannouncement.command.navigation.Router;
import uz.pdp.onlineannouncement.entity.User;
import uz.pdp.onlineannouncement.service.AnnouncementService;
import uz.pdp.onlineannouncement.service.CategoryService;
import uz.pdp.onlineannouncement.service.UserService;
import uz.pdp.onlineannouncement.service.impl.AnnouncementServiceImpl;
import uz.pdp.onlineannouncement.service.impl.CategoryServiceImpl;
import uz.pdp.onlineannouncement.service.impl.UserServiceImpl;

import static uz.pdp.onlineannouncement.command.navigation.AttributeParameterHolder.*;
import static uz.pdp.onlineannouncement.command.navigation.PageNavigation.PAGE_ADMIN_HOME_PAGE;
import static uz.pdp.onlineannouncement.command.navigation.Router.PageChangeType.REDIRECT;

public class AdminRadioCommands implements Command {
    @Override
    public Router execute(HttpServletRequest request) {
        String value = request.getParameter(PARAMETER_RADIO_KEY);
        HttpSession session = request.getSession();

        switch (value) {
            case PARAMETER_RADIO_USERS -> refreshAllUsers(request);
            case PARAMETER_RADIO_NEW_ANNOUNCEMENTS -> refreshNewAnnouncements(request);
            case PARAMETER_RADIO_ALL_ANNOUNCEMENTS -> refreshAllAnnouncements(request);
            case PARAMETER_RADIO_ADMIN_INFO -> session.setAttribute(PARAMETER_RADIO_KEY, PARAMETER_RADIO_ADMIN_INFO);
            case PARAMETER_RADIO_ADMIN_ALL_CATEGORY -> refreshAllCategory(request);
        }
        return new Router(PAGE_ADMIN_HOME_PAGE, REDIRECT);
    }

    @Override
    public void refresh(HttpServletRequest request) {

    }

    public void refreshAllUsers(HttpServletRequest request){
        UserService userService = UserServiceImpl.getInstance();
        User currentUser = (User) request.getSession().getAttribute(SESSION_ATTRIBUTE_USER);
        HttpSession session = request.getSession();
        session.setAttribute(SESSION_ATTRIBUTE_USER_LIST, userService.findAllWithoutUser(currentUser.getId()));
        session.setAttribute(PARAMETER_RADIO_KEY, PARAMETER_RADIO_USERS);
    }

    public void refreshNewAnnouncements(HttpServletRequest request){
        HttpSession session = request.getSession();
        AnnouncementService announcementService = AnnouncementServiceImpl.getInstance();

        session.setAttribute(SESSION_ATTRIBUTE_ANNOUNCEMENT_LIST, announcementService.findAllByApprovedOrBlocked(false));
        session.setAttribute(PARAMETER_RADIO_KEY, PARAMETER_RADIO_NEW_ANNOUNCEMENTS);
    }

    public void refreshAllAnnouncements(HttpServletRequest request){
        HttpSession session = request.getSession();
        AnnouncementService announcementService = AnnouncementServiceImpl.getInstance();

        session.setAttribute(SESSION_ATTRIBUTE_ANNOUNCEMENT_LIST, announcementService.findAllByApprovedOrBlocked(true));
        session.setAttribute(PARAMETER_RADIO_KEY, PARAMETER_RADIO_ALL_ANNOUNCEMENTS);
    }

    public void refreshAllCategory(HttpServletRequest request){
        HttpSession session = request.getSession();
        CategoryService categoryService = CategoryServiceImpl.getInstance();

        session.setAttribute(SESSION_ATTRIBUTE_CATEGORY_LIST, categoryService.findAll());
        session.setAttribute(PARAMETER_RADIO_KEY, PARAMETER_RADIO_ADMIN_ALL_CATEGORY);
    }
}
