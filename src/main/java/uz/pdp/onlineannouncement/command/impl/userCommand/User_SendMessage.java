package uz.pdp.onlineannouncement.command.impl.userCommand;

import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpSession;
import uz.pdp.onlineannouncement.command.Command;
import uz.pdp.onlineannouncement.command.navigation.Router;
import uz.pdp.onlineannouncement.entity.Announcement;
import uz.pdp.onlineannouncement.entity.Chat;
import uz.pdp.onlineannouncement.entity.User;
import uz.pdp.onlineannouncement.service.AnnouncementService;
import uz.pdp.onlineannouncement.service.ChatService;
import uz.pdp.onlineannouncement.service.UserService;
import uz.pdp.onlineannouncement.service.impl.AnnouncementServiceImpl;
import uz.pdp.onlineannouncement.service.impl.ChatServiceImpl;
import uz.pdp.onlineannouncement.service.impl.UserServiceImpl;

import java.sql.Timestamp;
import java.util.List;
import java.util.UUID;

import static uz.pdp.onlineannouncement.command.navigation.AttributeParameterHolder.*;
import static uz.pdp.onlineannouncement.command.navigation.PageNavigation.PAGE_SHOW_CHATTING_PAGE;
import static uz.pdp.onlineannouncement.command.navigation.Router.PageChangeType.REDIRECT;

public class User_SendMessage implements Command {
    @Override
    public Router execute(HttpServletRequest request) {
        String message = request.getParameter(PARAMETER_CHAT_MESSAGE);
        ChatService chatService = ChatServiceImpl.getInstance();
        HttpSession session = request.getSession();

        UserService userService = UserServiceImpl.getInstance();
        AnnouncementService announcementService = AnnouncementServiceImpl.getInstance();

        User currentUser = (User) session.getAttribute(SESSION_ATTRIBUTE_USER);
        UUID partnerId = (UUID) session.getAttribute(SESSION_ATTRIBUTE_CHAT_PARTNER_ID);
        UUID announcementId = (UUID) session.getAttribute(SESSION_ATTRIBUTE_CHAT_ANNOUNCEMENT_ID);
        User partner = userService.findById(partnerId);
        Announcement announcement = announcementService.findById(announcementId);


        if (message != null) {
            Chat chat = new Chat.ChatBuilder()
                    .setId(UUID.randomUUID())
                    .setName(null)
                    .setCreatedAt(new Timestamp(System.currentTimeMillis()))
                    .setAnnouncement(announcement)
                    .setSender(currentUser)
                    .setReceiver(partner)
                    .setMessage(message)
                    .setRead(false)
                    .setDeleted(false)
                    .build();

            chatService.insert(chat);
        }
        List<Chat> chatList = chatService.findAllByBothUserAndAnnouncement(currentUser.getId(), partnerId, announcementId);
        session.setAttribute(SESSION_ATTRIBUTE_CHAT_LIST, chatList);
        return new Router(PAGE_SHOW_CHATTING_PAGE, REDIRECT);
    }
}
