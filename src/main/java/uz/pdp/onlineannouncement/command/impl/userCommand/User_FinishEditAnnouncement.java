package uz.pdp.onlineannouncement.command.impl.userCommand;

import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpSession;
import uz.pdp.onlineannouncement.command.Command;
import uz.pdp.onlineannouncement.command.navigation.Router;
import uz.pdp.onlineannouncement.entity.Announcement;
import uz.pdp.onlineannouncement.entity.Category;
import uz.pdp.onlineannouncement.service.AnnouncementService;
import uz.pdp.onlineannouncement.service.CategoryService;
import uz.pdp.onlineannouncement.service.impl.AnnouncementServiceImpl;
import uz.pdp.onlineannouncement.service.impl.CategoryServiceImpl;

import java.util.UUID;

import static uz.pdp.onlineannouncement.command.navigation.AttributeParameterHolder.*;
import static uz.pdp.onlineannouncement.command.navigation.PageNavigation.PAGE_USER_CABINET;
import static uz.pdp.onlineannouncement.command.navigation.Router.PageChangeType.REDIRECT;
import static uz.pdp.onlineannouncement.entity.Announcement.AnnouncementBuilder;

public class User_FinishEditAnnouncement implements Command {

    @Override
    public Router execute(HttpServletRequest request) {
        HttpSession session = request.getSession();
        Announcement announcement1 = (Announcement) session.getAttribute(SESSION_ATTRIBUTE_ANNOUNCEMENT);
        UUID categoryId = UUID.fromString(request.getParameter(PARAMETER_ANNOUNCEMENT_CATEGORY_ID));
        String header = request.getParameter(PARAMETER_ANNOUNCEMENT_HEADER);
        String definition = request.getParameter(PARAMETER_ANNOUNCEMENT_DEFINITION);
        double price = Double.parseDouble(request.getParameter(PARAMETER_ANNOUNCEMENT_PRICE));
        double discount = Double.parseDouble(request.getParameter(PARAMETER_ANNOUNCEMENT_DISCOUNT));

        CategoryService categoryService = CategoryServiceImpl.getInstance();
        Category category = categoryService.findByIdOrName(categoryId, null);
        Announcement announcement = new AnnouncementBuilder()
                .setId(announcement1.getId())
                .setCategory(category)
                .setHeader(header)
                .setDefinition(definition)
                .setPrice(price)
                .setDiscount(discount)
                .build();
        AnnouncementService announcementService = AnnouncementServiceImpl.getInstance();
        announcementService.update(announcement);

        UserRadioCommands userRadioCommands = new UserRadioCommands();
        userRadioCommands.refreshUserOwnAnnouncement(request.getSession());

        session.setAttribute(SESSION_ATTRIBUTE_ANNOUNCEMENT, null);
        return new Router(PAGE_USER_CABINET, REDIRECT);
    }
}
