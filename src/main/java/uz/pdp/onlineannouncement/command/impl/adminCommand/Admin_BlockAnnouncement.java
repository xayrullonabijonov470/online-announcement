package uz.pdp.onlineannouncement.command.impl.adminCommand;

import jakarta.servlet.http.HttpServletRequest;
import uz.pdp.onlineannouncement.command.Command;
import uz.pdp.onlineannouncement.command.navigation.AttributeParameterHolder;
import uz.pdp.onlineannouncement.command.navigation.Router;
import uz.pdp.onlineannouncement.service.AnnouncementService;
import uz.pdp.onlineannouncement.service.impl.AnnouncementServiceImpl;

import java.util.UUID;

import static uz.pdp.onlineannouncement.command.navigation.PageNavigation.PAGE_ADMIN_HOME_PAGE;
import static uz.pdp.onlineannouncement.command.navigation.Router.PageChangeType.REDIRECT;

public class Admin_BlockAnnouncement implements Command {
    @Override
    public Router execute(HttpServletRequest request) {
        refresh(request);
        AdminRadioCommands adminRadioCommands = new AdminRadioCommands();
        adminRadioCommands.refreshAllAnnouncements(request);
        return new Router(PAGE_ADMIN_HOME_PAGE, REDIRECT);
    }

    @Override
    public void refresh(HttpServletRequest request) {
        UUID announcementId = UUID.fromString(request.getParameter(AttributeParameterHolder.PARAMETER_ANNOUNCEMENT_ID));
        AnnouncementService announcementService = AnnouncementServiceImpl.getInstance();
        announcementService.setApprovedOrBlocked(announcementId, false);
    }
}
