package uz.pdp.onlineannouncement.command.impl.adminCommand;

import jakarta.servlet.http.HttpServletRequest;
import uz.pdp.onlineannouncement.command.Command;
import uz.pdp.onlineannouncement.command.navigation.AttributeParameterHolder;
import uz.pdp.onlineannouncement.command.navigation.Router;

import static uz.pdp.onlineannouncement.command.navigation.PageNavigation.PAGE_ADMIN_HOME_PAGE;
import static uz.pdp.onlineannouncement.command.navigation.Router.PageChangeType.REDIRECT;

public class RemoveUserCommand implements Command {
    @Override
    public Router execute(HttpServletRequest request) {
        String userId = request.getParameter(AttributeParameterHolder.PARAMETER_USER_ID);
        System.out.println(userId);
        return new Router(PAGE_ADMIN_HOME_PAGE, REDIRECT);
    }
}
