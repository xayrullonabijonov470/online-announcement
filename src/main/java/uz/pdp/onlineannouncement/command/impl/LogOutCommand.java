package uz.pdp.onlineannouncement.command.impl;

import jakarta.servlet.http.HttpServletRequest;
import uz.pdp.onlineannouncement.command.Command;
import uz.pdp.onlineannouncement.command.navigation.Router;

import static uz.pdp.onlineannouncement.command.navigation.PageNavigation.PAGE_LOGIN;
import static uz.pdp.onlineannouncement.command.navigation.Router.PageChangeType.REDIRECT;

//import javax.servlet.http.HttpServletRequest;

public class LogOutCommand implements Command {
    @Override
    public Router execute(HttpServletRequest request)
    {
        request.getSession().invalidate();
        return new Router(PAGE_LOGIN, REDIRECT);
    }
}
