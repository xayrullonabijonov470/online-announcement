package uz.pdp.onlineannouncement.command.impl.userCommand;

import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpSession;
import uz.pdp.onlineannouncement.command.Command;
import uz.pdp.onlineannouncement.command.navigation.Router;
import uz.pdp.onlineannouncement.entity.User;
import uz.pdp.onlineannouncement.service.ChatService;
import uz.pdp.onlineannouncement.service.UserService;
import uz.pdp.onlineannouncement.service.impl.ChatServiceImpl;
import uz.pdp.onlineannouncement.service.impl.UserServiceImpl;

import java.util.UUID;

import static uz.pdp.onlineannouncement.command.navigation.AttributeParameterHolder.*;
import static uz.pdp.onlineannouncement.command.navigation.PageNavigation.PAGE_SHOW_CHATTING_PAGE;
import static uz.pdp.onlineannouncement.command.navigation.PageNavigation.PAGE_USER_CABINET;
import static uz.pdp.onlineannouncement.command.navigation.Router.PageChangeType.REDIRECT;

public class User_StartChatting implements Command
{

    @Override
    public Router execute(HttpServletRequest request)
    {
        HttpSession session = request.getSession();
        UUID partnerId = UUID.fromString(request.getParameter(PARAMETER_CHAT_PARTNER_ID));
        UUID announcementId = UUID.fromString(request.getParameter(PARAMETER_CHAT_ANNS_ID));
        User currentUser = (User) session.getAttribute(SESSION_ATTRIBUTE_USER);

        UserService userService = UserServiceImpl.getInstance();
        if (userService.checkToActive(currentUser.getId())){
            ChatService chatService = ChatServiceImpl.getInstance();

            session.setAttribute(SESSION_ATTRIBUTE_CHAT_ANNOUNCEMENT_ID, announcementId);
            session.setAttribute(SESSION_ATTRIBUTE_CHAT_PARTNER_ID, partnerId);

            session.setAttribute(SESSION_ATTRIBUTE_CHAT_USER_CHATTERS, chatService.findAllUserChattersByUser(currentUser.getId()));

            session.setAttribute(SESSION_ATTRIBUTE_CHAT_LIST, chatService.findAllByBothUserAndAnnouncement(currentUser.getId(), partnerId, announcementId));
            return new Router(PAGE_SHOW_CHATTING_PAGE, REDIRECT);
        }
        session.setAttribute(SESSION_ATTRIBUTE_BLOCKED_MSG, "You are blocked. Please call our admin!");
        return new Router(PAGE_USER_CABINET, REDIRECT);
    }
}
