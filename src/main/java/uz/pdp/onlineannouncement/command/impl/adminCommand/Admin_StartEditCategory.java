package uz.pdp.onlineannouncement.command.impl.adminCommand;

import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpSession;
import uz.pdp.onlineannouncement.command.Command;
import uz.pdp.onlineannouncement.command.navigation.AttributeParameterHolder;
import uz.pdp.onlineannouncement.command.navigation.Router;
import uz.pdp.onlineannouncement.service.CategoryService;
import uz.pdp.onlineannouncement.service.impl.CategoryServiceImpl;

import java.util.UUID;

import static uz.pdp.onlineannouncement.command.navigation.AttributeParameterHolder.SESSION_ATTRIBUTE_CATEGORY;
import static uz.pdp.onlineannouncement.command.navigation.PageNavigation.PAGE_ADMIN_HOME_PAGE;
import static uz.pdp.onlineannouncement.command.navigation.Router.PageChangeType.REDIRECT;

public class Admin_StartEditCategory implements Command {
    @Override
    public Router execute(HttpServletRequest request) {
        UUID id = UUID.fromString(request.getParameter(AttributeParameterHolder.PARAMETER_CATEGORY_ID));
        HttpSession session = request.getSession();

        CategoryService categoryService = CategoryServiceImpl.getInstance();
        session.setAttribute(SESSION_ATTRIBUTE_CATEGORY, categoryService.findByIdOrName(id, null));

        return new Router(PAGE_ADMIN_HOME_PAGE, REDIRECT);
    }
}
