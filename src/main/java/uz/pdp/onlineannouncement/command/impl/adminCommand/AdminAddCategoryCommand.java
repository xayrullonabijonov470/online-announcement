package uz.pdp.onlineannouncement.command.impl.adminCommand;

import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpSession;
import uz.pdp.onlineannouncement.command.Command;
import uz.pdp.onlineannouncement.command.navigation.AttributeParameterHolder;
import uz.pdp.onlineannouncement.command.navigation.Router;
import uz.pdp.onlineannouncement.entity.Category;
import uz.pdp.onlineannouncement.service.CategoryService;
import uz.pdp.onlineannouncement.service.impl.CategoryServiceImpl;

import java.sql.Timestamp;
import java.util.UUID;

import static uz.pdp.onlineannouncement.command.navigation.AttributeParameterHolder.*;
import static uz.pdp.onlineannouncement.command.navigation.PageNavigation.PAGE_ADMIN_HOME_PAGE;
import static uz.pdp.onlineannouncement.command.navigation.Router.PageChangeType.REDIRECT;

public class AdminAddCategoryCommand implements Command {
    @Override
    public Router execute(HttpServletRequest request) {

        HttpSession session = request.getSession();

        String categoryName = request.getParameter(AttributeParameterHolder.PARAMETER_CATEGORY_NAME);
        CategoryService categoryService = CategoryServiceImpl.getInstance();

        Category category = new Category.CategoryBuilder()
                .setId(UUID.randomUUID())
                .setName(categoryName)
                .setCreatedAt(new Timestamp(System.currentTimeMillis()))
                .build();

        boolean insert = categoryService.insert(category);
        if (!insert) {
            session.setAttribute(SESSION_ATTRIBUTE_EXIST_MSG, "This Category Already exist!");
        }

        session.setAttribute(SESSION_ATTRIBUTE_CATEGORY_LIST, categoryService.findAll());
        session.setAttribute(PARAMETER_RADIO_KEY, PARAMETER_RADIO_ADMIN_ALL_CATEGORY);

        return new Router(PAGE_ADMIN_HOME_PAGE, REDIRECT);
    }
}
