package uz.pdp.onlineannouncement.command.impl;

import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpSession;
import uz.pdp.onlineannouncement.command.Command;
import uz.pdp.onlineannouncement.command.navigation.Router;
import uz.pdp.onlineannouncement.entity.User;
import uz.pdp.onlineannouncement.service.UserService;
import uz.pdp.onlineannouncement.service.impl.UserServiceImpl;

import static uz.pdp.onlineannouncement.command.navigation.AttributeParameterHolder.*;
import static uz.pdp.onlineannouncement.command.navigation.PageNavigation.PAGE_LOGIN;
import static uz.pdp.onlineannouncement.command.navigation.Router.PageChangeType.REDIRECT;

public class EditUserInfo implements Command {
    @Override
    public Router execute(HttpServletRequest request) {
        HttpSession session = request.getSession();
        User currentUser = (User) session.getAttribute(SESSION_ATTRIBUTE_USER);
        String fio = request.getParameter(PARAMETER_USER_FIO);
        String user_name = request.getParameter(PARAMETER_USER_NAME);
        String email= request.getParameter(PARAMETER_USER_EMAIL);
        String password = request.getParameter(PARAMETER_USER_PASSWORD);
        String phone_number = request.getParameter(PARAMETER_USER_PHONE_NUMBER);

        currentUser.setName(fio);
        currentUser.setUsername(user_name);
        currentUser.setEmail(email);
        currentUser.setPassword(password);
        currentUser.setPhoneNumber(phone_number);

        UserService userService = UserServiceImpl.getInstance();
        userService.update(currentUser);
        session.invalidate();
        return new Router(PAGE_LOGIN, REDIRECT);
    }
}
