package uz.pdp.onlineannouncement.command.impl.adminCommand;

import jakarta.servlet.http.HttpServletRequest;
import uz.pdp.onlineannouncement.command.Command;
import uz.pdp.onlineannouncement.command.navigation.Router;
import uz.pdp.onlineannouncement.entity.Category;
import uz.pdp.onlineannouncement.service.CategoryService;
import uz.pdp.onlineannouncement.service.impl.CategoryServiceImpl;

import java.util.UUID;

import static uz.pdp.onlineannouncement.command.navigation.AttributeParameterHolder.*;
import static uz.pdp.onlineannouncement.command.navigation.PageNavigation.PAGE_ADMIN_HOME_PAGE;
import static uz.pdp.onlineannouncement.command.navigation.Router.PageChangeType.REDIRECT;

public class Admin_FinishEditCategory implements Command {
    @Override
    public Router execute(HttpServletRequest request) {
        String categoryName = request.getParameter(PARAMETER_CATEGORY_NAME);
        UUID categoryId = UUID.fromString(request.getParameter(PARAMETER_CATEGORY_ID));

        CategoryService categoryService = CategoryServiceImpl.getInstance();
        Category category = new Category.CategoryBuilder()
                .setId(categoryId)
                .setName(categoryName)
                .build();

        categoryService.update(category);

        AdminRadioCommands adminRadioCommands = new AdminRadioCommands();
        adminRadioCommands.refreshAllCategory(request);

        request.getSession().setAttribute(SESSION_ATTRIBUTE_CATEGORY, null);

        return new Router(PAGE_ADMIN_HOME_PAGE, REDIRECT);
    }
}
