package uz.pdp.onlineannouncement.command.impl;

import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpSession;
import uz.pdp.onlineannouncement.command.Command;
import uz.pdp.onlineannouncement.command.navigation.Router;
import uz.pdp.onlineannouncement.entity.User;
import uz.pdp.onlineannouncement.entity.enums.RoleName;
import uz.pdp.onlineannouncement.service.AnnouncementService;
import uz.pdp.onlineannouncement.service.UserService;
import uz.pdp.onlineannouncement.service.impl.AnnouncementServiceImpl;
import uz.pdp.onlineannouncement.service.impl.UserServiceImpl;

import static uz.pdp.onlineannouncement.command.navigation.AttributeParameterHolder.*;
import static uz.pdp.onlineannouncement.command.navigation.PageNavigation.*;
import static uz.pdp.onlineannouncement.command.navigation.Router.PageChangeType.REDIRECT;


public class LoginCommand implements Command
{
    @Override
    public Router execute(HttpServletRequest request)
    {
        String login = request.getParameter(PARAMETER_USER_EMAIL);
        String password = request.getParameter(PARAMETER_USER_PASSWORD);
        HttpSession session = request.getSession();
        Object userObj = session.getAttribute(SESSION_ATTRIBUTE_USER);
        UserService userService = UserServiceImpl.getInstance();

        User user ;
        boolean check ;
        boolean authenticate;
        if (login == null && password == null)
        {
            if (userObj != null)
            {
                user = (User) userObj;
                check = user.getRoleName().equals("ADMIN");
                authenticate = userService.authenticate(user.getEmail(), user.getPassword());
            }else
            {
                return new Router(PAGE_LOGIN, REDIRECT);
            }
        }
        else
        {
            authenticate = userService.authenticate(login, password);
            user = userService.findByEmail(login);
            check = userService.checkToAdmin(login, password);
        }
        AnnouncementService announcementService = AnnouncementServiceImpl.getInstance();
        String page = PAGE_LOGIN ;
        if (authenticate)
        {
            if (check)
            {
                session.setAttribute(SESSION_ATTRIBUTE_USER_ROLE, RoleName.ADMIN.name());
                session.setAttribute(SESSION_ATTRIBUTE_USER_LIST, userService.findAllWithoutUser(user.getId()));
                session.setAttribute(PARAMETER_RADIO_KEY, PARAMETER_RADIO_USERS);
                page = PAGE_ADMIN_HOME_PAGE;
            }
            else
            {
                session.setAttribute(SESSION_ATTRIBUTE_ANNOUNCEMENT_LIST, announcementService.findAllWithoutOwner(user.getId()));
                session.setAttribute(PARAMETER_RADIO_KEY, PARAMETER_USER_RADIO_ALL_ANNOUNCEMENT);
                session.setAttribute(SESSION_ATTRIBUTE_USER_ROLE, RoleName.USER.name());
                page = PAGE_USER_CABINET;
            }
            session.setAttribute(SESSION_ATTRIBUTE_USER_NAME, user.getUsername());
            session.setAttribute("user",user.getName());
            session.setAttribute(SESSION_ATTRIBUTE_USER, user);
        }
        else
        {
            session.setAttribute(SESSION_ATTRIBUTE_LOGIN_MSG,"Error login or password");
        }
        return new Router(page, REDIRECT);
    }
}
