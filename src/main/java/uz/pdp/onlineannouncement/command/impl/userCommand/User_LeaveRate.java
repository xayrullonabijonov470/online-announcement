package uz.pdp.onlineannouncement.command.impl.userCommand;

import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpSession;
import uz.pdp.onlineannouncement.command.Command;
import uz.pdp.onlineannouncement.command.navigation.Router;
import uz.pdp.onlineannouncement.entity.Rate;
import uz.pdp.onlineannouncement.entity.User;
import uz.pdp.onlineannouncement.service.RateService;
import uz.pdp.onlineannouncement.service.impl.RateServiceImpl;

import java.sql.Timestamp;
import java.util.UUID;

import static uz.pdp.onlineannouncement.command.navigation.AttributeParameterHolder.*;
import static uz.pdp.onlineannouncement.command.navigation.PageNavigation.PAGE_USER_VIEW_ANNOUNCEMENT;
import static uz.pdp.onlineannouncement.command.navigation.Router.PageChangeType.REDIRECT;

public class User_LeaveRate implements Command {
    @Override
    public Router execute(HttpServletRequest request) {
        HttpSession session = request.getSession();
        double rateNum = Double.parseDouble(request.getParameter(PARAMETER_RATE_NUMBER));
        User touser = (User) session.getAttribute(SESSION_ATTRIBUTE_RATE_TO_USER);
        User currentUser = (User) session.getAttribute(SESSION_ATTRIBUTE_USER);

        RateService rateService = RateServiceImpl.getInstance();
        Rate rate = new Rate.RateBuilder()
                .setId(UUID.randomUUID())
                .setName(null)
                .setToUser(touser)
                .setFromUser(currentUser)
                .setRate(rateNum)
                .setCreatedAt(new Timestamp(System.currentTimeMillis()))
                .build();
        rateService.insert(rate);

        User_StartViewAnnouncements user_startViewAnnouncements = new User_StartViewAnnouncements();
        user_startViewAnnouncements.refresh(request);

        return new Router(PAGE_USER_VIEW_ANNOUNCEMENT, REDIRECT);
    }

}
