package uz.pdp.onlineannouncement.command.impl.userCommand;

import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpSession;
import uz.pdp.onlineannouncement.command.Command;
import uz.pdp.onlineannouncement.command.navigation.AttributeParameterHolder;
import uz.pdp.onlineannouncement.command.navigation.Router;
import uz.pdp.onlineannouncement.entity.User;
import uz.pdp.onlineannouncement.service.AnnouncementService;
import uz.pdp.onlineannouncement.service.ChatService;
import uz.pdp.onlineannouncement.service.UserService;
import uz.pdp.onlineannouncement.service.impl.AnnouncementServiceImpl;
import uz.pdp.onlineannouncement.service.impl.ChatServiceImpl;
import uz.pdp.onlineannouncement.service.impl.UserServiceImpl;

import static uz.pdp.onlineannouncement.command.navigation.AttributeParameterHolder.*;
import static uz.pdp.onlineannouncement.command.navigation.PageNavigation.PAGE_SHOW_CHATTING_PAGE;
import static uz.pdp.onlineannouncement.command.navigation.PageNavigation.PAGE_USER_CABINET;
import static uz.pdp.onlineannouncement.command.navigation.Router.PageChangeType.REDIRECT;

public class UserRadioCommands implements Command {

    @Override
    public Router execute(HttpServletRequest request)
    {
        String value = request.getParameter(PARAMETER_RADIO_KEY);
        HttpSession session = request.getSession();
        UserService userService = UserServiceImpl.getInstance();
        String page = PAGE_USER_CABINET;
        User currentUser = (User) session.getAttribute(SESSION_ATTRIBUTE_USER);
        switch (value) {
            case AttributeParameterHolder.PARAMETER_USER_RADIO_ALL_ANNOUNCEMENT -> refreshAllAnnouncement(session);
            case PARAMETER_USER_RADIO_OWN_ANNOUNCEMENT -> refreshUserOwnAnnouncement(session);
            case PARAMETER_USER_RADIO_MESSAGING -> {
                if (!userService.checkToActive(currentUser.getId())) {
                    session.setAttribute(SESSION_ATTRIBUTE_BLOCKED_MSG, "You are blocked. Please call our admin!");
                } else {
                    page = PAGE_SHOW_CHATTING_PAGE;
                    refreshUserChatting(session);
                }
            }
            case PARAMETER_USER_RADIO_INFO -> session.setAttribute(PARAMETER_RADIO_KEY, PARAMETER_USER_RADIO_INFO);
        }

        return new Router(page, REDIRECT);
    }

    public void refreshAllAnnouncement(HttpSession session){
        User currentUser = (User) session.getAttribute(SESSION_ATTRIBUTE_USER);
        AnnouncementService announcementService = AnnouncementServiceImpl.getInstance();

        session.setAttribute(SESSION_ATTRIBUTE_ANNOUNCEMENT_LIST, announcementService.findAllWithoutOwner(currentUser.getId()));
        session.setAttribute(PARAMETER_RADIO_KEY, PARAMETER_USER_RADIO_ALL_ANNOUNCEMENT);
    }

    public void refreshUserOwnAnnouncement(HttpSession session){
        User currentUser = (User) session.getAttribute(SESSION_ATTRIBUTE_USER);
        AnnouncementService announcementService = AnnouncementServiceImpl.getInstance();

        session.setAttribute(SESSION_ATTRIBUTE_ANNOUNCEMENT_LIST, announcementService.findAllByOwner(currentUser.getId()));
        session.setAttribute(PARAMETER_RADIO_KEY, PARAMETER_USER_RADIO_OWN_ANNOUNCEMENT);
    }
    public void  refreshUserChatting(HttpSession session)
    {
        User currentUser = (User) session.getAttribute(SESSION_ATTRIBUTE_USER);
        ChatService chatService = ChatServiceImpl.getInstance();
        session.setAttribute(SESSION_ATTRIBUTE_CHAT_USER_CHATTERS, chatService.findAllUserChattersByUser(currentUser.getId()));
    }
}
