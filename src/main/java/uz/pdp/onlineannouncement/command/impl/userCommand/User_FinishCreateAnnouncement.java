package uz.pdp.onlineannouncement.command.impl.userCommand;

import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpSession;
import uz.pdp.onlineannouncement.command.Command;
import uz.pdp.onlineannouncement.command.navigation.Router;
import uz.pdp.onlineannouncement.entity.Announcement;
import uz.pdp.onlineannouncement.entity.Category;
import uz.pdp.onlineannouncement.entity.User;
import uz.pdp.onlineannouncement.service.AnnouncementService;
import uz.pdp.onlineannouncement.service.CategoryService;
import uz.pdp.onlineannouncement.service.impl.AnnouncementServiceImpl;
import uz.pdp.onlineannouncement.service.impl.CategoryServiceImpl;

import java.sql.Timestamp;
import java.util.UUID;

import static uz.pdp.onlineannouncement.command.navigation.AttributeParameterHolder.*;
import static uz.pdp.onlineannouncement.command.navigation.PageNavigation.PAGE_USER_CABINET;
import static uz.pdp.onlineannouncement.command.navigation.Router.PageChangeType.REDIRECT;

public class User_FinishCreateAnnouncement implements Command {
    @Override
    public Router execute(HttpServletRequest request) {

        UUID categoryId = UUID.fromString(request.getParameter(PARAMETER_ANNOUNCEMENT_CATEGORY_ID));
        HttpSession session = request.getSession();
        User currentUser = (User) session.getAttribute(SESSION_ATTRIBUTE_USER);
        String header = request.getParameter(PARAMETER_ANNOUNCEMENT_HEADER);
        String definition = request.getParameter(PARAMETER_ANNOUNCEMENT_DEFINITION);
        double price = Double.parseDouble(request.getParameter(PARAMETER_ANNOUNCEMENT_PRICE));
        double discount = Double.parseDouble(request.getParameter(PARAMETER_ANNOUNCEMENT_DISCOUNT));

        AnnouncementService announcementService = AnnouncementServiceImpl.getInstance();
        CategoryService categoryService = CategoryServiceImpl.getInstance();
        Category category = categoryService.findByIdOrName(categoryId, null);

        Announcement announcement = new Announcement.AnnouncementBuilder()
                .setId(UUID.randomUUID())
                .setName(null)
                .setCreatedAt(new Timestamp(System.currentTimeMillis()))
                .setCategory(category)
                .setUser(currentUser)
                .setHeader(header)
                .setDefinition(definition)
                .setPrice(price)
                .setDiscount(discount)
                .setApproved(false)
                .build();
        announcementService.insert(announcement);

        UserRadioCommands userRadioCommands = new UserRadioCommands();

        userRadioCommands.refreshAllAnnouncement(session);
        return new Router(PAGE_USER_CABINET, REDIRECT);

    }
}
