package uz.pdp.onlineannouncement.command.impl.userCommand;

import jakarta.servlet.http.HttpServletRequest;
import uz.pdp.onlineannouncement.command.Command;
import uz.pdp.onlineannouncement.command.navigation.Router;
import uz.pdp.onlineannouncement.service.AnnouncementService;
import uz.pdp.onlineannouncement.service.impl.AnnouncementServiceImpl;

import java.util.UUID;

import static uz.pdp.onlineannouncement.command.navigation.AttributeParameterHolder.PARAMETER_ANNOUNCEMENT_ID;
import static uz.pdp.onlineannouncement.command.navigation.PageNavigation.PAGE_USER_CABINET;
import static uz.pdp.onlineannouncement.command.navigation.Router.PageChangeType.REDIRECT;

public class User_BlockOwnAnnouncement implements Command {
    @Override
    public Router execute(HttpServletRequest request) {
        UUID announcementId = UUID.fromString(request.getParameter(PARAMETER_ANNOUNCEMENT_ID));

        AnnouncementService announcementService = AnnouncementServiceImpl.getInstance();
        announcementService.setApprovedOrBlocked(announcementId, false);

        UserRadioCommands userRadioCommands = new UserRadioCommands();
        userRadioCommands.refreshUserOwnAnnouncement(request.getSession());

        return new Router(PAGE_USER_CABINET, REDIRECT);
    }
}
