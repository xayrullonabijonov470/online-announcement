package uz.pdp.onlineannouncement.command.impl.userCommand;

import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpSession;
import uz.pdp.onlineannouncement.command.Command;
import uz.pdp.onlineannouncement.command.navigation.Router;
import uz.pdp.onlineannouncement.entity.User;
import uz.pdp.onlineannouncement.service.AnnouncementService;
import uz.pdp.onlineannouncement.service.impl.AnnouncementServiceImpl;

import static uz.pdp.onlineannouncement.command.navigation.AttributeParameterHolder.*;
import static uz.pdp.onlineannouncement.command.navigation.PageNavigation.PAGE_USER_CABINET;
import static uz.pdp.onlineannouncement.command.navigation.Router.PageChangeType.REDIRECT;

public class User_OwnAnnouncement implements Command {
    @Override
    public Router execute(HttpServletRequest request) {
        HttpSession session = request.getSession();

        User currentUser = (User) session.getAttribute(SESSION_ATTRIBUTE_USER);
        AnnouncementService announcementService = AnnouncementServiceImpl.getInstance();

        session.setAttribute(SESSION_ATTRIBUTE_ANNOUNCEMENT_LIST, announcementService.findAllByOwner(currentUser.getId()));
        session.setAttribute(PARAMETER_RADIO_KEY, PARAMETER_RADIO_ALL_ANNOUNCEMENTS);

        return new Router(PAGE_USER_CABINET, REDIRECT);
    }
}
