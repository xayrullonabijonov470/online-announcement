package uz.pdp.onlineannouncement.command.impl.userCommand;

import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpSession;
import uz.pdp.onlineannouncement.command.Command;
import uz.pdp.onlineannouncement.command.navigation.Router;
import uz.pdp.onlineannouncement.entity.Announcement;
import uz.pdp.onlineannouncement.entity.Category;
import uz.pdp.onlineannouncement.service.AnnouncementService;
import uz.pdp.onlineannouncement.service.CategoryService;
import uz.pdp.onlineannouncement.service.impl.AnnouncementServiceImpl;
import uz.pdp.onlineannouncement.service.impl.CategoryServiceImpl;

import java.util.List;
import java.util.UUID;

import static uz.pdp.onlineannouncement.command.navigation.AttributeParameterHolder.*;
import static uz.pdp.onlineannouncement.command.navigation.PageNavigation.PAGE_USER_EDIT_ANNOUNCEMENT;
import static uz.pdp.onlineannouncement.command.navigation.Router.PageChangeType.REDIRECT;

public class User_StartEditAnnouncement implements Command {
    @Override
    public Router execute(HttpServletRequest request) {
        UUID announcementId = UUID.fromString(request.getParameter(PARAMETER_ANNOUNCEMENT_ID));

        AnnouncementService announcementService = AnnouncementServiceImpl.getInstance();
        Announcement announcement = announcementService.findById(announcementId);

        CategoryService categoryService = CategoryServiceImpl.getInstance();
        List<Category> categoryList = categoryService.findAll();

        HttpSession session = request.getSession();
        session.setAttribute(SESSION_ATTRIBUTE_ANNOUNCEMENT, announcement);
        session.setAttribute(SESSION_ATTRIBUTE_CATEGORY_LIST, categoryList);

        return new Router(PAGE_USER_EDIT_ANNOUNCEMENT, REDIRECT);
    }
}
