package uz.pdp.onlineannouncement.command;

import uz.pdp.onlineannouncement.command.impl.*;
import uz.pdp.onlineannouncement.command.impl.adminCommand.*;
import uz.pdp.onlineannouncement.command.impl.userCommand.*;

public enum CommandType {
    ADD_ADMIN(new AddAdminCommand()),
    HOME_PAGE(new HomePage()),
    LOGIN(new LoginCommand()),
    LOGOUT(new LogOutCommand()),
    DEFAULT(new DefaultCommand()),
    REMOVE_USER(new RemoveUserCommand()),
    REGISTER(new RegisterCommand()),
    RADIO_COMMAND(new AdminRadioCommands()),
    RADIO_COMMAND_USER(new UserRadioCommands()),
    USER_FINISH_CREATE_ANNOUNCEMENT(new User_FinishCreateAnnouncement()),
    USER_START_CREATE_ANNOUNCEMENT(new User_StartCreateAnnouncement()),
    USER_START_EDIT_ANNOUNCEMENT(new User_StartEditAnnouncement()),
    USER_START_CHATTING(new User_StartChatting()),
    USER_SEND_MESSAGE(new User_SendMessage()),
    USER_FINISH_EDIT_ANNOUNCEMENT(new User_FinishEditAnnouncement()),
    USER_BLOCK_ANNOUNCEMENT(new User_BlockOwnAnnouncement()),
    USER_OWN_ANNOUNCEMENT(new User_OwnAnnouncement()),
    USER_START_VIEW_ANNOUNCEMENT(new User_StartViewAnnouncements()),
    USER_LEAVE_COMMENT(new User_LeaveComment()),
    USER_LEAVE_RATE(new User_LeaveRate()),
    ADMIN_APPROVE_ANNOUNCEMENT(new Admin_ApproveAnnouncement()),
    ADMIN_BLOCK_ANNOUNCEMENT(new Admin_BlockAnnouncement()),
    ADMIN_BLOCK_OR_ACTIVATE_USER(new Admin_BlockOrActivateUser()),
    ADMIN_START_EDIT_CATEGORY(new Admin_StartEditCategory()),
    ADMIN_FINISH_EDIT_CATEGORY(new Admin_FinishEditCategory()),
    ADD_CATEGORY(new AdminAddCategoryCommand()),
    EDIT_USER_INFO(new EditUserInfo());

    final Command command;

    CommandType(Command command) {
        this.command = command;
    }

    public static Command define(String commandStr){
        CommandType current = CommandType.valueOf(commandStr.toUpperCase());
        return current.command;
    }

    public static CommandType defineCommandType(String commandStr) {
        CommandType commandType;
        try {
            commandType = commandStr != null ? CommandType.valueOf(commandStr.toUpperCase()) : DEFAULT;
        } catch (IllegalArgumentException e){
            commandType = DEFAULT;
        }
        return commandType;
    }

}
