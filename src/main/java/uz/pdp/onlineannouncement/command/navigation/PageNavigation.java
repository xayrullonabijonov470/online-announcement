package uz.pdp.onlineannouncement.command.navigation;

/**
 * Class with paths to /pages and patterns for routing requests.
 */
public final class PageNavigation {

    public static final String PAGE_DEFAULT = "/index.jsp";
    public static final String PAGE_LOGIN = "/pages/login.jsp";
    public static final String PAGE_SHOW_CHATTING_PAGE = "/pages/User_showChattingPage.jsp";
    public static final String PAGE_REGISTER = "/pages/register.jsp";
    public static final String PAGE_ADMIN_HOME_PAGE = "/pages/adminHomePage.jsp";
    public static final String PAGE_USER_CABINET = "/pages/userCabinet.jsp";
    public static final String PAGE_USER_CREATE_ANNOUNCEMENT = "/pages/user_createAnnouncement.jsp";
    public static final String PAGE_USER_EDIT_ANNOUNCEMENT = "/pages/user_editAnnouncement.jsp";
    public static final String PAGE_USER_VIEW_ANNOUNCEMENT = "/pages/User_viewAnnouncement.jsp";

   private PageNavigation() {
    }
}
