package uz.pdp.onlineannouncement.command.navigation;


import static uz.pdp.onlineannouncement.command.navigation.PageNavigation.PAGE_DEFAULT;

public class Router {

    private String page = PAGE_DEFAULT;
    private PageChangeType type = PageChangeType.REDIRECT;

    public enum PageChangeType {
        FORWARD, REDIRECT;
    }

    public Router() {
    }

    public Router(String page) {
        this.page = (page != null ? page : PAGE_DEFAULT);
    }

    public Router(String page, PageChangeType type) {
        this.page = (page != null ? page : PAGE_DEFAULT);
        this.type = (type != null ? type : PageChangeType.REDIRECT);
    }

    public String getPage() {
        return page;
    }

    public void setPage(String page) {
        this.page = (page != null ? page : PAGE_DEFAULT);
    }

    public void setRedirect() {
        this.type = PageChangeType.REDIRECT;
    }

    public void setForward() {
        this.type = PageChangeType.FORWARD;
    }

    public PageChangeType getType() {
        return type;
    }
}
