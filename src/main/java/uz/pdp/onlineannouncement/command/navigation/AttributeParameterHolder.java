package uz.pdp.onlineannouncement.command.navigation;

public final class AttributeParameterHolder {

    public static final String PARAMETER_USER_NAME = "user_name";
    public static final String PARAMETER_USER_EMAIL = "user_email";
    public static final String PARAMETER_USER_PASSWORD = "user_password";
    public static final String PARAMETER_USER_FIO = "user_fio";
    public static final String PARAMETER_USER_PHONE_NUMBER = "phone_number";
    public static final String PARAMETER_USER_ID = "user_id";



    // param commands
    public static final String PARAMETER_COMMAND = "command";
    public static final String PARAMETER_COMMAND_HOME_PAGE = "home_page";
    public static final String PARAMETER_COMMAND_EDIT_USER_INFO = "edit_user_info";
    public static final String PARAMETER_COMMAND_CONTROLLER = "controller";
    public static final String PARAMETER_COMMAND_REGISTER = "register";
    public static final String PARAMETER_COMMAND_LOGIN = "login";
    public static final String PARAMETER_COMMAND_LOGOUT = "logout";
    public static final String PARAMETER_COMMAND_DEFAULT = "default";
    public static final String PARAMETER_COMMAND_ADD_ADMIN = "add_admin";
    public static final String PARAMETER_COMMAND_RADIO_COMMAND = "radio_command";
    public static final String PARAMETER_COMMAND_RADIO_COMMAND_USER = "radio_command_user";
    public static final String PARAMETER_COMMAND_ADD_CATEGORY = "add_category";
    public static final String PARAMETER_COMMAND_ADMIN_APPROVE_ANNOUNCEMENT = "admin_approve_announcement";
    public static final String PARAMETER_COMMAND_ADMIN_BLOCK_ANNOUNCEMENT = "admin_block_announcement";
    public static final String PARAMETER_COMMAND_START_EDIT_CATEGORY = "admin_start_edit_category";
    public static final String PARAMETER_COMMAND_FINISH_EDIT_CATEGORY = "admin_finish_edit_category";
    public static final String PARAMETER_COMMAND_USER_FINISH_CREATE_ANNOUNCEMENT = "user_finish_create_announcement";
    public static final String PARAMETER_COMMAND_USER_START_CREATE_ANNOUNCEMENT = "user_start_create_announcement";
    public static final String PARAMETER_COMMAND_USER_START_EDIT_ANNOUNCEMENT = "user_start_edit_announcement";
    public static final String PARAMETER_COMMAND_USER_FINISH_EDIT_ANNOUNCEMENT = "user_finish_edit_announcement";
    public static final String PARAMETER_COMMAND_USER_BLOCK_ANNOUNCEMENT = "user_block_announcement";
    public static final String PARAMETER_COMMAND_USER_START_VIEW_ANNOUNCEMENT = "user_start_view_announcement";
    public static final String PARAMETER_COMMAND_USER_LEAVE_COMMENT = "user_leave_comment";
    public static final String PARAMETER_COMMAND_USER_LEAVE_RATE = "user_leave_rate";
    public static final String PARAMETER_COMMAND_USER_START_CHATTING = "user_start_chatting";
    public static final String PARAMETER_COMMAND_USER_SEND_MESSAGE = "user_send_message";
    public static final String PARAMETER_COMMAND_ADMIN_BLOCK_OR_ACTIVATE_USER = "admin_block_or_activate_user";
    public static final String PARAMETER_COMMAND_SHOW_ANN_BY_CATEGORY = "show_ann_by_category";



    // radio for admin
    public static final String PARAMETER_RADIO_KEY = "radio";
    public static final String PARAMETER_RADIO_USERS = "radio_users";
    public static final String PARAMETER_RADIO_NEW_ANNOUNCEMENTS = "radio_new_announcements";
    public static final String PARAMETER_RADIO_ALL_ANNOUNCEMENTS = "radio_all_announcements";
    public static final String PARAMETER_RADIO_ADMIN_INFO = "radio_admin_info";
    public static final String PARAMETER_RADIO_ADMIN_ALL_CATEGORY = "all_category";
    //user radio commands
    public static final String PARAMETER_USER_RADIO_ALL_ANNOUNCEMENT = "user_radio_all_announcement";
    public static final String PARAMETER_USER_RADIO_OWN_ANNOUNCEMENT = "user_own_announcement";
    public static final String PARAMETER_USER_RADIO_INFO = "user_own_info";
    public static final String PARAMETER_USER_RADIO_MESSAGING = "user_radio_messaging";
    public static final String PARAMETER_USER_RADIO_CHAT = "user_chat";

    // category params
    public static final String PARAMETER_CATEGORY_ID = "category_id";
    public static final String SESSION_ATTRIBUTE_CATEGORY_ID = "session_category_id";
    public static final String PARAMETER_CATEGORY_NAME = "category_name";

    // announcement params
    public static final String PARAMETER_ANNOUNCEMENT_ID = "announcement_id";
    public static final String PARAMETER_ANNOUNCEMENT_CATEGORY_ID = "announcement_category_id";
    public static final String PARAMETER_ANNOUNCEMENT_HEADER = "announcement_header";
    public static final String PARAMETER_ANNOUNCEMENT_DEFINITION = "announcement_definition";
    public static final String PARAMETER_ANNOUNCEMENT_PRICE = "announcement_price";
    public static final String PARAMETER_ANNOUNCEMENT_DISCOUNT = "announcement_discount";

    // comment params
    public static final String PARAMETER_COMMENT_TEXT = "comment_text";

    // rates param
    public static final String PARAMETER_RATE_NUMBER = "rate_number";
    public static final String SESSION_ATTRIBUTE_RATE_TO_USER = "rate_to_user";

    //PreCommand
    public static final String PARAMETER_COMMAND_PRE_REGISTER = "pre_register";
    public static final String PARAMETER_COMMAND_PRE_LOGIN = "pre_login";

    // chat params
    public static final String PARAMETER_CHAT_PARTNER_ID = "chat_partner_id";
    public static final String PARAMETER_CHAT_ANNS_ID = "chat_anss_id";
    public static final String PARAMETER_CHAT_MESSAGE = "chat_partner_id";



    // session attr
    public static final String SESSION_ATTRIBUTE_USER_ROLE = "user_role";
    public static final String SESSION_ATTRIBUTE_USER = "user";
    public static final String SESSION_ATTRIBUTE_USER_NAME = "user_name";
    public static final String SESSION_ATTRIBUTE_USER_EMAIL = "user_email";
    public static final String SESSION_ATTRIBUTE_LOGIN_MSG = "login_msg";
    public static final String SESSION_ATTRIBUTE_BLOCKED_MSG = "blocked_msg";
    public static final String SESSION_ATTRIBUTE_EXIST_MSG = "exist_msg";
    public static final String SESSION_ATTRIBUTE_USER_LIST = "user_list";
    //announcement
    public static final String SESSION_ATTRIBUTE_NEW_ANNOUNCEMENT_LIST = "new_announcement_list";
    public static final String SESSION_ATTRIBUTE_ANNOUNCEMENT = "announcement";
    public static final String SESSION_ATTRIBUTE_ANNOUNCEMENT_OWNER = "announcement_owner";
    public static final String SESSION_ATTRIBUTE_ANNOUNCEMENT_LIST = "announcement_list";
    public static final String SESSION_ATTRIBUTE_CATEGORY_LIST = "category_list";
    public static final String SESSION_ATTRIBUTE_CATEGORY = "category";
    // comment
    public static final String SESSION_ATTRIBUTE_COMMENT_LIST = "comment_list";
    public static final String SESSION_ATTRIBUTE_RATES_TO_USER = "rates_to_user";
    public static final String SESSION_ATTRIBUTE_COMMENT_FROM_USER = "comment_from_user";
    public static final String SESSION_ATTRIBUTE_COMMENT_TO_ANNOUNCEMENT = "comment_to_announcement";

    // chatting sessions
    public static final String SESSION_ATTRIBUTE_CHAT_LIST = "chat_list";
    public static final String SESSION_ATTRIBUTE_CHAT_ANNOUNCEMENT_ID = "chat_announcement_id";
    public static final String SESSION_ATTRIBUTE_CHAT_PARTNER_ID = "chat_partner_id";
    public static final String SESSION_ATTRIBUTE_CHAT_USER_CHATTERS = "chat_user_chattersList";


    private AttributeParameterHolder() {
    }
}
