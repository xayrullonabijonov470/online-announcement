package uz.pdp.onlineannouncement.dao;

import uz.pdp.onlineannouncement.entity.User;

import java.util.List;
import java.util.UUID;

public interface UserDao {

    boolean authenticate(String login, String password);
    boolean insert(User user);
    boolean delete(User user);
    List<User> findAll();
    boolean update(User user);
    boolean checkToAdmin(String user_name, String password);
    boolean checkToActive(UUID userId);
    User findById(UUID id);
    User findByEmail(String email);
    boolean blockOrActive(UUID userId, boolean setActive);
    List<User> findAllWithoutUser(UUID userId);
}
