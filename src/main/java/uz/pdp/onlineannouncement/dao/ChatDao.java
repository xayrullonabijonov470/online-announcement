package uz.pdp.onlineannouncement.dao;

import uz.pdp.onlineannouncement.entity.Chat;

import java.util.List;
import java.util.UUID;

public interface ChatDao {
    boolean insert(Chat chat);
    boolean delete(UUID id);
    List<Chat> findAll();
    List<Chat> findAllByBothUserAndAnnouncement(UUID senderId, UUID receiverId, UUID announceemntId);
    boolean update(Chat chat);
    Chat findById(UUID id);
    List<Chat> findAllUserChattersByUser(UUID userId);

}
