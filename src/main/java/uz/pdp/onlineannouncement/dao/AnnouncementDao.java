package uz.pdp.onlineannouncement.dao;


import uz.pdp.onlineannouncement.entity.Announcement;

import java.util.List;
import java.util.UUID;

public interface AnnouncementDao {

    boolean insert(Announcement announcement);
    boolean delete(UUID id);
    List<Announcement> findAll();
    boolean update(Announcement announcement);
    Announcement findById(UUID id);
    List<Announcement> findAllByApprovedOrBlocked(boolean findApproved);
    List<Announcement> findAllWithoutOwner(UUID userId);
    List<Announcement> findAllByOwner(UUID userId);
    List<Announcement> findAllByCategory(UUID categoryId, UUID userId);
    boolean setApprovedOrBlocked(UUID id, boolean approving);

}
