package uz.pdp.onlineannouncement.dao.impl;

import uz.pdp.onlineannouncement.dao.RatesDao;
import uz.pdp.onlineannouncement.entity.Rate;
import uz.pdp.onlineannouncement.pool.ConnectionPool;
import uz.pdp.onlineannouncement.util.mapper.RateMapper;

import java.sql.*;
import java.util.List;
import java.util.UUID;

public class RatesDaoImpl implements RatesDao
{
    private static final RatesDaoImpl instance = new RatesDaoImpl();

    public static RatesDaoImpl getInstance(){
        return instance;
    }

    private RatesDaoImpl() {
    }

    private final ConnectionPool connectionPool = ConnectionPool.getInstance();

    @Override
    public boolean insert(Rate rate)
    {
        boolean b = checkNotExist(rate.getToUser().getId(), rate.getFromUser().getId());
        if (b){
            return false;
        }
        Connection connection = connectionPool.getConnection();
        try
        {
            PreparedStatement preparedStatement = connection.prepareStatement(
                    "insert into rates(id, to_user, from_user, rate, created_at) values (?, ?, ?, ?, ?);");
            preparedStatement.setObject(1,rate.getId());
            preparedStatement.setObject(2,rate.getToUser().getId());
            preparedStatement.setObject(3,rate.getFromUser().getId());
            preparedStatement.setDouble(4, rate.getRate());
            preparedStatement.setTimestamp(5, rate.getCreatedAt());

            preparedStatement.execute();
            return true;
        }
        catch (SQLException e)
        {
            throw new RuntimeException(e);
        }
        finally
        {
            connectionPool.releaseConnection(connection);
        }
    }

    @Override
    public boolean delete(UUID id)
    {
        Connection connection = connectionPool.getConnection();
        try
        {
            PreparedStatement preparedStatement = connection.prepareStatement(
                    "delete from rates where id = ?;");
            preparedStatement.setObject(1, id);
            preparedStatement.execute();
            return true;
        }
        catch (SQLException e)
        {
            throw new RuntimeException(e);
        }
        finally
        {
            connectionPool.releaseConnection(connection);
        }
    }

    @Override
    public List<Rate> findAll()
    {
        Connection connection = connectionPool.getConnection();
        try
        {
            Statement statement = connection.createStatement();
            ResultSet resultSet = statement.executeQuery("select * from rates order by created_at;");
            return RateMapper.convertResultSetToRateList(resultSet);
        }
        catch (SQLException e)
        {
            throw new RuntimeException(e);
        }
        finally
        {
            connectionPool.releaseConnection(connection);
        }
    }

    @Override
    public List<Rate> findAllByToUserId(UUID toUserId)
    {
        Connection connection = connectionPool.getConnection();
        try
        {
            PreparedStatement preparedStatement = connection.prepareStatement("select * from rates where to_user = ? order by created_at");
            preparedStatement.setObject(1, toUserId);
            ResultSet resultSet = preparedStatement.executeQuery();
            return RateMapper.convertResultSetToRateList(resultSet);
        }
        catch (SQLException e)
        {
            throw new RuntimeException(e);
        }
        finally
        {
            connectionPool.releaseConnection(connection);
        }
    }

    @Override
    public boolean update(Rate rate) {
        return false;
    }

    @Override
    public Rate findById(UUID id)
    {
        Connection connection = connectionPool.getConnection();
        try
        {
            PreparedStatement preparedStatement = connection.prepareStatement("select * from rates where id = ?");
            preparedStatement.setObject(1, id);
            ResultSet resultSet = preparedStatement.executeQuery();
            List<Rate> rateList = RateMapper.convertResultSetToRateList(resultSet);
            return rateList.isEmpty() ? null : rateList.get(0);
        }
        catch (SQLException e)
        {
            throw new RuntimeException(e);
        }
        finally
        {
            connectionPool.releaseConnection(connection);
        }
    }

    public boolean checkNotExist(UUID toUser, UUID fromUser)
    {
        Connection connection = connectionPool.getConnection();
        try
        {
            PreparedStatement preparedStatement = connection.prepareStatement("select 1 from rates where to_user = ? and from_user = ?");
            preparedStatement.setObject(1, toUser);
            preparedStatement.setObject(2, fromUser);
            ResultSet resultSet = preparedStatement.executeQuery();
            return resultSet.next();
        }
        catch (SQLException e)
        {
            throw new RuntimeException(e);
        }
        finally
        {
            connectionPool.releaseConnection(connection);
        }
    }

}
