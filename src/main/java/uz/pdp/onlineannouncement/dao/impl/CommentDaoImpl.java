package uz.pdp.onlineannouncement.dao.impl;

import uz.pdp.onlineannouncement.dao.CommentDao;
import uz.pdp.onlineannouncement.entity.Comment;
import uz.pdp.onlineannouncement.pool.ConnectionPool;
import uz.pdp.onlineannouncement.util.mapper.CommentMapper;

import java.sql.*;
import java.util.List;
import java.util.UUID;

public class CommentDaoImpl implements CommentDao {

    private static final CommentDaoImpl instance = new CommentDaoImpl();

    public static CommentDaoImpl getInstance(){
        return instance;
    }

    private CommentDaoImpl() {
    }

    private final ConnectionPool connectionPool = ConnectionPool.getInstance();

    @Override
    public boolean insert(Comment comment)
    {
        Connection connection = connectionPool.getConnection();
        try
        {
            PreparedStatement preparedStatement = connection.prepareStatement(
                    "insert into comment(id, announcement_id, text, parent_comment_id, user_id, created_at) VALUES(?, ?, ?, ?, ?, ?) ");
            preparedStatement.setObject(1,comment.getId());
            preparedStatement.setObject(2,comment.getAnnouncement().getId());
            preparedStatement.setString(3, comment.getText());
            preparedStatement.setObject(4, null);
            preparedStatement.setObject(5, comment.getUser().getId());
            preparedStatement.setTimestamp(6, comment.getCreatedAt());

            preparedStatement.execute();
            return true;
        }
        catch (SQLException e)
        {
            throw new RuntimeException(e);
        }
        finally
        {
            connectionPool.releaseConnection(connection);
        }
    }

    @Override
    public boolean delete(UUID id)
    {
        Connection connection = connectionPool.getConnection();
        try
        {
            PreparedStatement preparedStatement = connection.prepareStatement("delete from comment where id = ? ;");
            preparedStatement.setObject(1, id);
            preparedStatement.execute();
            return true;
        }
        catch (SQLException e)
        {
            throw new RuntimeException(e);
        }
        finally
        {
            connectionPool.releaseConnection(connection);
        }
    }

    @Override
    public List<Comment> findAll()
    {
        Connection connection = connectionPool.getConnection();
        try
        {
            Statement statement = connection.createStatement();
            ResultSet resultSet = statement.executeQuery("select * from comment  order by created_at ;");
            List<Comment> commentList = CommentMapper.convertResultSetToCommentList(resultSet);
            return commentList;
        }
        catch (SQLException e)
        {
            throw new RuntimeException(e);
        }
        finally
        {
            connectionPool.releaseConnection(connection);
        }
    }

    @Override
    public List<Comment> findAllByAnnouncementId(UUID announcementId)
    {
        Connection connection = connectionPool.getConnection();
        try
        {
            PreparedStatement preparedStatement = connection.prepareStatement("select * from comment where announcement_id = ? order by created_at;");
            preparedStatement.setObject(1, announcementId);
            ResultSet resultSet = preparedStatement.executeQuery();
            return CommentMapper.convertResultSetToCommentList(resultSet);
        }
        catch (SQLException e)
        {
            throw new RuntimeException(e);
        }
        finally
        {
            connectionPool.releaseConnection(connection);
        }
    }

    @Override
    public boolean update(Comment comment) {
        return false;
    }

    @Override
    public Comment findById(UUID id)
    {
        Connection connection = connectionPool.getConnection();
        try
        {
            PreparedStatement preparedStatement = connection.prepareStatement("select * from comment where id = ?;");
            preparedStatement.setObject(1, id);
            ResultSet resultSet = preparedStatement.executeQuery();
            List<Comment> commentList = CommentMapper.convertResultSetToCommentList(resultSet);
            return commentList.isEmpty() ? null : commentList.get(0);
        }
        catch (SQLException e)
        {
            throw new RuntimeException(e);
        }
        finally
        {
            connectionPool.releaseConnection(connection);
        }
    }
}
