package uz.pdp.onlineannouncement.dao.impl;

import uz.pdp.onlineannouncement.dao.ChatDao;
import uz.pdp.onlineannouncement.entity.Chat;
import uz.pdp.onlineannouncement.pool.ConnectionPool;
import uz.pdp.onlineannouncement.service.AnnouncementService;
import uz.pdp.onlineannouncement.service.UserService;
import uz.pdp.onlineannouncement.service.impl.AnnouncementServiceImpl;
import uz.pdp.onlineannouncement.service.impl.UserServiceImpl;
import uz.pdp.onlineannouncement.util.mapper.ChatMapper;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

public class ChatDaoImpl implements ChatDao
{
    private static final ChatDaoImpl instance = new ChatDaoImpl();

    public static ChatDaoImpl getInstance()
    {
        return instance;
    }

    private ChatDaoImpl(){
    }

    private final ConnectionPool connectionPool = ConnectionPool.getInstance();

    @Override
    public boolean insert(Chat chat)
    {
        Connection connection = connectionPool.getConnection();
        try
        {
            PreparedStatement ps = connection.prepareStatement(
                    "insert into chat(id, announcement_id, sender_id, receiver_id, message, date, is_read, is_deleted) VALUES(?, ?, ?, ?, ?, ?, ?, ?) ;");
            ps.setObject(1, chat.getId());
            ps.setObject(2, chat.getAnnouncement().getId());
            ps.setObject(3, chat.getSender().getId());
            ps.setObject(4, chat.getReceiver().getId());
            ps.setString(5, chat.getMessage());
            ps.setTimestamp(6, chat.getCreatedAt());
            ps.setBoolean(7, chat.isRead());
            ps.setBoolean(8, chat.isDeleted());

            ps.execute();
            return true;
        } catch (SQLException e) {
            throw new RuntimeException(e);
        } finally {
            connectionPool.releaseConnection(connection);
        }
    }

    @Override
    public boolean delete(UUID id) {
        return false;
    }

    @Override
    public List<Chat> findAll()
    {
        Connection connection = connectionPool.getConnection();
        try
        {
            Statement statement = connection.createStatement();
            ResultSet resultSet = statement.executeQuery("select * chat where is_deleted = false order by date");
            return ChatMapper.convertResultSetToChatList(resultSet);
        } catch (SQLException e) {
            throw new RuntimeException(e);
        } finally {
            connectionPool.releaseConnection(connection);
        }
    }

    @Override
    public List<Chat> findAllByBothUserAndAnnouncement(UUID senderId, UUID receiverId, UUID announceemntId)
    {
        Connection connection = connectionPool.getConnection();
        try
        {
            PreparedStatement preparedStatement = connection.prepareStatement(
                    "select * from chat where announcement_id = ? and ((sender_id = ? and receiver_id = ?) or (sender_id = ? and receiver_id = ?)) order by date;");
            preparedStatement.setObject(1, announceemntId);
            preparedStatement.setObject(2, senderId);
            preparedStatement.setObject(3, receiverId);
            preparedStatement.setObject(4, receiverId);
            preparedStatement.setObject(5, senderId);
            ResultSet resultSet = preparedStatement.executeQuery();
            return ChatMapper.convertResultSetToChatList(resultSet);
        } catch (SQLException e) {
            throw new RuntimeException(e);
        } finally {
            connectionPool.releaseConnection(connection);
        }
    }

    @Override
    public boolean update(Chat chat) {
        return false;
    }

    @Override
    public Chat findById(UUID id)
    {
        Connection connection = connectionPool.getConnection();
        try
        {
            PreparedStatement preparedStatement = connection.prepareStatement("select * chat where id = ? ;");
            preparedStatement.setObject(1, id);
            ResultSet resultSet = preparedStatement.executeQuery();
            List<Chat> chatList = ChatMapper.convertResultSetToChatList(resultSet);
            return chatList.isEmpty() ? null : chatList.get(0);
        } catch (SQLException e) {
            throw new RuntimeException(e);
        } finally {
            connectionPool.releaseConnection(connection);
        }
    }

    @Override
    public List<Chat> findAllUserChattersByUser(UUID userId)
    {
        Connection connection = connectionPool.getConnection();
        try
        {
            String sql = "with ss as( select c.receiver_id, c.sender_id, announcement_id from chat c where receiver_id = ? group by announcement_id, c.sender_id, c.receiver_id ) select * from ss UNION select c.receiver_id, c.sender_id, c.announcement_id from chat c where c.sender_id = ? and c.receiver_id not in (select sender_id from ss);";
            PreparedStatement preparedStatement = connection.prepareStatement(sql);
            preparedStatement.setObject(1, userId);
            preparedStatement.setObject(2, userId);
            ResultSet resultSet = preparedStatement.executeQuery();

            AnnouncementService announcementService = AnnouncementServiceImpl.getInstance();
            UserService userService = UserServiceImpl.getInstance();
            List<Chat> chatList = new ArrayList<>();
            while (resultSet.next()){
                Chat chat = new Chat();
                chat.setReceiver(userService.findById((UUID) resultSet.getObject(1)));
                chat.setSender(userService.findById((UUID) resultSet.getObject(2)));
                chat.setAnnouncement(announcementService.findById((UUID) resultSet.getObject(3)));
                chatList.add(chat);
            }
            return chatList;
        } catch (SQLException e) {
            throw new RuntimeException(e);
        } finally {
            connectionPool.releaseConnection(connection);
        }
    }

}
