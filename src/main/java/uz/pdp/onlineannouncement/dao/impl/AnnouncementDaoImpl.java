package uz.pdp.onlineannouncement.dao.impl;

import uz.pdp.onlineannouncement.dao.AnnouncementDao;
import uz.pdp.onlineannouncement.entity.Announcement;
import uz.pdp.onlineannouncement.pool.ConnectionPool;
import uz.pdp.onlineannouncement.util.mapper.AnnouncementMapper;

import java.sql.*;
import java.util.List;
import java.util.UUID;

public class AnnouncementDaoImpl implements AnnouncementDao {

    private static final AnnouncementDaoImpl instance = new AnnouncementDaoImpl();

    public static AnnouncementDaoImpl getInstance(){
        return instance;
    }

    private final ConnectionPool connectionPool = ConnectionPool.getInstance();

    private AnnouncementDaoImpl() {
    }

    @Override
    public boolean insert(Announcement announcement)
    {
        Connection connection = connectionPool.getConnection();
        try
        {
            PreparedStatement preparedStatement = connection.prepareStatement(
                    "insert into announcement(id, category_id, user_id, header, definition, price, discount, approved, created_at) VALUES (?,?,?,?,?,?,?,?,?);");
            preparedStatement.setObject(1, UUID.randomUUID());
            preparedStatement.setObject(2, announcement.getCategory().getId());
            preparedStatement.setObject(3, announcement.getUser().getId());
            preparedStatement.setString(4, announcement.getHeader());
            preparedStatement.setString(5, announcement.getDefinition());
            preparedStatement.setDouble(6, announcement.getPrice());
            preparedStatement.setDouble(7, announcement.getDiscount());
            preparedStatement.setBoolean(8, announcement.isApproved());
            preparedStatement.setTimestamp(9, announcement.getCreatedAt());
            preparedStatement.execute();
            return true;
        }
        catch (SQLException e)
        {
            throw new RuntimeException(e);
        }
        finally
        {
            connectionPool.releaseConnection(connection);
        }
    }

    @Override
    public boolean delete(UUID id)
    {
        Connection connection = connectionPool.getConnection();
        try
        {
            PreparedStatement preparedStatement = connection.prepareStatement("delete from announcement where id = ?;");
            preparedStatement.setObject(1,id);
            preparedStatement.execute();
            return true;
        }
        catch (SQLException e)
        {
            throw new RuntimeException(e);
        }
        finally
        {
            connectionPool.releaseConnection(connection);
        }
    }

    @Override
    public List<Announcement> findAll()
    {
        Connection connection = connectionPool.getConnection();
        try
        {
            Statement statement = connection.createStatement();
            ResultSet resultSet = statement.executeQuery("select * from announcement where approved = true order by created_at;");
            return AnnouncementMapper.convertResultSetToAnnouncement(resultSet);
        }
        catch (SQLException e)
        {
            throw new RuntimeException(e);
        }
        finally
        {
            connectionPool.releaseConnection(connection);
        }
    }

    @Override
    public boolean update(Announcement announcement)
    {
        Connection connection = connectionPool.getConnection();
        try
        {
            PreparedStatement preparedStatement = connection.prepareStatement(
                    "update announcement set category_id = ?, header = ?, definition = ?, price = ?, discount = ?, approved = false where id = ?;");
            preparedStatement.setObject(1,announcement.getCategory().getId());
            preparedStatement.setString(2, announcement.getHeader());
            preparedStatement.setString(3, announcement.getDefinition());
            preparedStatement.setDouble(4, announcement.getPrice());
            preparedStatement.setDouble(5, announcement.getDiscount());
            preparedStatement.setObject(6,announcement.getId());

            preparedStatement.execute();
            return true;
        }
        catch (SQLException e)
        {
            throw new RuntimeException(e);
        }
        finally
        {
            connectionPool.releaseConnection(connection);
        }
    }

    @Override
    public Announcement findById(UUID id)
    {
        Connection connection = connectionPool.getConnection();
        try
        {
            PreparedStatement preparedStatement = connection.prepareStatement("select * from announcement where id = ? ;");
            preparedStatement.setObject(1,id);
            ResultSet resultSet = preparedStatement.executeQuery();
            List<Announcement> list = AnnouncementMapper.convertResultSetToAnnouncement(resultSet);
            return  !list.isEmpty() ? list.get(0) : null;
        }
        catch (SQLException e)
        {
            throw new RuntimeException(e);
        }
        finally
        {
            connectionPool.releaseConnection(connection);
        }
    }

    @Override
    public List<Announcement> findAllByApprovedOrBlocked(boolean findApproved)
    {
        Connection connection = connectionPool.getConnection();
        try
        {
            PreparedStatement preparedStatement = connection.prepareStatement("select * from announcement where approved = ? order by created_at;");
            preparedStatement.setBoolean(1,findApproved);
            ResultSet resultSet = preparedStatement.executeQuery();
            return AnnouncementMapper.convertResultSetToAnnouncement(resultSet);
        }
        catch (SQLException e)
        {
            throw new RuntimeException(e);
        }
        finally
        {
            connectionPool.releaseConnection(connection);
        }
    }

    @Override
    public List<Announcement> findAllWithoutOwner(UUID userId)
    {
        Connection connection = connectionPool.getConnection();
        try
        {

            PreparedStatement preparedStatement = connection.prepareStatement("select * from announcement where approved = true and user_id <> ? order by created_at;");
            preparedStatement.setObject(1, userId);
            ResultSet resultSet = preparedStatement.executeQuery();
            return AnnouncementMapper.convertResultSetToAnnouncement(resultSet);
        }
        catch (SQLException e)
        {
            throw new RuntimeException(e);
        }
        finally
        {
            connectionPool.releaseConnection(connection);
        }
    }

    @Override
    public List<Announcement> findAllByOwner(UUID userId)
    {
        Connection connection = connectionPool.getConnection();
        try
        {
            PreparedStatement preparedStatement = connection.prepareStatement("select * from announcement where user_id = ? order by created_at;");
            preparedStatement.setObject(1, userId);
            ResultSet resultSet = preparedStatement.executeQuery();
            return AnnouncementMapper.convertResultSetToAnnouncement(resultSet);
        }
        catch (SQLException e)
        {
            throw new RuntimeException(e);
        }
        finally
        {
            connectionPool.releaseConnection(connection);
        }
    }

    @Override
    public List<Announcement> findAllByCategory(UUID categoryId, UUID userId)
    {
        Connection connection = connectionPool.getConnection();
        try
        {
            PreparedStatement preparedStatement ;
            String sql ;
            if (userId != null)
            {
                sql = "select * from announcement where category_id = ? and user_id <> ? order by created_at;";
                preparedStatement = connection.prepareStatement(sql);
                preparedStatement.setObject(1, categoryId);
                preparedStatement.setObject(2, userId);
            }else
            {
                sql = "select * from announcement where category_id = ? order by created_at;";
                preparedStatement = connection.prepareStatement(sql);
                preparedStatement.setObject(1, categoryId);
            }

            ResultSet resultSet = preparedStatement.executeQuery();
            return AnnouncementMapper.convertResultSetToAnnouncement(resultSet);
        }
        catch (SQLException e)
        {
            throw new RuntimeException(e);
        }
        finally
        {
            connectionPool.releaseConnection(connection);
        }
    }

    @Override
    public boolean setApprovedOrBlocked(UUID id, boolean approving)
    {
        Connection connection = connectionPool.getConnection();
        try
        {

            PreparedStatement preparedStatement = connection.prepareStatement("update announcement set approved = ? where id = ? ;");

            preparedStatement.setBoolean(1, approving);
            preparedStatement.setObject(2, id);
            preparedStatement.execute();
            return true;
        }
        catch (SQLException e)
        {
            throw new RuntimeException(e);
        }
        finally
        {
            connectionPool.releaseConnection(connection);
        }
    }

}
