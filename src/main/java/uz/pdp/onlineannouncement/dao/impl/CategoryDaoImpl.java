package uz.pdp.onlineannouncement.dao.impl;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import uz.pdp.onlineannouncement.dao.CategoryDao;
import uz.pdp.onlineannouncement.entity.Category;
import uz.pdp.onlineannouncement.pool.ConnectionPool;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import static org.apache.logging.log4j.Level.ERROR;

public class CategoryDaoImpl implements CategoryDao
{
    private static final CategoryDaoImpl instance = new CategoryDaoImpl();

    public static CategoryDaoImpl getInstance(){
        return instance;
    }

    static Logger logger = LogManager.getLogger();

    private final ConnectionPool connectionPool = ConnectionPool.getInstance();

    private CategoryDaoImpl() {
    }

    @Override
    public boolean insert(Category category)
    {
        if (checkToInserted(category.getName())){
            return false;
        }
        Connection connection = connectionPool.getConnection();
        try
        {
            PreparedStatement preparedStatement = connection.prepareStatement("insert into category(id, name, created_at) VALUES(?,?,?) ;");
            preparedStatement.setObject(1,category.getId());
            preparedStatement.setString(2, category.getName());
            preparedStatement.setTimestamp(3, category.getCreatedAt());
            preparedStatement.execute();
            return true;
        }
        catch (SQLException e)
        {
            logger.error(e);
            throw new RuntimeException(e);
        }
        finally
        {
            connectionPool.releaseConnection(connection);
        }
    }

    @Override
    public boolean delete(Category category)
    {
        Connection connection = connectionPool.getConnection();
        try
        {
            PreparedStatement preparedStatement = connection.prepareStatement("delete from category where id = ?;");
            preparedStatement.setObject(1, category.getId());
            preparedStatement.execute();
            return true;
        }
        catch (SQLException e)
        {
            logger.log(ERROR, e);
            throw new RuntimeException(e);
        }
        finally
        {
            connectionPool.releaseConnection(connection);
        }
    }

    public boolean checkToInserted(String categoryName)
    {
        Connection connection = connectionPool.getConnection();
        try
        {
            PreparedStatement preparedStatement = connection.prepareStatement("select * from category where name = ?;");
            preparedStatement.setString(1, categoryName);
            ResultSet resultSet = preparedStatement.executeQuery();
            return resultSet.next();
        }
        catch (NullPointerException | SQLException e)
        {
            logger.log(ERROR, e);
            throw new RuntimeException(e);
        }
        finally
        {
            connectionPool.releaseConnection(connection);
        }
    }

    @Override
    public List<Category> findAll()
    {
        Connection connection = connectionPool.getConnection();
        try
        {
            List<Category> categoryList = new ArrayList<>();
            Statement statement = connection.createStatement();
            ResultSet resultSet = statement.executeQuery("select * from category order by created_at ;");
            while (resultSet.next())
            {
                Category category = new Category();
                category.setId((UUID) resultSet.getObject(1));
                category.setName(resultSet.getString(2));
                category.setCreatedAt(resultSet.getTimestamp(3));
                categoryList.add(category);
            }
            return categoryList;
        }
        catch (SQLException e)
        {
            throw new RuntimeException(e);
        }
        finally
        {
            connectionPool.releaseConnection(connection);
        }
    }

    @Override
    public boolean update(Category category)
    {
        Connection connection = connectionPool.getConnection();
        try
        {
            PreparedStatement preparedStatement = connection.prepareStatement("update category set name = ? where id = ?;");
            preparedStatement.setString(1, category.getName());
            preparedStatement.setObject(2,category.getId());
            preparedStatement.execute();
            return true;
        }
        catch (SQLException e)
        {
            throw new RuntimeException(e);
        }
        finally
        {
            connectionPool.releaseConnection(connection);
        }
    }

    @Override
    public Category findByIdOrName(UUID id, String name)
    {
        Connection connection = connectionPool.getConnection();
        try
        {
            PreparedStatement preparedStatement ;
            if (id != null)
            {
                preparedStatement = connection.prepareStatement("select * from category where id = ? ;");
                preparedStatement.setObject(1,id);
            }
            else
            {
                preparedStatement = connection.prepareStatement("select * from category where name = ? ;");
                preparedStatement.setString(1,name);
            }
            ResultSet resultSet = preparedStatement.executeQuery();
            Category category = new Category();
            while (resultSet.next())
            {
                category.setId((UUID) resultSet.getObject(1));
                category.setName(resultSet.getString(2));
            }
            return category;
        }
        catch (SQLException e)
        {
            throw new RuntimeException(e);
        }
        finally
        {
            connectionPool.releaseConnection(connection);
        }
    }

}
