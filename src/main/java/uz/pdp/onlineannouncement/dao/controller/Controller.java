package uz.pdp.onlineannouncement.dao.controller;

import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import uz.pdp.onlineannouncement.command.Command;
import uz.pdp.onlineannouncement.command.CommandType;
import uz.pdp.onlineannouncement.command.PreCommandType;
import uz.pdp.onlineannouncement.command.navigation.Router;
import uz.pdp.onlineannouncement.pool.ConnectionPool;

import java.io.IOException;

import static uz.pdp.onlineannouncement.command.navigation.AttributeParameterHolder.PARAMETER_COMMAND;

@WebServlet(name = "helloServlet", value = {"/controller", "/pages/controller", "*.do" })
public class Controller extends HttpServlet
{

    static Logger logger = LogManager.getLogger();

    public void init()
    {
        ConnectionPool.getInstance();
        logger.log(Level.INFO, "---------> servlet init : " + this.getServletInfo());
    }

    public void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException
    {
        mainMethod(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        mainMethod(req, resp);
    }

    public void mainMethod(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
        response.setContentType("text/html");
        String commandStr = request.getParameter(PARAMETER_COMMAND);
        if (commandStr.startsWith("pre_"))
        {
            PreCommandType[] values = PreCommandType.values();
            for (PreCommandType value : values)
            {
                if (value.name().equals(commandStr.toUpperCase()))
                {
                    String page = value.page;
                    request.getRequestDispatcher(page).forward(request,response);
                }
            }
        }
        else
        {
            Command command = CommandType.define(commandStr);
            Router router = command.execute(request);
            String page = router.getPage();
            switch (router.getType()) {
                case FORWARD -> request.getRequestDispatcher(page).forward(request, response);
                case REDIRECT -> response.sendRedirect(page);
            }
        }
    }

    public void destroy()
    {
        ConnectionPool.getInstance().destroyPool();
        logger.log(Level.INFO, "---------> servlet destroyed : " + this.getServletName());
    }
}