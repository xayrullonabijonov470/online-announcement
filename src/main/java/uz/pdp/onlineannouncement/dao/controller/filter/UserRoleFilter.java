package uz.pdp.onlineannouncement.dao.controller.filter;


import jakarta.servlet.*;
import jakarta.servlet.annotation.WebFilter;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;
import uz.pdp.onlineannouncement.command.CommandType;
import uz.pdp.onlineannouncement.command.navigation.PageNavigation;
import uz.pdp.onlineannouncement.entity.User;
import uz.pdp.onlineannouncement.entity.enums.RoleName;

import java.io.IOException;
import java.util.EnumSet;
import java.util.Map;

import static uz.pdp.onlineannouncement.command.CommandType.*;
import static uz.pdp.onlineannouncement.command.navigation.AttributeParameterHolder.PARAMETER_COMMAND;
import static uz.pdp.onlineannouncement.command.navigation.AttributeParameterHolder.SESSION_ATTRIBUTE_USER;
import static uz.pdp.onlineannouncement.entity.enums.RoleName.*;


@WebFilter(
        filterName = "CheckUserRoleFilter",
        dispatcherTypes = {DispatcherType.FORWARD,  DispatcherType.REQUEST},
        urlPatterns = {"/controller", "/pages/controller"})
public class UserRoleFilter implements Filter
{

    private Map<RoleName, EnumSet<CommandType>> userCommands;


    @Override
    public void init(FilterConfig filterConfig) throws ServletException {
        userCommands = Map.of(ADMIN, EnumSet.of(
                ADD_ADMIN,
                HOME_PAGE,
                LOGIN,
                LOGOUT,
                DEFAULT,
                REMOVE_USER,
                REGISTER,
                RADIO_COMMAND,
                ADMIN_APPROVE_ANNOUNCEMENT,
                ADMIN_BLOCK_ANNOUNCEMENT,
                ADMIN_BLOCK_OR_ACTIVATE_USER,
                ADMIN_START_EDIT_CATEGORY,
                ADMIN_FINISH_EDIT_CATEGORY,
                ADD_CATEGORY,
                EDIT_USER_INFO
        ), USER, EnumSet.of(
                HOME_PAGE,
                LOGIN,
                LOGOUT,
                DEFAULT,
                REGISTER,
                RADIO_COMMAND_USER,
                USER_START_CREATE_ANNOUNCEMENT,
                USER_FINISH_CREATE_ANNOUNCEMENT,
                USER_START_EDIT_ANNOUNCEMENT,
                USER_FINISH_EDIT_ANNOUNCEMENT,
                USER_START_CHATTING,
                USER_SEND_MESSAGE,
                USER_BLOCK_ANNOUNCEMENT,
                USER_OWN_ANNOUNCEMENT,
                USER_START_VIEW_ANNOUNCEMENT,
                USER_LEAVE_COMMENT,
                USER_LEAVE_RATE,
                EDIT_USER_INFO
        ), GUEST, EnumSet.of(
                HOME_PAGE,
                LOGIN,
                DEFAULT,
                REGISTER
        ));
    }

    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) throws IOException, ServletException {
        HttpServletRequest request = (HttpServletRequest) servletRequest;
        HttpSession session = request.getSession();
        HttpServletResponse response = (HttpServletResponse) servletResponse;

        String commandStr = request.getParameter(PARAMETER_COMMAND);

        User user = (User) session.getAttribute(SESSION_ATTRIBUTE_USER);
        RoleName userRole = user != null ? RoleName.valueOf(user.getRoleName()) : GUEST;

        EnumSet<CommandType> allowedCommands = userCommands.get(userRole);
        CommandType command = CommandType.defineCommandType(commandStr);

        if (!allowedCommands.contains(command)) {
            response.sendRedirect(request.getContextPath() + PageNavigation.PAGE_DEFAULT);
        } else {
            filterChain.doFilter(servletRequest, servletResponse);
        }
    }
}
