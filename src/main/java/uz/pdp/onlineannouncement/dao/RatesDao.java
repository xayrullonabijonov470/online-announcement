package uz.pdp.onlineannouncement.dao;

import uz.pdp.onlineannouncement.entity.Rate;

import java.util.List;
import java.util.UUID;

public interface RatesDao {
    boolean insert(Rate rate);
    boolean delete(UUID id);
    List<Rate> findAll();
    List<Rate> findAllByToUserId(UUID toUserId);
    boolean update(Rate rate);
    Rate findById(UUID id);
}
