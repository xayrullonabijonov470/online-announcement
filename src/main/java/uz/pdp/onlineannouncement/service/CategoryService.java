package uz.pdp.onlineannouncement.service;

import uz.pdp.onlineannouncement.entity.Category;

import java.util.List;
import java.util.UUID;

public interface CategoryService {
    boolean insert(Category category);
    boolean delete(Category category);
    List<Category> findAll();
    boolean update(Category category);
    Category findByIdOrName(UUID id, String name);
}
