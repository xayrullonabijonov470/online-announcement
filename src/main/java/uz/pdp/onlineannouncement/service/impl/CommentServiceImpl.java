package uz.pdp.onlineannouncement.service.impl;

import uz.pdp.onlineannouncement.dao.CommentDao;
import uz.pdp.onlineannouncement.dao.impl.CommentDaoImpl;
import uz.pdp.onlineannouncement.entity.Comment;
import uz.pdp.onlineannouncement.service.CommentService;

import java.util.List;
import java.util.UUID;

public class CommentServiceImpl implements CommentService
{
    private static final CommentServiceImpl instance = new CommentServiceImpl();

    public static CommentServiceImpl getInstance(){
        return instance;
    }

    private CommentServiceImpl() {
    }

    CommentDao commentDao = CommentDaoImpl.getInstance();

    @Override
    public boolean insert(Comment comment) {
        return commentDao.insert(comment);
    }

    @Override
    public boolean delete(UUID id) {
        return commentDao.delete(id);
    }

    @Override
    public List<Comment> findAll() {
        return commentDao.findAll();
    }

    @Override
    public List<Comment> findAllByAnnouncementId(UUID announcementId) {
        return commentDao.findAllByAnnouncementId(announcementId);
    }

    @Override
    public boolean update(Comment comment) {
        return commentDao.update(comment);
    }

    @Override
    public Comment findById(UUID id) {
        return commentDao.findById(id);
    }
}
