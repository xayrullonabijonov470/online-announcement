package uz.pdp.onlineannouncement.service.impl;

import uz.pdp.onlineannouncement.dao.AnnouncementDao;
import uz.pdp.onlineannouncement.dao.impl.AnnouncementDaoImpl;
import uz.pdp.onlineannouncement.entity.Announcement;
import uz.pdp.onlineannouncement.service.AnnouncementService;

import java.util.List;
import java.util.UUID;

public class AnnouncementServiceImpl implements AnnouncementService {

    private final AnnouncementDao announcementDao = AnnouncementDaoImpl.getInstance();

    private static final AnnouncementServiceImpl instance = new AnnouncementServiceImpl();

    public static AnnouncementServiceImpl getInstance(){
        return instance;
    }

    private AnnouncementServiceImpl() {
    }

    @Override
    public boolean insert(Announcement announcement) {
        return announcementDao.insert(announcement);
    }

    @Override
    public boolean delete(UUID id) {
        return announcementDao.delete(id);
    }

    @Override
    public List<Announcement> findAll() {
        return announcementDao.findAll();
    }

    @Override
    public boolean update(Announcement announcement) {
        return announcementDao.update(announcement);
    }

    @Override
    public Announcement findById(UUID id) {
        return announcementDao.findById(id);
    }

    @Override
    public List<Announcement> findAllByApprovedOrBlocked(boolean findApproved) {
        return announcementDao.findAllByApprovedOrBlocked(findApproved);
    }

    @Override
    public List<Announcement> findAllWithoutOwner(UUID userId) {
        return announcementDao.findAllWithoutOwner(userId);
    }

    @Override
    public boolean setApprovedOrBlocked(UUID id, boolean approving) {
        return announcementDao.setApprovedOrBlocked(id, approving);
    }

    @Override
    public List<Announcement> findAllByOwner(UUID userId) {
        return announcementDao.findAllByOwner(userId);
    }

}
