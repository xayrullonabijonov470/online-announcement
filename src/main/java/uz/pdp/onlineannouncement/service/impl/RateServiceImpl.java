package uz.pdp.onlineannouncement.service.impl;

import uz.pdp.onlineannouncement.dao.RatesDao;
import uz.pdp.onlineannouncement.dao.impl.RatesDaoImpl;
import uz.pdp.onlineannouncement.entity.Rate;
import uz.pdp.onlineannouncement.service.RateService;

import java.util.List;
import java.util.UUID;

public class RateServiceImpl implements RateService
{
    private static final RateServiceImpl instance = new RateServiceImpl();

    public static RateServiceImpl getInstance(){
        return instance;
    }

    private RateServiceImpl() {
    }

    RatesDao ratesDao = RatesDaoImpl.getInstance();

    @Override
    public boolean insert(Rate rate) {
        return ratesDao.insert(rate);
    }

    @Override
    public boolean delete(UUID id) {
        return ratesDao.delete(id);
    }

    @Override
    public List<Rate> findAll() {
        return ratesDao.findAll();
    }

    @Override
    public List<Rate> findAllByToUserId(UUID toUserId) {
        return ratesDao.findAllByToUserId(toUserId);
    }

    @Override
    public boolean update(Rate rate) {
        return ratesDao.update(rate);
    }

    @Override
    public Rate findById(UUID id) {
        return ratesDao.findById(id);
    }
}
