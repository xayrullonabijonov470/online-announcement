package uz.pdp.onlineannouncement.service.impl;

import uz.pdp.onlineannouncement.dao.UserDao;
import uz.pdp.onlineannouncement.dao.impl.UserDaoImpl;
import uz.pdp.onlineannouncement.entity.User;
import uz.pdp.onlineannouncement.service.UserService;

import java.util.List;
import java.util.UUID;

public class UserServiceImpl implements UserService
{

    private static final UserServiceImpl instance = new UserServiceImpl();

    private final UserDao userDao = UserDaoImpl.getInstance();

    private UserServiceImpl() {
    }

    public static UserServiceImpl getInstance() {
        return instance;
    }

    @Override
    public boolean authenticate(String login, String password){
        return userDao.authenticate(login, password);
    }

    @Override
    public boolean insert(User user)
    {
        return userDao.insert(user);
    }

    @Override
    public boolean delete(User user){
        return userDao.delete(user);
    }

    @Override
    public List<User> findAll(){
        return userDao.findAll();
    }

    @Override
    public boolean update(User user){
        return userDao.update(user);
    }

    @Override
    public boolean checkToAdmin(String email, String password){
        return userDao.checkToAdmin(email, password);
    }

    @Override
    public User findById(UUID id){
        return userDao.findById(id);
    }

    @Override
    public User findByEmail(String email){
        return userDao.findByEmail(email);
    }

    @Override
    public boolean blockOrActive(UUID userId, boolean setActive){
        return userDao.blockOrActive(userId, setActive);
    }

    @Override
    public List<User> findAllWithoutUser(UUID userId) {
        return userDao.findAllWithoutUser(userId);
    }

    @Override
    public boolean checkToActive(UUID userId) {
        return userDao.checkToActive(userId);
    }
}
