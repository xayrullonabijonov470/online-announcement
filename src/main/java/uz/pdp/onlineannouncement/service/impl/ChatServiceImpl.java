package uz.pdp.onlineannouncement.service.impl;

import uz.pdp.onlineannouncement.dao.ChatDao;
import uz.pdp.onlineannouncement.dao.impl.ChatDaoImpl;
import uz.pdp.onlineannouncement.entity.Chat;
import uz.pdp.onlineannouncement.service.ChatService;

import java.util.List;
import java.util.UUID;

public class ChatServiceImpl implements ChatService
{
    private static final ChatServiceImpl instance = new ChatServiceImpl();

    public static ChatServiceImpl getInstance(){
        return instance;
    }

    private ChatServiceImpl()
    {

    }

    ChatDao chatDao = ChatDaoImpl.getInstance();

    @Override
    public boolean insert(Chat chat) {
        return chatDao.insert(chat);
    }

    @Override
    public boolean delete(UUID id) {
        return chatDao.delete(id);
    }

    @Override
    public List<Chat> findAll() {
        return chatDao.findAll();
    }

    @Override
    public List<Chat> findAllByBothUserAndAnnouncement(UUID senderId, UUID receiverId, UUID announceemntId) {
        return chatDao.findAllByBothUserAndAnnouncement(senderId, receiverId, announceemntId);
    }

    @Override
    public boolean update(Chat chat) {
        return chatDao.update(chat);
    }

    @Override
    public Chat findById(UUID id) {
        return chatDao.findById(id);
    }

    @Override
    public List<Chat> findAllUserChattersByUser(UUID userId) {
        return chatDao.findAllUserChattersByUser(userId);
    }


}
