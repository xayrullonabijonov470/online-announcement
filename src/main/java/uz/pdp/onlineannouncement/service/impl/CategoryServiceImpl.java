package uz.pdp.onlineannouncement.service.impl;

import uz.pdp.onlineannouncement.dao.CategoryDao;
import uz.pdp.onlineannouncement.dao.impl.CategoryDaoImpl;
import uz.pdp.onlineannouncement.entity.Category;
import uz.pdp.onlineannouncement.service.CategoryService;

import java.util.List;
import java.util.UUID;

public class CategoryServiceImpl implements CategoryService {

    private static final CategoryServiceImpl instance = new CategoryServiceImpl();

    public static CategoryServiceImpl getInstance() {
        return instance;
    }

    private CategoryServiceImpl() {
    }

    public CategoryDao categoryDao = CategoryDaoImpl.getInstance();
    @Override
    public boolean insert(Category category) {
        return categoryDao.insert(category);
    }

    @Override
    public boolean delete(Category category) {
        return categoryDao.delete(category);
    }

    @Override
    public List<Category> findAll() {
        return categoryDao.findAll();
    }

    @Override
    public boolean update(Category category) {
        return categoryDao.update(category);
    }

    @Override
    public Category findByIdOrName(UUID id, String name) {
        return categoryDao.findByIdOrName(id, name);
    }
}
